﻿using Xamarin.Forms;

namespace ProjectOrange.CustomViews
{
    public class LocalWebView : WebView
    {
        public static readonly BindableProperty UriProperty = BindableProperty.Create(nameof(Uri),
                                                                                      typeof(string),
                                                                                      typeof(LocalWebView),
                                                                                      default(string),
                                                                                      BindingMode.TwoWay,
                                                                                      null,
                                                                                      (bindable, oldValue, newValue) =>
                                                                                      {
                                                                                        LocalWebView lwv = (LocalWebView)bindable;
                                                                                      });

        public string Uri
        {
            get { return (string) GetValue(UriProperty); }
            set { SetValue(UriProperty, value); }
        }
    }
}
