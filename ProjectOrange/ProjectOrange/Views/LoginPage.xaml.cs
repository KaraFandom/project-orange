﻿using ProjectOrange.Models;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class LoginPage : ContentPage
    {

        private const string IS_USERNAME_PASSWORD = "IsUsernamePassword";
        private const string IS_LOADING = "IsLoading";

        public LoginPage ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Initialization
            NegateBooleanConverter boolInverter = new NegateBooleanConverter();

            BackgroundColor = Color.Transparent;
            BackgroundImage = AppTheme.LoginBackground;

            Color buttonColor = Color.FromHex("#333333");
            Color buttonTextColor = Color.White;
            LayoutOptions buttonHLayoutOptions = LayoutOptions.FillAndExpand;

            Color entryTextColor = Device.OnPlatform(Color.Black, Color.White, Color.White);
            Color entryPlaceholderColor = Device.OnPlatform(Color.Black, Color.White, Color.White);
            LayoutOptions entryHLayoutOptions = LayoutOptions.FillAndExpand;
            TextAlignment entryHTextAlignment = TextAlignment.Center;

            Color labelWarningsColor = Color.FromHex(AppTheme.WarningsColorFailedDefault);
            LayoutOptions labelHLayoutOptions = LayoutOptions.FillAndExpand;
            TextAlignment labelHTextAlignment = TextAlignment.Center;

            RelativeLayout main = new RelativeLayout () { };
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 10,
                Padding = new Thickness(10, Device.OnPlatform(30, 10, 10), 10, 10),
                VerticalOptions = LayoutOptions.Center
            };
            content.SetBinding(StackLayout.IsVisibleProperty,
                new Binding(IS_LOADING) {
                    Converter = boolInverter
                });
            main.Children.Add(content,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));

            StackLayout stackUsernamePassword = new StackLayout ()
            {
                VerticalOptions = LayoutOptions.Center
            };
            stackUsernamePassword.SetBinding(StackLayout.IsVisibleProperty, IS_USERNAME_PASSWORD);
            content.Children.Add(stackUsernamePassword);

            Entry entryUsername = new Entry ()
            {
                Placeholder = "username",
                TextColor = entryTextColor,
                PlaceholderColor = entryPlaceholderColor,
                HorizontalOptions = entryHLayoutOptions,
                HorizontalTextAlignment = entryHTextAlignment
            };
            entryUsername.SetBinding(Entry.TextProperty, "Username");
            stackUsernamePassword.Children.Add(entryUsername);

            Entry entryPassword = new Entry ()
            {
                IsPassword = true,
                Placeholder = "password",
                TextColor = entryTextColor,
                PlaceholderColor = entryPlaceholderColor,
                HorizontalOptions = entryHLayoutOptions,
                HorizontalTextAlignment = entryHTextAlignment
            };
            entryPassword.SetBinding(Entry.TextProperty, "Password");
            stackUsernamePassword.Children.Add(entryPassword);

            StackLayout stackPinCode = new StackLayout ()
            {
                VerticalOptions = LayoutOptions.Center
            };
            stackPinCode.SetBinding(StackLayout.IsVisibleProperty,
                new Binding(IS_USERNAME_PASSWORD) {
                    Converter = boolInverter
                });
            content.Children.Add(stackPinCode);

            Entry entryPinCode = new Entry ()
            {
                IsPassword = true,
                Keyboard = Keyboard.Numeric,
                Placeholder = "pincode",
                TextColor = entryTextColor,
                PlaceholderColor = entryPlaceholderColor,
                HorizontalOptions = entryHLayoutOptions,
                HorizontalTextAlignment = entryHTextAlignment
            };
            entryPinCode.SetBinding(Entry.TextProperty, "PinCode");
            stackPinCode.Children.Add(entryPinCode);

            Entry entrySiteCode = new Entry ()
            {
                Placeholder = "sitecode (optional)",
                TextColor = entryTextColor,
                PlaceholderColor = entryPlaceholderColor,
                HorizontalOptions = entryHLayoutOptions,
                HorizontalTextAlignment = entryHTextAlignment
            };
            entrySiteCode.SetBinding(Entry.TextProperty, "SiteCode");
            content.Children.Add(entrySiteCode);

            Button buttonLogin = new Button ()
            {
                Text = "Login",
                BackgroundColor = buttonColor,
                TextColor = buttonTextColor,
                HorizontalOptions = buttonHLayoutOptions
            };
            buttonLogin.SetBinding(Button.CommandProperty, "AttemptLoginCommand");
            content.Children.Add(buttonLogin);

            Button buttonToggleInput = new Button ()
            {
                FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Button)),
                Margin = new Thickness(0, -15),

                BackgroundColor = Color.Transparent,
                TextColor = buttonTextColor
            };
            buttonToggleInput.SetBinding(Button.IsVisibleProperty, "IsPinCodeAvailable");
            buttonToggleInput.SetBinding(Button.TextProperty, "ToggleInputText");
            buttonToggleInput.SetBinding(Button.CommandProperty, "ToggleInputCommand");
            content.Children.Add(buttonToggleInput);

            Label labelWarnings = new Label ()
            {
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                TextColor = labelWarningsColor,

                HorizontalOptions = labelHLayoutOptions,
                HorizontalTextAlignment = labelHTextAlignment
            };
            labelWarnings.SetBinding(Label.TextProperty, "WarningsText");
            content.Children.Add(labelWarnings);
            #endregion

            #region Loading
            StackLayout loading = new StackLayout ()
            {
                Spacing = 25,
                Padding = new Thickness(30, 0),
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            loading.SetBinding(StackLayout.IsVisibleProperty, IS_LOADING);
            main.Children.Add(loading,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));

            ActivityIndicator activityIndicator = new ActivityIndicator ()
            {
                Color = Color.White
            };
            activityIndicator.SetBinding(ActivityIndicator.IsRunningProperty, IS_LOADING);
            loading.Children.Add(activityIndicator);

            Label labelActivityIndicator = new Label ()
            {
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                TextColor = Color.White,

                HorizontalTextAlignment = labelHTextAlignment
            };
            labelActivityIndicator.SetBinding(Label.TextProperty, "ActivityText");
            loading.Children.Add(labelActivityIndicator);
            #endregion

            Content = main;
        }
    }
}
