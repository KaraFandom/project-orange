using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using ProjectOrange.Models;
using ProjectOrange.Services;
using System;

namespace ProjectOrange.Droid.Other
{
    [Activity (ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize, Label = "FilePickerActivity")]
    [Preserve (AllMembers = true)]
    public class FilePickerActivity : Activity
    {
        internal static event EventHandler<FilePickerEventArgs> FilePicked;
        internal static event EventHandler<EventArgs> Cancelled;

        public static void OnFilePicked (FilePickerEventArgs e)
        {
            FilePicked?.Invoke(null, e);
        }

        public static void OnCancelled ()
        {
            Cancelled?.Invoke(null, null);
        }

        private Context context;

        protected override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            context = Application.Context;

            Bundle bundle = (savedInstanceState ?? Intent.Extras);
            Intent intent = new Intent(Intent.ActionGetContent);
            intent.SetType("*/*");
            intent.AddCategory(Intent.CategoryOpenable);

            try
            {
                StartActivityForResult(Intent.CreateChooser(intent, "Select File"), 0);
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
            }
        }

        protected override void OnActivityResult (int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode == Result.Canceled)
            {
                OnCancelled();
                Finish();
            }
            else
            {
                try
                {
                    Android.Net.Uri uri = data.Data;
                    string fileName = FilePickerUtils.GetFileName(context, uri);
                    string filePath = FilePickerUtils.GetFilePath(context, uri);
                    if (string.IsNullOrEmpty(filePath)) filePath = uri.Path;

                    byte[] dataArray = FilePickerUtils.ReadFile(filePath);
                    OnFilePicked(new FilePickerEventArgs(dataArray, fileName, filePath));
                }
                catch (Exception ex)
                {
                    DebugService.WriteLine(ex.Message);

                    OnCancelled();
                }
                finally
                {
                    Finish();
                }
            }
        }
    }
}