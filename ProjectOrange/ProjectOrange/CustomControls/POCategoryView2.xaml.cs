﻿using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using ProjectOrange.Services;
using ProjectOrange.Views;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class POCategoryView2 : ContentView
    {
        public static readonly BindableProperty CategoryProperty = BindableProperty.Create(nameof(Category),
                                                                                           typeof(AppCategory),
                                                                                           typeof(POCategoryView2),
                                                                                           new AppCategory(),
                                                                                           BindingMode.TwoWay,
                                                                                           null,
                                                                                           (bindable, oldValue, newValue) =>
                                                                                           {
                                                                                               POCategoryView2 categoryView2 = (POCategoryView2) bindable;
                                                                                               categoryView2.Category = newValue as AppCategory;
                                                                                               categoryView2.Redraw();
                                                                                           });

        private string _filterQuery = string.Empty;
        public string FilterQuery
        {
            get { return _filterQuery; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _filterQuery = string.Empty;
                else
                    _filterQuery = value;

                Redraw();
            }
        }

        private ScrollView _scrollView = new ScrollView();
        public ScrollView ScrollView
        {
            get { return _scrollView; }
        }

        public AppCategory Category
        {
            get { return (AppCategory) GetValue(CategoryProperty); }
            set { SetValue(CategoryProperty, value); }
        }

        public POCategoryView2 ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            if (Category == null)
                return;

            #region Initialization
            string imagePathArrowBack = AppTheme.ArrowBackDefault;
            string imagePathArrowCollapse = AppTheme.ArrowCollapseDefault;
            string imagePathArrowExpand = AppTheme.ArrowExpandDefault;
            string imagePathArrowNext = AppTheme.ArrowNextDefault;
            Color separatorColor = Color.Transparent;
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);

            if (DataService.Theme != null)
            {
                if (DataService.Theme.IconArrowBackImage != null)
                    imagePathArrowBack = DataService.Theme.IconArrowBackImage.GetFileContentPath();

                if (DataService.Theme.IconArrowCollapseImage != null)
                    imagePathArrowCollapse = DataService.Theme.IconArrowCollapseImage.GetFileContentPath();

                if (DataService.Theme.IconArrowExpandImage != null)
                    imagePathArrowExpand = DataService.Theme.IconArrowExpandImage.GetFileContentPath();

                if (DataService.Theme.IconArrowNextImage != null)
                    imagePathArrowNext = DataService.Theme.IconArrowNextImage.GetFileContentPath();

                textColor = DataService.Theme.TextColorValue;
            }

            bool allowUpload = Category.AllowUploading();
            double contentTitleFontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Small, NamedSize.Default, NamedSize.Default), typeof(Label));
            double elementTitleFontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Micro, NamedSize.Small, NamedSize.Small), typeof(Label));
            Color elementTitleColor = Color.Black;
            Color elementBackgroundColor = Color.White;
            Color elementGrayBackgroundColor = new Color(0.925, 0.925, 0.925);
            Color elementOpaqueWhiteColor = new Color(1.0, 1.0, 1.0, 0.75);
            TextAlignment titleHAlign = TextAlignment.Start;
            TextAlignment titleVAlign = TextAlignment.Center;

            ScrollView main = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0
            };

            #region Content.Header
            Grid gridHeader = CreateHeaderGrid();
            content.Children.Add(gridHeader);

            Image imageHeaderBack = new Image ()
            {
                Source = imagePathArrowBack
            };
            gridHeader.Children.Add(imageHeaderBack, 1, 0);

            Label labelHeaderTitle = new Label ()
            {
                Text = Category.Name,
                TextColor = textColor,

                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            gridHeader.Children.Add(labelHeaderTitle, 2, 0);

            Image imageHeaderIcon = new Image ()
            {
                Source = Category.GetIconFileContentPath()
            };
            gridHeader.Children.Add(imageHeaderIcon, 3, 0);

            Button buttonHeaderBack = new Button ()
            {
                BackgroundColor = Color.Transparent
            };
            buttonHeaderBack.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            gridHeader.Children.Add(buttonHeaderBack, 0, 0);
            Grid.SetColumnSpan(buttonHeaderBack, 5);
            #endregion

            #region Content.Articles
            if (Category.Articles != null && Category.Articles.Count > 0)
            {
                foreach (AppArticle article in Category.Articles)
                {
                    if (article == null)
                        continue;

                    if (!article.Title.ToLower().Contains(FilterQuery))
                        continue;

                    content.Children.Add(CreateSeparator(separatorColor));

                    View articleElement = CreateArticleElementView(article, elementBackgroundColor, elementOpaqueWhiteColor, elementTitleFontSize, elementTitleColor);

                    if (articleElement != null)
                        content.Children.Add(articleElement);
                }
            }
            #endregion

            #region Content.SubCategories
            if (Category.SubCategories != null && Category.SubCategories.Count > 0)
            {
                bool hasArticles = false;
                bool hasSubCategories = false;
                bool hasNoChildren = false;
                bool hasOnlySubCategories = false;
                bool hasOnlyOneTextArticle = false;
                string subCategoryName = string.Empty;
                Color subViewColor = Color.Transparent;

                foreach (AppCategory subCategory in Category.SubCategories)
                {
                    if (subCategory == null)
                        continue;

                    if (!subCategory.Name.ToLower().Contains(FilterQuery))
                        continue;

                    hasArticles = (subCategory.Articles != null && subCategory.Articles.Count > 0);
                    hasSubCategories = (subCategory.SubCategories != null && subCategory.SubCategories.Count > 0);
                    hasNoChildren = (!hasArticles && !hasSubCategories);
                    hasOnlySubCategories = (!hasArticles && hasSubCategories);
                    hasOnlyOneTextArticle = (hasArticles && !hasSubCategories &&
                                            subCategory.Articles.Count == 1 && subCategory.Articles[0] != null &&
                                            (subCategory.Articles[0].GetArticleType().Equals(AppArticleType.Text) || subCategory.Articles[0].GetArticleType().Equals(AppArticleType.DirectText)));
                    subCategoryName = subCategory.Name;

                    subViewColor = UtilService.SafeFromHex(subCategory.ViewColor, Color.Transparent);

                    content.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 10 });

                    Grid gridContent = CreateContentGrid(subViewColor);
                    content.Children.Add(gridContent);

                    Image imageContentIcon = new Image ()
                    {
                        Source = subCategory.GetIconFileContentPath()
                    };
                    gridContent.Children.Add(imageContentIcon, 1, 0);

                    Label labelContentTitle = new Label ()
                    {
                        Text = subCategoryName,

                        FontSize = contentTitleFontSize,
                        TextColor = textColor,
                        HorizontalTextAlignment = titleHAlign,
                        VerticalTextAlignment = titleVAlign
                    };
                    gridContent.Children.Add(labelContentTitle, 3, 0);

                    Image imageContentExpandCollapse = new Image ()
                    {
                        Source = (hasNoChildren || hasOnlySubCategories) ? imagePathArrowNext : imagePathArrowExpand
                    };
                    gridContent.Children.Add(imageContentExpandCollapse, 4, 0);

                    Button buttonContent = new Button ()
                    {
                        BackgroundColor = Color.Transparent
                    };
                    gridContent.Children.Add(buttonContent, 0, 0);
                    Grid.SetColumnSpan(buttonContent, 4);

                    if (hasNoChildren || hasOnlySubCategories)
                    {
                        buttonContent.CommandParameter = subCategory;
                        buttonContent.SetBinding(Button.CommandProperty, "NavigateToSubPageCommand");
                    }
                    else
                    {
                        StackLayout stackContentBody = new StackLayout ()
                        {
                            IsVisible = false,
                            BackgroundColor = subViewColor,

                            Spacing = 0
                        };
                        content.Children.Add(stackContentBody);

                        buttonContent.Clicked += (s, e) =>
                        {
                            stackContentBody.IsVisible = !stackContentBody.IsVisible;

                            if (stackContentBody.IsVisible)
                                imageContentExpandCollapse.Source = imagePathArrowCollapse;
                            else
                                imageContentExpandCollapse.Source = imagePathArrowExpand;
                        };

                        foreach (AppArticle subCategoryArticle in subCategory.Articles)
                        {
                            if (subCategoryArticle == null)
                                continue;

                            stackContentBody.Children.Add(CreateSeparator(separatorColor));

                            View subCategoryArticleElement = CreateArticleElementView(subCategoryArticle, elementBackgroundColor, elementOpaqueWhiteColor, elementTitleFontSize, elementTitleColor);

                            if (subCategoryArticleElement != null)
                                stackContentBody.Children.Add(subCategoryArticleElement);
                        }

                        stackContentBody.Children.Add(CreateSeparator(separatorColor));

                        View subCategorySubCategoryElement = CreateSubCategoryElementView(subCategory, elementGrayBackgroundColor, elementOpaqueWhiteColor, elementTitleFontSize, elementTitleColor);

                        if (subCategorySubCategoryElement != null)
                            stackContentBody.Children.Add(subCategorySubCategoryElement);
                    }
                }
            }
            #endregion

            #region Content.LastSeparator
            content.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 10 });
            #endregion

            #region Content.Upload
            if (allowUpload)
            {
                View addArticleElement = CreateAddArticleElementView(elementGrayBackgroundColor, elementOpaqueWhiteColor, elementTitleFontSize, elementTitleColor);

                if (addArticleElement != null)
                    content.Children.Add(addArticleElement);
            }
            #endregion

            #region Content.ComingSoon
            if (!allowUpload)
            {
                if ((Category.Articles != null && Category.Articles.Count > 0) ||
                    (Category.SubCategories != null && Category.SubCategories.Count > 0))
                { }
                else
                {
                    StackLayout stackComingSoon = new StackLayout()
                    {
                        BackgroundColor = Color.Transparent,
                        Padding = new Thickness(15, 15),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    content.Children.Add(stackComingSoon);

                    Label labelComingSoon = new Label()
                    {
                        Text = "Coming Soon",
                        TextColor = textColor,
                        FontSize = contentTitleFontSize,
                        HorizontalTextAlignment = TextAlignment.Center,
                        VerticalTextAlignment = TextAlignment.Center
                    };
                    stackComingSoon.Children.Add(labelComingSoon);
                }
            }
            #endregion
            #endregion

            main.Content = content;
            _scrollView = main;
            Content = main;
        }

        private View CreateSeparator (Color separatorColor)
        {
            return new HorizontalSeparator() { Color = separatorColor };
        }

        private View CreateArticleElementView (AppArticle article, Color backgroundColor, Color tint, double titleFontSize, Color titleColor)
        {
            if (article == null)
                return null;

            bool isArticleIconAvailable = article.IsArticleIconAvailable();
            string imagePathArticle = isArticleIconAvailable ? article.GetIconFileContentPath() : article.GetFileExtensionImagePath();
            string articleTitle = article.Title;
            string articleText = (article.GetArticleType().Equals(AppArticleType.DirectText)) ? article.Value : article.TextValue;
            AppArticleType articleType = article.GetArticleType();

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
            {

            };

            Grid gridElement = CreateElementGrid();
            gridElement.BackgroundColor = backgroundColor;
            gridElement.GestureRecognizers.Add(tapGestureRecognizer);

            if (!articleType.Equals(AppArticleType.DirectText) && !articleType.Equals(AppArticleType.Text))
            {
                Image imageElementIcon = new Image ()
                {
                    Source = imagePathArticle
                };
                imageElementIcon.GestureRecognizers.Add(tapGestureRecognizer);
                if (!isArticleIconAvailable)
                {
                    imageElementIcon.Margin = new Thickness(20, 20);
                    gridElement.Children.Add(imageElementIcon, 1, 0);
                    Grid.SetRowSpan(imageElementIcon, 2);
                }
                else
                {
                    StackLayout imageElementIconStack = new StackLayout ()
                    {
                        IsClippedToBounds = true,
                        HorizontalOptions = LayoutOptions.StartAndExpand,
                        VerticalOptions = LayoutOptions.StartAndExpand
                    };
                    gridElement.Children.Add(imageElementIconStack, 0, 0);
                    Grid.SetColumnSpan(imageElementIconStack, 2);
                    Grid.SetRowSpan(imageElementIconStack, 2);

                    imageElementIcon.Aspect = Aspect.AspectFill;
                    imageElementIcon.HeightRequest = SubPage.SWidth;
                    imageElementIcon.WidthRequest = SubPage.SWidth;
                    imageElementIconStack.Children.Add(imageElementIcon);
                }
            }

            BoxView boxBackground = new BoxView ()
            {
                Color = tint
            };
            boxBackground.GestureRecognizers.Add(tapGestureRecognizer);
            gridElement.Children.Add(boxBackground, 0, 1);
            Grid.SetColumnSpan(boxBackground, 2);

            if (articleType.Equals(AppArticleType.DirectText) || articleType.Equals(AppArticleType.Text))
            {
                string textClean = Regex.Replace(articleText, "<.*?>", string.Empty);

                Label labelElementBody = new Label ()
                {
                    Text = textClean,

                    Margin = new Thickness(10, 10),
                    TextColor = titleColor,
                    FontSize = titleFontSize * 0.9,
                    HorizontalTextAlignment = TextAlignment.Start,
                    VerticalTextAlignment = TextAlignment.Start
                };
                labelElementBody.GestureRecognizers.Add(tapGestureRecognizer);
                gridElement.Children.Add(labelElementBody, 0, 0);
                Grid.SetColumnSpan(labelElementBody, 2);
            }

            Label labelElementTitle = new Label ()
            {
                Text = articleTitle,

                Margin = new Thickness(10, 0, 10, 0),
                TextColor = titleColor,
                FontSize = titleFontSize,
                HorizontalTextAlignment = TextAlignment.Start,
                VerticalTextAlignment = TextAlignment.Center
            };
            labelElementTitle.GestureRecognizers.Add(tapGestureRecognizer);
            gridElement.Children.Add(labelElementTitle, 0, 1);
            Grid.SetColumnSpan(labelElementTitle, 2);

            if (articleType.Equals(AppArticleType.Link) || articleType.Equals(AppArticleType.LinkLogin) || articleType.Equals(AppArticleType.LinkExternal) ||
                articleType.Equals(AppArticleType.LinkAutoFill) || articleType.Equals(AppArticleType.LinkCredential))
            {
                string link = article.Value;

                if (string.IsNullOrEmpty(link))
                    link = string.Empty;

                if (articleType.Equals(AppArticleType.LinkAutoFill))
                {
                    string username = DataService.Username;
                    string password = DataService.Password;

                    if (string.IsNullOrEmpty(username)) username = "username";
                    if (string.IsNullOrEmpty(password)) password = "password";

                    tapGestureRecognizer.CommandParameter = new AppLinkFill { Title = articleTitle, Value = link, Username = username, Password = password };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebFillCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkCredential))
                {
                    string domain = DataService.BrowserCredentials.Domain;
                    string username = DataService.BrowserCredentials.Username;
                    string password = DataService.BrowserCredentials.Password;

                    if (string.IsNullOrEmpty(username)) domain = string.Empty;
                    if (string.IsNullOrEmpty(username)) username = "username";
                    if (string.IsNullOrEmpty(password)) password = "password";

                    tapGestureRecognizer.CommandParameter = new AppLinkFill { Title = articleTitle, Value = link, Username = (domain + username), Password = password };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebFillCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkExternal))
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebExternalCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkLogin))
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToAuthenticatingWebCommand");
                }
                else
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebCommand");
                }
            }
            else if (articleType.Equals(AppArticleType.DirectText) || articleType.Equals(AppArticleType.Text))
            {
                tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = articleText };
                tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToTextPageCommand");
            }
            else
            {
                tapGestureRecognizer.CommandParameter = article;
                tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToFileAttachmentCommand");
            }

            return gridElement;
        }

        private View CreateSubCategoryElementView (AppCategory category, Color backgroundColor, Color tint, double titleFontSize, Color titleColor)
        {
            if (category == null)
                return null;

            string imagePathArticle = AppTheme.FileTypeIconMore;
            string title = "See More.";

            Grid gridElement = CreateElementGrid();
            gridElement.BackgroundColor = backgroundColor;

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
            {

            };

            Image imageElementIcon = new Image ()
            {
                Source = imagePathArticle
            };
            imageElementIcon.Margin = new Thickness(20, 20);
            gridElement.Children.Add(imageElementIcon, 1, 0);
            Grid.SetRowSpan(imageElementIcon, 2);

            BoxView boxBackground = new BoxView ()
            {
                Color = tint
            };
            boxBackground.GestureRecognizers.Add(tapGestureRecognizer);
            gridElement.Children.Add(boxBackground, 0, 1);
            Grid.SetColumnSpan(boxBackground, 2);

            Label labelElementTitle = new Label ()
            {
                Text = title,

                Margin = new Thickness(10, 0, 10, 0),
                TextColor = titleColor,
                FontSize = titleFontSize,
                HorizontalTextAlignment = TextAlignment.Start,
                VerticalTextAlignment = TextAlignment.Center
            };
            labelElementTitle.GestureRecognizers.Add(tapGestureRecognizer);
            gridElement.Children.Add(labelElementTitle, 0, 1);
            Grid.SetColumnSpan(labelElementTitle, 2);

            tapGestureRecognizer.CommandParameter = category;
            tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToSubPageCommand");

            return gridElement;
        }

        private View CreateAddArticleElementView (Color backgroundColor, Color tint, double titleFontSize, Color titleColor)
        {
            string imagePathArticle = AppTheme.FileTypeIconUpload;
            string title = "Add Article.";

            Grid gridElement = CreateElementGrid();
            gridElement.BackgroundColor = backgroundColor;

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
            {

            };

            Image imageElementIcon = new Image ()
            {
                Source = imagePathArticle
            };
            imageElementIcon.Margin = new Thickness(20, 20);
            gridElement.Children.Add(imageElementIcon, 1, 0);
            Grid.SetRowSpan(imageElementIcon, 2);

            BoxView boxBackground = new BoxView ()
            {
                Color = tint
            };
            boxBackground.GestureRecognizers.Add(tapGestureRecognizer);
            gridElement.Children.Add(boxBackground, 0, 1);
            Grid.SetColumnSpan(boxBackground, 2);

            Label labelElementTitle = new Label ()
            {
                Text = title,

                Margin = new Thickness(10, 0, 10, 0),
                TextColor = titleColor,
                FontSize = titleFontSize,
                HorizontalTextAlignment = TextAlignment.Start,
                VerticalTextAlignment = TextAlignment.Center
            };
            labelElementTitle.GestureRecognizers.Add(tapGestureRecognizer);
            gridElement.Children.Add(labelElementTitle, 0, 1);
            Grid.SetColumnSpan(labelElementTitle, 2);

            tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToUploadPageCommand");

            return gridElement;
        }

        private Grid CreateHeaderGrid ()
        {
            int headerHeight = Device.OnPlatform(55, 65, 65);

            Grid headerGrid = new Grid ()
            {
                BackgroundColor = Color.Transparent,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(40) },
                    new ColumnDefinition { Width = new GridLength(25) },
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(headerHeight) }
                }
            };

            return headerGrid;
        }

        private Grid CreateContentGrid (Color viewColor)
        {
            Grid contentGrid = new Grid ()
            {
                BackgroundColor = viewColor,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(40) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(25) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(50) }
                }
            };

            return contentGrid;
        }

        private Grid CreateElementGrid ()
        {
            Grid elementGrid = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(0.6, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(0.4, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(85) },
                    new RowDefinition { Height = new GridLength(35) }
                }
            };

            return elementGrid;
        }
    }
}
