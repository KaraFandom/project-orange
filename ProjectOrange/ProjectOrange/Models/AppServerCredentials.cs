﻿using ProjectOrange.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectOrange.Models
{
    public class AppServerCredentials
    {
        private const string USERNAME_KEY = "AppServerCredentials.Username";
        private const string PASSWORD_KEY = "AppServerCredentials.Password";
        private const string USERNAME_DEFAULT = "mobileappuser";
        private const string PASSWORD_DEFAULT = "Xchangebiz2016";

        private bool _isInitialized = false;
        private string _username = USERNAME_DEFAULT;
        private string _password = PASSWORD_DEFAULT;

        public bool IsInitialized { get { return _isInitialized; } }
        public string Username { get { return _username; } }
        public string Password { get { return _password; } }

        public bool HasUsername { get { return !string.IsNullOrEmpty(Username); } }
        public bool HasPassword { get { return !string.IsNullOrEmpty(Password); } }
        public bool IsValid { get { return (HasUsername && HasPassword); } }

        public AppServerCredentials ()
        {
            _username = USERNAME_DEFAULT;
            _password = PASSWORD_DEFAULT;
        }
        public AppServerCredentials (string username, string password)
        {
            _username = username;
            _password = password;
        }

        public void TryInitialize (bool reloadAlways = false)
        {
            if (reloadAlways || !IsInitialized)
                Load();
        }

        public void Load ()
        {
            string username = (string) DataService.LoadAppProperty(USERNAME_KEY, string.Empty);
            string password = (string) DataService.LoadAppProperty(PASSWORD_KEY, string.Empty);

            if (!username.IsNullOrEmpty() && !password.IsNullOrEmpty())
            {
                _username = username;
                _password = password;

                _isInitialized = true;
            }
            else
            {
                _username = USERNAME_DEFAULT;
                _password = PASSWORD_DEFAULT;

                _isInitialized = false;
            }
        }

        public async Task<bool> Clear ()
        {
            try
            {
                List<Tuple<string, object>> properties = new List<Tuple<string, object>>();
                properties.Add(new Tuple<string, object>(USERNAME_KEY, USERNAME_DEFAULT));
                properties.Add(new Tuple<string, object>(PASSWORD_KEY, PASSWORD_DEFAULT));

                await DataService.SaveAppProperty(properties.ToArray());

                _isInitialized = false;
                _username = USERNAME_DEFAULT;
                _password = PASSWORD_DEFAULT;

                return true;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<bool> Save (string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;

            try
            {
                List<Tuple<string, object>> properties = new List<Tuple<string, object>>();
                properties.Add(new Tuple<string, object>(USERNAME_KEY, username));
                properties.Add(new Tuple<string, object>(PASSWORD_KEY, password));

                await DataService.SaveAppProperty(properties.ToArray());
                Load();

                return true;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
