using ProjectOrange.CustomViews;
using ProjectOrange.iOS.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(RoundedFrame), typeof(RoundedFrameRenderer))]
namespace ProjectOrange.iOS.CustomRenderers
{
    public class RoundedFrameRenderer : FrameRenderer
    {
        private RoundedFrame roundedFrame;

        protected override void OnElementChanged (ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null || (e.NewElement as RoundedFrame == null))
                return;

            roundedFrame = e.NewElement as RoundedFrame;
            roundedFrame.OnUpdateCornerRadius += UpdateCornerRadius;

            UpdateCornerRadius();
        }

        private void UpdateCornerRadius ()
        {
            if (NativeView != null && roundedFrame != null)
                NativeView.Layer.CornerRadius = roundedFrame.CornerRadius;
        }
    }
}