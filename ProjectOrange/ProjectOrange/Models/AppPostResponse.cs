﻿namespace ProjectOrange.Models
{
    public class AppPostResponse
    {
        public string ErrorMessage { get; set; }
        public string InnerException { get; set; }
        public string IsSuccessful { get; set; }

        public bool Success { get { return (IsSuccessful.ToUpper().Equals("TRUE")) ? true : false; } }

        public AppPostResponse ()
        {
            ErrorMessage = string.Empty;
            InnerException = string.Empty;
            IsSuccessful = string.Empty;
        }
    }
}
