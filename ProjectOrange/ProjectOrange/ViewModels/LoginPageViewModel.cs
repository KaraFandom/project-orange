﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Models;
using ProjectOrange.Services;
using System;
using System.Threading.Tasks;

namespace ProjectOrange.ViewModels
{
    public class LoginPageViewModel : BindableBase, INavigationAware
    {

        private const string USERNAME_KEY = "username";
        private const string PASSWORD_KEY = "password";

        private INavigationService _navigationService;

        public DelegateCommand AttemptLoginCommand { get; set; }

        public DelegateCommand ToggleInputCommand { get; set; }

        private bool _isLoading = false;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }

        private bool _isPinCodeAvailable = false;
        public bool IsPinCodeAvailable
        {
            get { return _isPinCodeAvailable; }
            set { SetProperty(ref _isPinCodeAvailable, value); }
        }

        private bool _isUsernamePassword = true;
        public bool IsUsernamePassword
        {
            get { return _isUsernamePassword; }
            set { SetProperty(ref _isUsernamePassword, value); }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _siteCode;
        public string SiteCode
        {
            get { return _siteCode; }
            set { SetProperty(ref _siteCode, value); }
        }

        private string _pinCode;
        public string PinCode
        {
            get { return _pinCode; }
            set { SetProperty(ref _pinCode, value); }
        }

        private string _toggleInputText = "Use Pin Code";
        public string ToggleInputText
        {
            get { return _toggleInputText; }
            set { SetProperty(ref _toggleInputText, value); }
        }

        private string _warningsText;
        public string WarningsText
        {
            get { return _warningsText; }
            set { SetProperty(ref _warningsText, value); }
        }

        private string _activityText;
        public string ActivityText
        {
            get { return _activityText; }
            set { SetProperty(ref _activityText, value); }
        }

        private string SavedUsername;
        private string SavedPassword;
        private string SavedPinCode;

        public LoginPageViewModel (INavigationService navigationService)
        {
            _navigationService = navigationService;
            AttemptLoginCommand = new DelegateCommand(AttemptLogin);
            ToggleInputCommand = new DelegateCommand(ToggleInput);

            InitializePinCode();
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo (NavigationParameters parameters)
        {
            if (parameters.ContainsKey(USERNAME_KEY) && parameters.ContainsKey(PASSWORD_KEY))
            {
                string username = parameters[USERNAME_KEY] as string;
                string password = parameters[PASSWORD_KEY] as string;

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    Username = username;
                    Password = password;

                    AttemptLogin();
                }
            }
        }

        private void AttemptLogin ()
        {
            if (IsUsernamePassword)
            {
                if (string.IsNullOrEmpty(Username))
                {
                    WarningsText = "Username field is empty.";
                    return;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(PinCode))
                {
                    WarningsText = "Pincode field is empty.";
                    return;
                }

                InitializePinCode();

                if (!IsPinCodeAvailable)
                {
                    WarningsText = "Pincode has expired.";
                    IsUsernamePassword = true;
                    return;
                }
                else if (PinCode != SavedPinCode)
                {
                    WarningsText = "Incorrect Pincode.";
                    return;
                }
                else
                {
                    Username = SavedUsername;
                    Password = SavedPassword;
                }
            }

            WarningsText = string.Empty;
            Login();
        }

        private void ToggleInput ()
        {
            IsUsernamePassword = !IsUsernamePassword;

            if (IsUsernamePassword)
                ToggleInputText = "Use Pin Code";
            else
                ToggleInputText = "Use Username & Password";

            WarningsText = string.Empty;
        }

        private void InitializePinCode ()
        {
            DataService.UserCredentials.TryInitialize(true);
            IsPinCodeAvailable = DataService.UserCredentials.IsPinCodeValid;

            if (IsPinCodeAvailable)
            {
                SavedUsername = DataService.UserCredentials.Username;
                SavedPassword = DataService.UserCredentials.Password;
                SavedPinCode = DataService.UserCredentials.PinCode;
            }
        }

        private async void Login ()
        {
            if (IsLoading)
                return;

            IsLoading = true;

            WarningsText = string.Empty;
            ActivityText = "Logging in";

            
            AppUser user = await DataService.Authenticate(Username, Password, SiteCode);

            if (user != null)
            {
                if (!string.IsNullOrEmpty(user.ErrorMessage))
                    WarningsText = user.ErrorMessage;
                else
                {
                    await RevalidatePinCode(Username, Password, PinCode);

                    await DataService.LoadTheme((s) => { ActivityText = s; });

                    await NavigateToMainPage();
                }
            }

            IsLoading = false;
        }

        private async Task RevalidatePinCode (string username, string password, string pinCode)
        {
            if (SavedUsername != username ||
                SavedPassword != password ||
                SavedPinCode != pinCode)
            {
                IsPinCodeAvailable = false;
                SavedUsername = string.Empty;
                SavedPassword = string.Empty;
                SavedPinCode = string.Empty;

                await DataService.UserCredentials.Clear();
            }
        }

        private async Task NavigateToMainPage ()
        {
            string pageName = "MainPage";
            await _navigationService.NavigateAsync(new Uri(DataService.AppURL + DataService.GetMasterDetailPageUriString(pageName) + pageName, UriKind.Absolute));
        }
    }
}
