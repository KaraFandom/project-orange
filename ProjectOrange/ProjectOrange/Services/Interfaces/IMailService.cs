﻿using System;

namespace ProjectOrange.Services.Interfaces
{
    public interface IMailService
    {
        void Compose (string[] to, string[] cc, string[] bcc, string subject = null, string body = null, bool isHtml = false, string[] attachments = null, Action<bool> completed = null);
    }
}
