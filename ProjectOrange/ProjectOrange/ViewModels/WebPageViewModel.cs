﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Models;
using ProjectOrange.Services.Interfaces;
using System;
using Xamarin.Forms;

namespace ProjectOrange.ViewModels
{
    public class WebPageViewModel : BindableBase, INavigationAware
    {
        private INavigationService _navigationService;

        public DelegateCommand MailCommand { get; set; }

        public DelegateCommand NavigateBackCommand { get; set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _source;
        public string Source
        {
            get { return _source; }
            set { SetProperty(ref _source, value); }
        }

        public WebPageViewModel (INavigationService navigationService)
        {
            _navigationService = navigationService;

            NavigateBackCommand = new DelegateCommand(NavigateBack);
            MailCommand = new DelegateCommand(Mail);
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        { }

        public void OnNavigatedTo (NavigationParameters parameters)
        {
            if (parameters.ContainsKey("source"))
            {
                AppLink link = parameters["source"] as AppLink;

                if (link != null && !string.IsNullOrEmpty(link.Value))
                {
                    Title = (!string.IsNullOrEmpty(link.Title)) ? link.Title : link.Value;

                    bool success = false;
                    bool http = false;
                    Uri uri;

                    success = Uri.TryCreate(link.Value, UriKind.Absolute, out uri);

                    if (success)
                        http = (uri.Scheme.Equals("http") || uri.Scheme.Equals("https"));

                    if (success && http)
                    {
                        Source = uri.AbsoluteUri;
                    }
                }
            }
        }

        private void Mail ()
        {
            IMailService mailService = DependencyService.Get<IMailService>();

            if (mailService != null)
            {
                if (!string.IsNullOrEmpty(Source))
                {
                    string htmlBody = ("<a href=\"" + Source + "\" target=\"_blank\">" + ((!string.IsNullOrEmpty(Title) ? Title : Source)) + "</a>");

                    mailService.Compose(null,
                                        null,
                                        null,
                                        null,
                                        htmlBody,
                                        true,
                                        null,
                                        null);
                }
            }
        }

        private void NavigateBack ()
        {
            _navigationService.GoBackAsync();
        }
    }
}
