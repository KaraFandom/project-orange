﻿using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using ProjectOrange.Services;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class POCategoriesViewList : ContentView
    {
        public static readonly BindableProperty CategoriesProperty = BindableProperty.Create(nameof(Categories),
                                                                                             typeof(List<AppCategory>),
                                                                                             typeof(POCategoriesViewList),
                                                                                             new List<AppCategory>(),
                                                                                             BindingMode.TwoWay,
                                                                                             null,
                                                                                             (bindable, oldValue, newValue) =>
                                                                                             {
                                                                                                 POCategoriesViewList categoriesViewList = (POCategoriesViewList) bindable;
                                                                                                 categoriesViewList.Redraw();
                                                                                             });

        private string _filterQuery = string.Empty;
        public string FilterQuery
        {
            get { return _filterQuery; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _filterQuery = string.Empty;
                else
                    _filterQuery = value;

                Redraw();
            }
        }

        private ScrollView _scrollView = new ScrollView();
        public ScrollView ScrollView
        {
            get { return _scrollView; }
        }

        public List<AppCategory> Categories
        {
            get { return (List<AppCategory>) GetValue(CategoriesProperty); }
            set { SetValue(CategoriesProperty, value); }
        }

        public List<AppCategory> FilteredCategories
        {
            get { return Categories.Where(x => x.Name.ToLower().Contains(FilterQuery.ToLower())).OrderBy(x => x.SortOrder).ToList(); }
        }

        public POCategoriesViewList ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            if (Categories == null || Categories.Count == 0)
                return;

            #region Initialization
            int listEntryHeight = 75;
            string imagePathArrowNext = AppTheme.ArrowNextDefault;
            Color separatorColor = Color.FromHex(AppTheme.SeperatorColorDefault);
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);
            TextAlignment alignmentTitleHorizontal = TextAlignment.Center;
            TextAlignment alignmentTitleVertical = TextAlignment.Center;

            if (DataService.Theme != null)
            {
                if (DataService.Theme.IconArrowNextImage != null)
                    imagePathArrowNext = DataService.Theme.IconArrowNextImage.GetFileContentPath();

                separatorColor = DataService.Theme.SeperatorColorValue;

                textColor = DataService.Theme.TextColorValue;
            }

            ScrollView main = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0
            };

            bool isFirstCategory = true;
            string imagePathCategoryIcon = string.Empty;
            Color categoryWindowColor;
            foreach (AppCategory category in FilteredCategories)
            {
                if (category == null)
                    continue;

                if (isFirstCategory)
                    isFirstCategory = false;
                else
                    content.Children.Add(new HorizontalSeparator () { Color = separatorColor });

                categoryWindowColor = UtilService.SafeFromHex(category.ViewColor, Color.Transparent);

                imagePathCategoryIcon = category.GetIconFileContentPath();

                Grid gridCategory = new Grid ()
                {
                    BackgroundColor = categoryWindowColor,

                    ColumnSpacing = 0,
                    RowSpacing = 0,

                    ColumnDefinitions =
                    {
                        new ColumnDefinition { Width = new GridLength(25) },
                        new ColumnDefinition { Width = new GridLength(listEntryHeight) },
                        new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition { Width = new GridLength(20) },
                        new ColumnDefinition { Width = new GridLength(25) },
                    },
                    RowDefinitions =
                    {
                        new RowDefinition { Height = new GridLength(listEntryHeight) }
                    }
                };
                content.Children.Add(gridCategory);

                Image imageCategoryIcon = new Image ()
                {
                    Source = imagePathCategoryIcon
                };
                gridCategory.Children.Add(imageCategoryIcon, 1, 0);

                Label labelCategoryTitle = new Label ()
                {
                    Text = category.Name,
                    TextColor = textColor,
                    HorizontalTextAlignment = alignmentTitleHorizontal,
                    VerticalTextAlignment = alignmentTitleVertical
                };
                gridCategory.Children.Add(labelCategoryTitle, 2, 0);

                Image imageCategoryNext = new Image ()
                {
                    Source = imagePathArrowNext
                };
                gridCategory.Children.Add(imageCategoryNext, 3, 0);

                Button buttonCategory = new Button ()
                {
                    BackgroundColor = Color.Transparent,
                    CommandParameter = category
                };
                buttonCategory.SetBinding(Button.CommandProperty, "NavigateToSubPageCommand");
                gridCategory.Children.Add(buttonCategory, 0, 0);
                Grid.SetColumnSpan(buttonCategory, 5);
            }
            #endregion

            main.Content = content;
            _scrollView = main;
            Content = main;
        }
    }
}
