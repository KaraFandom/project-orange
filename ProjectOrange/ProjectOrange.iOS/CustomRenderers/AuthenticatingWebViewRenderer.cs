using Foundation;
using ObjCRuntime;
using ProjectOrange.CustomViews;
using ProjectOrange.iOS.CustomRenderers;
using ProjectOrange.iOS.DependencyServices;
using Security;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(AuthenticatingWebView), typeof(AuthenticatingWebViewRenderer))]
namespace ProjectOrange.iOS.CustomRenderers
{
    public class AuthenticatingWebViewRenderer : WebViewRenderer
    {
        private string _lastUrl;
        private WebViewSource _lastSource;
        private WebNavigationEvent _lastNavigationEvent;

        public new AuthenticatingWebView Element { get { return (AuthenticatingWebView) base.Element; } }

        protected override void OnElementChanged (VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            if (!(Delegate is AuthenticatingWebViewDelegate))
            {
                NSObject orginalDelegate = (NSObject) Delegate;

                Delegate = null;
                Delegate = new AuthenticatingWebViewDelegate(this, orginalDelegate);
            }

            if (e.OldElement != null)
                ((WebView) e.OldElement).Navigating -= HandleElementNavigating;

            if (e.NewElement != null)
                ((WebView) e.NewElement).Navigating += HandleElementNavigating;
        }

        private void HandleElementNavigating (object sender, WebNavigatingEventArgs e)
        {
            _lastUrl = e.Url;
            _lastSource = e.Source;
            _lastNavigationEvent = e.NavigationEvent;
        }

        private class AuthenticatingWebViewDelegate : UIWebViewDelegate, INSUrlConnectionDelegate
        {
            private readonly AuthenticatingWebViewRenderer _renderer;
            private readonly NSObject _originalDelegate;

            private NSUrlRequest _request;

            public AuthenticatingWebViewDelegate (AuthenticatingWebViewRenderer renderer, NSObject originalDelegate)
            {
                _renderer = renderer;
                _originalDelegate = originalDelegate;
            }

            public override void LoadFailed (UIWebView webView, NSError error)
            {
                // base.LoadFailed(webView, error);

                if (!error.Domain.Equals("WebKitErrorDomain") ||
                    !error.Code.Equals(102))
                    ForwardDelegateMethod("webView:didFailLoadWithError:", webView, error);
            }

            public override void LoadingFinished (UIWebView webView)
            {
                // base.LoadingFinished(webView);

                ForwardDelegateMethod("webViewDidFinishLoad:", webView);
            }

            public override void LoadStarted (UIWebView webView)
            {
                // base.LoadStarted(webView);

                ForwardDelegateMethod("webViewDidStartLoad:", webView);
            }

            public override bool ShouldStartLoad (UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
            {
                // return base.ShouldStartLoad(webView, request, navigationType);

                if (_request != null)
                {
                    _request = null;
                    return true;
                }

                bool originalResult = ForwardDelegatePredicate("webView:shouldStartLoadWithRequest:navigationType:", webView, request, (int) navigationType, true);

                if (_renderer != null)
                {
                    if (_renderer.Element.ShouldTrustUnknownCertificate != null)
                    {
                        if (originalResult)
                        {
                            _request = request;

                            new NSUrlConnection(request, this, true);
                        }

                        return false;
                    }
                }

                return originalResult;
            }

            private void SendFailedNavigation ()
            {
                SendNavigated(new WebNavigatedEventArgs(_renderer._lastNavigationEvent, _renderer._lastSource, _renderer._lastUrl, WebNavigationResult.Failure));
            }

            [Export("connection:didFailWithError:")]
            private void ConnectionFailed (NSUrlConnection connection, NSError error)
            {
                SendFailedNavigation();
            }

            [Export("connection:didReceiveResponse:")]
            private void ReceivedResponse (NSUrlConnection connection, NSUrlResponse response)
            {
                connection.Cancel();

                if (_request != null)
                    _renderer.LoadRequest(_request);
            }

            [Export("connection:willSendRequestForAuthenticationChallenge:")]
            private void WillSendRequestForAuthenticationChallenge (NSUrlConnection connection, NSUrlAuthenticationChallenge challenge)
            {
                if (challenge.ProtectionSpace.AuthenticationMethod == NSUrlProtectionSpace.AuthenticationMethodNTLM)
                {
                    string username;
                    string password;

                    UIAlertView alert = new UIAlertView();
                    alert.Title = "Login Required";
                    alert.AddButton("OK");
                    alert.AddButton("Cancel");
                    alert.AlertViewStyle = UIAlertViewStyle.LoginAndPasswordInput;
                    alert.Clicked += (object s, UIButtonEventArgs e) =>
                    {
                        if (e.ButtonIndex == 0)
                        {
                            username = alert.GetTextField(0).Text;
                            password = alert.GetTextField(1).Text;

                            challenge.Sender.UseCredential(new NSUrlCredential(username, password, NSUrlCredentialPersistence.Permanent), challenge);
                            challenge.Sender.PerformDefaultHandling(challenge);
                        }
                        else
                        {
                            challenge.Sender.CancelAuthenticationChallenge(challenge);
                            SendFailedNavigation();
                        }
                    };

                    alert.Show();
                }
                else if (challenge.ProtectionSpace.AuthenticationMethod == NSUrlProtectionSpace.AuthenticationMethodServerTrust)
                {
                    SecTrust trust = challenge.ProtectionSpace.ServerSecTrust;
                    SecTrustResult result = trust.Evaluate();
                    bool trustedCertificate = result == SecTrustResult.Proceed || result == SecTrustResult.Unspecified;

                    if (!trustedCertificate && trust.Count != 0)
                    {
                        X509Certificate2 originalCertificate = trust[0].ToX509Certificate2();
                        AuthenticatingWebViewCertificate x509Certificate = new AuthenticatingWebViewCertificate(challenge.ProtectionSpace.Host, originalCertificate);
                        trustedCertificate = _renderer.Element.ShouldTrustUnknownCertificate(x509Certificate);
                    }

                    if (trustedCertificate)
                    {
                        challenge.Sender.UseCredential(new NSUrlCredential(trust), challenge);
                        challenge.Sender.PerformDefaultHandling(challenge);
                    }
                    else
                    {
                        challenge.Sender.CancelAuthenticationChallenge(challenge);
                        SendFailedNavigation();
                    }
                }
                else
                {
                    challenge.Sender.PerformDefaultHandling(challenge);
                }
            }

            #region Wrappers
            [DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
            private static extern bool void_objc_msgSend_IntPtr (IntPtr receiver, IntPtr selector, IntPtr arg1);

            [DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
            private static extern bool void_objc_msgSend_IntPtr_IntPtr (IntPtr receiver, IntPtr selector, IntPtr arg1, IntPtr arg2);

            [DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
            private static extern bool bool_objc_msgSend_IntPtr_IntPtr_int (IntPtr receiver, IntPtr selector, IntPtr arg1, IntPtr arg2, int arg3);

            private void ForwardDelegateMethod (string selectorName, NSObject arg1)
            {
                Selector sel = new Selector(selectorName);

                if (_originalDelegate != null && _originalDelegate.RespondsToSelector(sel))
                    void_objc_msgSend_IntPtr(_originalDelegate.Handle, sel.Handle, arg1 != null ? arg1.Handle : IntPtr.Zero);
            }

            private void ForwardDelegateMethod (string selectorName, NSObject arg1, NSObject arg2)
            {
                Selector sel = new Selector(selectorName);

                if (_originalDelegate != null && _originalDelegate.RespondsToSelector(sel))
                    void_objc_msgSend_IntPtr_IntPtr(_originalDelegate.Handle, sel.Handle, arg1 != null ? arg1.Handle : IntPtr.Zero, arg2 != null ? arg2.Handle : IntPtr.Zero);
            }

            private bool ForwardDelegatePredicate (string selectorName, NSObject arg1, NSObject arg2, int arg3, bool defaultResult)
            {
                var sel = new Selector(selectorName);

                if (_originalDelegate != null && _originalDelegate.RespondsToSelector(sel))
                    return bool_objc_msgSend_IntPtr_IntPtr_int(_originalDelegate.Handle, sel.Handle, arg1 != null ? arg1.Handle : IntPtr.Zero, arg2 != null ? arg2.Handle : IntPtr.Zero, arg3);
                else
                    return defaultResult;
            }
            #endregion

            #region Reflection
            private delegate void SendNavigatedDelegate (WebNavigatedEventArgs e);

            private MethodInfo _sendNavigatedMethodInfo;
            private MethodInfo SendNavigatedMethodInfo
            {
                get
                {
                    if (_sendNavigatedMethodInfo == null)
                    {
                        _sendNavigatedMethodInfo = typeof(WebView).GetMethod(name: "SendNavigated",
                                                                             bindingAttr: BindingFlags.Instance | BindingFlags.NonPublic,
                                                                             binder: null,
                                                                             types: new[] { typeof(WebNavigatedEventArgs) },
                                                                             modifiers: null);
                    }

                    return _sendNavigatedMethodInfo;
                }
            }

            private void SendNavigated (WebNavigatedEventArgs e)
            {
                MethodInfo methodInfo = SendNavigatedMethodInfo;

                if (methodInfo != null)
                {
                    SendNavigatedDelegate methodDelegate = (SendNavigatedDelegate) methodInfo.CreateDelegate(typeof(SendNavigatedDelegate), _renderer.Element);

                    if (methodDelegate != null)
                        methodDelegate(e);
                }
            }
            #endregion
        }
    }
}