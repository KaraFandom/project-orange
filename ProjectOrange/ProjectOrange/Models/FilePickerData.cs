﻿namespace ProjectOrange.Models
{
    public class FilePickerData
    {
        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set {_fileName = value; }
        }

        private string _filePath;
        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; }
        }

        private byte[] _dataArray;
        public byte[] DataArray
        {
            get { return _dataArray; }
            set { _dataArray = value; }
        }

        public FilePickerData (byte[] dataArray, string fileName, string filePath)
        {
            _dataArray = dataArray;
            _fileName = fileName;
            _filePath = filePath;
        }
    }
}
