﻿using ProjectOrange.CustomControls;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class ImagePage : ContentPage
    {
        public ImagePage()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Content
            StackLayout main = new StackLayout ()
            {
                BackgroundColor = Color.Black,
                Spacing = 0
            };

            PONavBar navBar = new PONavBar ()
            {
                UseBanner = true,
                UseButtonRight = true
            };
            navBar.Redraw();
            navBar.ButtonLeft.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            navBar.ButtonRight.SetBinding(Button.CommandProperty, "MailCommand");
            main.Children.Add(navBar);

            Image image = new Image ()
            {
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            image.SetBinding(Image.SourceProperty, "Source");
            main.Children.Add(image);
            #endregion

            Content = main;
        }
    }
}
