﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Events;
using ProjectOrange.Models;
using ProjectOrange.Services;
using System;
using Xamarin.Forms;

namespace ProjectOrange.ViewModels
{
    public class POMasterDetailPageViewModel : BindableBase, INavigationAware
    {
        private IEventAggregator _eventAggregator;

        private INavigationService _navigationService;

        public DelegateCommand<string> NavigateCommand { get; set; }

        public DelegateCommand NavigateToHomePageCommand { get; set; }

        public DelegateCommand<string> NavigateToSidePageCommand { get; set; }

        public DelegateCommand NavigateToSettingsPageCommand { get; set; }

        public DelegateCommand LogoutCommand { get; set; }

        private bool _isPresented;
        public bool IsPresented
        {
            get { return _isPresented; }
            set { SetProperty(ref _isPresented, value); }
        }

        private string _detailPageName;
        public string DetailPageName
        {
            get { return _detailPageName; }
            set { SetProperty(ref _detailPageName, value); }
        }

        private AppTheme _theme;
        public AppTheme Theme
        {
            get { return _theme; }
            set { SetProperty(ref _theme, value); }
        }

        public POMasterDetailPageViewModel (IEventAggregator eventAggregator, INavigationService navigationService)
        {
            _eventAggregator = eventAggregator;
            _navigationService = navigationService;

            _eventAggregator.GetEvent<TogglePOMasterDetailPageEvent>().Subscribe(Toggle);

            NavigateCommand = new DelegateCommand<string>(Navigate);
            NavigateToHomePageCommand = new DelegateCommand(NavigateToHomePage);
            NavigateToSidePageCommand = new DelegateCommand<string>(NavigateToSidePage);
            NavigateToSettingsPageCommand = new DelegateCommand(NavigateToSettingsPage);
            LogoutCommand = new DelegateCommand(Logout);

            Initialize();
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo (NavigationParameters parameters)
        {
            if (parameters.ContainsKey("detailpagename"))
                DetailPageName = parameters["detailpagename"] as string;
        }

        private void Initialize ()
        {
            if (DataService.Theme != null)
                Theme = DataService.Theme;
        }

        private void Toggle (TogglePOMasterDetailPagePayload obj)
        {
            if (obj == null)
                return;

            if (!obj.DetailPageName.ToLower().Equals(DetailPageName.ToLower()))
                return;

            IsPresented = obj.IsPresented;
        }

        private async void Navigate (string obj)
        {
            if (string.IsNullOrEmpty(obj))
                return;

            IsPresented = false;

            // HACK: A fix on a known Xamarin bug on iOS Tablets: Xamarin.Forms.Platform.iOS.TabletMasterDetailRenderer.ViewDidDisappear.
            if (Device.OS == TargetPlatform.iOS && Device.Idiom == TargetIdiom.Tablet && DetailPageName == "SubPage")
                await _navigationService.GoBackAsync(animated: false);

            await _navigationService.NavigateAsync(new Uri(DataService.AppURL + obj, UriKind.Absolute));
        }

        private async void NavigateToHomePage ()
        {
            IsPresented = false;

            string pageName = "MainPage";

            await _navigationService.NavigateAsync(new Uri(DataService.AppURL + DataService.GetMasterDetailPageUriString(pageName) + pageName, UriKind.Absolute));
        }

        private async void NavigateToSidePage (string obj)
        {
            if (string.IsNullOrEmpty(obj))
                return;

            IsPresented = false;

            string pageName = "SidePage";
            string title = "";
            string body = "";

            if (Theme != null)
            {
                switch (obj.ToUpper())
                {
                    case "ABOUT":
                        title = Theme.AboutTitle;
                        body = Theme.AboutContent;
                        break;

                    case "CONTACT":
                        title = Theme.ContactTitle;
                        body = Theme.ContactContent;
                        break;

                    case "FACTS":
                        title = Theme.FactsTitle;
                        body = Theme.FactsContent;
                        break;
                }

                DataService.Theme.SidePageTitle = title;
                DataService.Theme.SidePageContent = body;
            }

            await _navigationService.NavigateAsync(new Uri(DataService.AppURL + DataService.GetMasterDetailPageUriString(pageName) + pageName, UriKind.Absolute),
                                                   new NavigationParameters { { "title", title }, { "body", body } });
        }

        private async void NavigateToSettingsPage ()
        {
            IsPresented = false;

            string pageName = "SettingsPage";

            await _navigationService.NavigateAsync(new Uri(DataService.AppURL + DataService.GetMasterDetailPageUriString(pageName) + pageName, UriKind.Absolute));
        }

        private void Logout ()
        {
            IsPresented = false;

            string pageName = "LoginPage";

            _navigationService.NavigateAsync(new Uri(DataService.AppURL + pageName, UriKind.Absolute));
        }
    }
}
