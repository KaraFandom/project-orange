using ProjectOrange.Effects;
using ProjectOrange.Droid.CustomRenderers;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("FandomInc")]
[assembly: ExportEffect(typeof(LabelShadowEffectRenderer), "LabelShadowEffect")]
namespace ProjectOrange.Droid.CustomRenderers
{
    public class LabelShadowEffectRenderer : PlatformEffect
    {
        protected override void OnAttached ()
        {
            try
            {
                Android.Widget.TextView control = Control as Android.Widget.TextView;
                LabelShadowEffect effect = (LabelShadowEffect) Element.Effects.FirstOrDefault(e => e is LabelShadowEffect);

                if (effect != null)
                {
                    float radius = effect.Radius;
                    float distanceX = effect.DistanceX;
                    float distanceY = effect.DistanceY;
                    Android.Graphics.Color color = effect.Color.ToAndroid();
                    control.SetShadowLayer(radius, distanceX, distanceY, color);
                }
            }
            catch (Exception ex) { }
        }

        protected override void OnDetached () { }
    }
}