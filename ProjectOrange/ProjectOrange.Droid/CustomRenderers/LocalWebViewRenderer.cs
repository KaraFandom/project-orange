using ProjectOrange.CustomViews;
using ProjectOrange.Droid.CustomRenderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(LocalWebView), typeof(LocalWebViewRenderer))]
namespace ProjectOrange.Droid.CustomRenderers
{
    public class LocalWebViewRenderer : WebViewRenderer
    {
        protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == LocalWebView.UriProperty.PropertyName)
            {
                LocalWebView localWebView = Element as LocalWebView;
                Control.Settings.AllowUniversalAccessFromFileURLs = true;

                if (localWebView.Uri == null || string.IsNullOrEmpty(localWebView.Uri))
                    return;

                string url = string.Format("file:///android_asset/pdfjs/web/viewer.html?file={0}", localWebView.Uri);

                Control.LoadUrl(url);
            }
        }
    }
}