﻿using ProjectOrange.Models;
using ProjectOrange.Services;

using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class PONavBar : ContentView
    {
        private bool HasIosTopPadding = true;

        private bool _useBanner = false;
        public bool UseBanner
        {
            get { return _useBanner; }
            set { _useBanner = value; }
        }

        private bool _useButtonRight = false;
        public bool UseButtonRight
        {
            get { return _useButtonRight; }
            set { _useButtonRight = value; }
        }

        private string _title = "Back";
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _imagePathButtonLeft = AppTheme.ArrowReturnDefault;
        public string ImagePathButtonLeft
        {
            get { return _imagePathButtonLeft; }
            set { _imagePathButtonLeft = value; }
        }

        private string _imagePathBanner = AppTheme.BannerDefault;
        public string ImagePathBanner
        {
            get { return _imagePathBanner; }
            set { _imagePathBanner = value; }
        }

        private string _imagePathButtonRight = AppTheme.ShareDefault;
        public string ImagePathButtonRight
        {
            get { return _imagePathButtonRight; }
            set { _imagePathButtonRight = value; }
        }

        private Image _imageLeft = new Image();
        public Image ImageLeft
        {
            get { return _imageLeft; }
        }

        private Image _imageBanner = new Image();
        public Image ImageBanner
        {
            get { return _imageBanner; }
        }

        private Image _imageRight = new Image();
        public Image ImageRight
        {
            get { return _imageRight; }
        }

        private Label _labelTitle = new Label();
        public Label LabelTitle
        {
            get { return _labelTitle; }
            set { _labelTitle = value; }
        }

        private Button _buttonLeft = new Button();
        public Button ButtonLeft
        {
            get { return _buttonLeft; }
        }

        private Button _buttonRight = new Button();
        public Button ButtonRight
        {
            get { return _buttonRight; }
        }

        public PONavBar ()
        {
            InitializeComponent();

            InitializeImagePaths();
            Redraw();
        }

        public void InitializeImagePaths ()
        {
            if (DataService.Theme != null)
            {
                if (DataService.Theme.BannerImage != null)
                    _imagePathBanner = DataService.Theme.BannerImage.GetFileContentPath();

                if (DataService.Theme.IconShareImage != null)
                    _imagePathButtonRight = DataService.Theme.IconShareImage.GetFileContentPath();
            }
        }

        public void Redraw ()
        {
            Color colorBackground = Color.White;

            Grid main = new Grid ()
            {
                BackgroundColor = colorBackground,
                Padding = new Thickness(0, Device.OnPlatform(((HasIosTopPadding) ? 20 : 0), 0, 0), 0, 0),

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(15) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(15) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(15) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(15) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(50) }
                }
            };

            _imageLeft.Source = _imagePathButtonLeft;
            main.Children.Add(_imageLeft, 1, 0);

            _buttonLeft.BackgroundColor = Color.Transparent;
            _buttonLeft.Margin = Device.OnPlatform(0, -4, 0);
            main.Children.Add(_buttonLeft, 0, 0);
            Grid.SetColumnSpan(_buttonLeft, 3);

            if (UseBanner)
            {
                _imageBanner.Source = _imagePathBanner;
                main.Children.Add(_imageBanner, 3, 0);
            }
            else
            {
                _labelTitle.Text = Title;
                _labelTitle.VerticalTextAlignment = TextAlignment.Center;
                main.Children.Add(_labelTitle, 3, 0);
            }

            if (UseButtonRight)
            {
                _imageRight.Source = _imagePathButtonRight;
                main.Children.Add(_imageRight, 5, 0);

                _buttonRight.BackgroundColor = Color.Transparent;
                _buttonRight.Margin = Device.OnPlatform(0, -4, 0);
                main.Children.Add(_buttonRight, 4, 0);
                Grid.SetColumnSpan(_buttonRight, 3);
            }

            Content = main;
        }
    }
}
