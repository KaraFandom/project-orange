﻿using ProjectOrange.Services;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ProjectOrange.Models
{
    public enum AppArticleType
    {
        Text,
        DirectText,
        Link,
        LinkAutoFill,
        LinkExternal,
        LinkCredential,
        LinkLogin,
        File
    }

    public class AppArticle
    {
        public string ErrorMessage { get; set; }
        public string Id { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string BaseContent { get; set; }
        public string CategoryId { get; set; }
        public string FileName { get; set; }
        public string Icon { get; set; }
        public string IconName { get; set; }
        public int SortOrder { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public bool IconFileContentCached { get; set; }
        public AppFileContent IconFileContent { get; set; }

        public AppFileContent FileContent { get; set; }
        public string TextValue { get; set; }

        public AppArticle ()
        {
            ErrorMessage = string.Empty;
            Id = string.Empty;
            ModifiedBy = string.Empty;
            ModifiedDate = string.Empty;
            BaseContent = string.Empty;
            CategoryId = string.Empty;
            FileName = string.Empty;
            Icon = string.Empty;
            IconName = string.Empty;
            SortOrder = 0;
            Title = string.Empty;
            Type = string.Empty;
            Value = string.Empty;
            CreatedDate = string.Empty;
            CreatedBy = string.Empty;

            IconFileContentCached = false;
            IconFileContent = new AppFileContent();

            FileContent = new AppFileContent();
            TextValue = string.Empty;
        }

        public bool IsArticleIconAvailable ()
        {
            return (!string.IsNullOrEmpty(Icon) && !string.IsNullOrEmpty(IconName) && IconFileContentCached) ? true : false;
        }

        public string GetFileExtension ()
        {
            return Path.GetExtension(GetFriendlyFileName()).ToLower();
        }

        public string GetFileExtensionImagePath ()
        {
            string fileExtension = GetFileExtension();
            AppArticleType type = GetArticleType();

            if (type.Equals(AppArticleType.Link))
                return AppTheme.FileTypeIconLink;

            else if (type.Equals(AppArticleType.LinkExternal))
                return AppTheme.FileTypeIconLinkExternal;

            else if (type.Equals(AppArticleType.LinkAutoFill))
                return AppTheme.FileTypeIconLinkAutoFill;

            else if (type.Equals(AppArticleType.LinkCredential))
                return AppTheme.FileTypeIconLinkCredential;

            else if (type.Equals(AppArticleType.LinkLogin))
                return AppTheme.FileTypeIconLinkLogin;

            else if (fileExtension.Equals(".bmp") || fileExtension.Equals(".gif") || fileExtension.Equals(".jpg") || fileExtension.Equals(".jpeg") ||
                fileExtension.Equals(".png") || fileExtension.Equals(".tif") || fileExtension.Equals(".tiff"))
                return AppTheme.FileTypeIconImage;

            else if (fileExtension.Equals(".doc") || fileExtension.Equals(".docx") || fileExtension.Equals(".pdf") || fileExtension.Equals(".rtf") ||
                        fileExtension.Equals(".txt"))
                return AppTheme.FileTypeIconDocument;

            else if (fileExtension.Equals(".avi") || fileExtension.Equals(".flv") || fileExtension.Equals(".m2v") || fileExtension.Equals(".m4v") ||
                        fileExtension.Equals(".mkv") || fileExtension.Equals(".mov") || fileExtension.Equals(".mp2") || fileExtension.Equals(".mp4") ||
                        fileExtension.Equals(".mpg") || fileExtension.Equals(".mpeg") || fileExtension.Equals(".mpv") || fileExtension.Equals(".wmv"))
                return AppTheme.FileTypeIconVideo;

            else
                return AppTheme.FileTypeIconObject;
        }

        public string GetIconFileContentPath ()
        {
            if (IsArticleIconAvailable())
                return StorageService.GetAttachmentPath(DataService.SiteCode, IconName);
            else
                return null;
        }

        public string GetFriendlyFileName ()
        {
            return (!string.IsNullOrEmpty(FileName)) ? StorageService.CleanFilePath(FileName) : string.Empty;
        }

        public string GetFriendlyFileExtension ()
        {
            return Path.GetExtension(GetFriendlyFileName());
        }

        public string GetFriendlyFilePath ()
        {
            return (!string.IsNullOrEmpty(FileName)) ? StorageService.GetAttachmentPath(DataService.SiteCode, FileName) : string.Empty;
        }

        public AppArticleType GetArticleType ()
        {
            return GetArticleType(Type);
        }

        public async Task<bool> DownloadIcon (string siteCode)
        {
            if (string.IsNullOrEmpty(Icon))
                return false;

            if (string.IsNullOrEmpty(IconName))
                return false;

            string iconsFolderName = StorageService.IconFolderName;
            DateTime lastWriteTime = StorageService.DateTimeMinValue;
            IconFileContentCached = await DataService.LoadFileContent(Icon, IconName, StorageService.AttachmentsFolderName, ModifiedBy, ModifiedDate,
                                                                      (fc, dt) => { IconFileContent = fc; lastWriteTime = dt; });

            if (IconFileContentCached)
                await IconFileContent.WriteToFile(lastWriteTime, DataService.SiteCode, iconsFolderName);

            return IconFileContentCached;
        }

        public async Task<bool> PreloadFileContent (string siteCode)
        {
            AppArticleType articleType = GetArticleType();

            if (!articleType.Equals(AppArticleType.File) && !articleType.Equals(AppArticleType.Text))
                return true;

            if (string.IsNullOrEmpty(FileName))
                return false;

            string requestUri = (DataService.DBService is DatabaseService_Sharepoint) ? BaseContent :
                                (DataService.DBService is DatabaseService_SQL) ? Value :
                                string.Empty;

            if (string.IsNullOrEmpty(requestUri))
                return false;

            DateTime start = DateTime.UtcNow;

            string attachmentsFolderName = StorageService.AttachmentsFolderName;
            DateTime lastWriteTime = StorageService.DateTimeMinValue;
            bool success = await DataService.LoadFileContent(requestUri, FileName, StorageService.AttachmentsFolderName, ModifiedBy, ModifiedDate,
                                                             (fc, dt) => { FileContent = fc; lastWriteTime = dt; });

            if (success)
            {
                await FileContent.WriteToFile(lastWriteTime, DataService.SiteCode, attachmentsFolderName);

                if (articleType.Equals(AppArticleType.Text))
                {
                    string textValue = await StorageService.ReadTextFile(FileName, siteCode, StorageService.AttachmentsFolderName);

                    if (!string.IsNullOrEmpty(textValue))
                        TextValue = textValue;
                    else
                        success = false;
                }
            }

            DebugService.WriteLine(string.Format("Downloaded article (#{0}) '{1}' {2} in {3} seconds. Status: {4}",
                                                 Id, Title, FileName, (DateTime.UtcNow - start).TotalSeconds, (success ? "success" : "failed")));

            return success;
        }

        public static AppArticleType GetArticleType (string code)
        {
            switch (code.ToUpper())
            {
                case "TEXT":
                    return AppArticleType.Text;
                case "DIRECTTEXT":
                    return AppArticleType.DirectText;
                case "LINK":
                    return AppArticleType.Link;
                case "LINKAUTOFILL":
                    return AppArticleType.LinkAutoFill;
                case "LINKEXTERNAL":
                    return AppArticleType.LinkExternal;
                case "LINKCREDENTIAL":
                    return AppArticleType.LinkCredential;
                case "LINKLOGIN":
                    return AppArticleType.LinkLogin;
                default:
                case "FILE":
                    return AppArticleType.File;
            }
        }

        public static string GetArticleTypeCode (AppArticleType type)
        {
            switch (type)
            {
                case AppArticleType.Text:
                    return "TEXT";
                case AppArticleType.DirectText:
                    return "DIRECTTEXT";
                case AppArticleType.Link:
                    return "LINK";
                case AppArticleType.LinkAutoFill:
                    return "LINKAUTOFILL";
                case AppArticleType.LinkExternal:
                    return "LINKEXTERNAL";
                case AppArticleType.LinkCredential:
                    return "LINKCREDENTIAL";
                case AppArticleType.LinkLogin:
                    return "LINKLOGIN";
                default:
                case AppArticleType.File:
                    return "FILE";
            }
        }
    }
}
