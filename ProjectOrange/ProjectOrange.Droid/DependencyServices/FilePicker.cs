using Android.Content;
using Android.OS;
using Android.Runtime;
using ProjectOrange.Droid.DependencyServices;
using ProjectOrange.Droid.Other;
using ProjectOrange.Models;
using ProjectOrange.Services;
using ProjectOrange.Services.Interfaces;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(FilePicker))]
namespace ProjectOrange.Droid.DependencyServices
{
    [Preserve(AllMembers = true)]
    public class FilePicker : IFilePicker
    {
        private readonly Context _context;

        private int _requestId;

        private TaskCompletionSource<FilePickerData> _completionSource;

        public FilePicker()
        {
            _context = Android.App.Application.Context;
        }

        public async Task<FilePickerData> PickFile ()
        {
            FilePickerData filePickerData = await PickFileAsync("file/*", Intent.ActionGetContent);

            return filePickerData;
        }

        private Task<FilePickerData> PickFileAsync (string type, string action)
        {
            int requestId = GetRequestId();

            TaskCompletionSource<FilePickerData> _ncompletionSource = new TaskCompletionSource<FilePickerData>(requestId);

            if (Interlocked.CompareExchange(ref _completionSource, _ncompletionSource, null) != null)
                throw new InvalidOperationException("Only one operation can be active at a time.");

            try
            {
                EventHandler<FilePickerEventArgs> handler = null;
                EventHandler<EventArgs> handlerCancel = null;
                Intent documentPicker = new Intent(_context, typeof(FilePickerActivity));
                documentPicker.SetFlags(ActivityFlags.NewTask);
                _context.StartActivity(documentPicker);

                handler = (s, e) =>
                {
                    FilePickerActivity.FilePicked -= handler;
                    TaskCompletionSource<FilePickerData> _xcompletionSource = Interlocked.Exchange(ref _completionSource, null);

                    if (e != null)
                        _xcompletionSource.SetResult(new FilePickerData(e.DataArray, e.FileName, e.FilePath));
                    else
                        _xcompletionSource.SetResult(null);
                };

                handlerCancel = (s, e) =>
                {
                    FilePickerActivity.Cancelled -= handlerCancel;
                    TaskCompletionSource<FilePickerData> _xcompletionSource = Interlocked.Exchange(ref _completionSource, null);

                    _xcompletionSource.SetResult(null);
                };

                FilePickerActivity.FilePicked += handler;
                FilePickerActivity.Cancelled += handlerCancel;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
            }

            return _completionSource.Task;
        }

        private int GetRequestId ()
        {
            int id = _requestId;

            if (_requestId == int.MaxValue)
                _requestId = 0;
            else
                _requestId++;

            return id;
        }
    }
}