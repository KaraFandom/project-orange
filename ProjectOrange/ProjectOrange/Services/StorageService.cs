﻿using PCLStorage;
using ProjectOrange.Services.Interfaces;
using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectOrange.Services
{
    public static class StorageService
    {
        public static readonly DateTime DateTimeMinValue = new DateTime(2000, 1, 1, 0, 0, 0);
        public static readonly string RootFolderName = "Cache";
        public static readonly string ThemeFolderName = "Theme";
        public static readonly string IconFolderName = "Icons";
        public static readonly string AttachmentsFolderName = "Contents";

        public static string CleanFilePath (string value)
        {
            return value.Replace(" ", "%20");
        }

        public static string GetThemeFolderPath (string siteCode)
        {
            return Path.Combine(FileSystem.Current.LocalStorage.Path, RootFolderName, siteCode, ThemeFolderName);
        }

        public static string GetThemeImagePath (string siteCode, string fileName)
        {
            return Path.Combine(FileSystem.Current.LocalStorage.Path, RootFolderName, siteCode, ThemeFolderName, CleanFilePath(fileName));
        }

        public static string GetIconFolderPath (string siteCode)
        {
            return Path.Combine(FileSystem.Current.LocalStorage.Path, RootFolderName, siteCode, IconFolderName);
        }

        public static string GetIconImagePath (string siteCode, string fileName)
        {
            return Path.Combine(FileSystem.Current.LocalStorage.Path, RootFolderName, siteCode, IconFolderName, CleanFilePath(fileName));
        }

        public static string GetAttachmentsFolderPath (string siteCode)
        {
            return Path.Combine(FileSystem.Current.LocalStorage.Path, RootFolderName, siteCode, AttachmentsFolderName);
        }

        public static string GetAttachmentPath (string siteCode, string fileName)
        {
            return Path.Combine(FileSystem.Current.LocalStorage.Path, RootFolderName, siteCode, AttachmentsFolderName, CleanFilePath(fileName));
        }

        public static DateTime CleanDateTime (DateTime value)
        {
            if (value < DateTimeMinValue)
                return DateTimeMinValue;
            else
                return value;
        }

        public static DateTime ParseExactDateTime (string value)
        {
            return DateTime.ParseExact(value, "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture);
        }

        public static async Task<bool> CleanCache ()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder cacheFolder = await rootFolder.CreateFolderAsync(RootFolderName, CreationCollisionOption.OpenIfExists);
            IFolder fileFolder = cacheFolder;

            string responseSuccess = ("CleanCache(" + fileFolder.Path + ") : TRUE");

            await fileFolder.DeleteAsync();

            DebugService.WriteLine(responseSuccess);
            return true;
        }

        public static async Task<bool> CleanCacheSiteFolder (string folderName = "", string subFolderName = "")
        {
            folderName = CleanFilePath(folderName);
            subFolderName = CleanFilePath(subFolderName);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder cacheFolder = await rootFolder.CreateFolderAsync(RootFolderName, CreationCollisionOption.OpenIfExists);
            IFolder fileFolder = cacheFolder;

            if (!string.IsNullOrEmpty(folderName))
            {
                IFolder siteFolder = await fileFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteFolder;
            }

            if (!string.IsNullOrEmpty(subFolderName))
            {
                IFolder siteSubFoler = await fileFolder.CreateFolderAsync(subFolderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteSubFoler;
            }

            string responseSuccess = ("CleanCacheSiteFolder(" + fileFolder.Path + ") : TRUE");

            await fileFolder.DeleteAsync();

            DebugService.WriteLine(responseSuccess);
            return true;
        }

        public static async Task<bool> DeleteFromCache (string fileName, string folderName = "", string subFolderName = "")
        {
            fileName = CleanFilePath(fileName);
            folderName = CleanFilePath(folderName);
            subFolderName = CleanFilePath(subFolderName);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder cacheFolder = await rootFolder.CreateFolderAsync(RootFolderName, CreationCollisionOption.OpenIfExists);
            IFolder fileFolder = cacheFolder;

            if (!string.IsNullOrEmpty(folderName))
            {
                IFolder siteFolder = await fileFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteFolder;
            }

            if (!string.IsNullOrEmpty(subFolderName))
            {
                IFolder siteSubFoler = await fileFolder.CreateFolderAsync(subFolderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteSubFoler;
            }

            string responseSuccess = ("DeleteFromCache(" + (fileFolder.Path + "/" + fileName) + ") : TRUE");
            string responseFailed = ("DeleteFromCache(" + (fileFolder.Path + "/" + fileName) + ") : FALSE");

            ExistenceCheckResult doesFileExist = await fileFolder.CheckExistsAsync(fileName);

            if (doesFileExist == ExistenceCheckResult.NotFound)
            {
                DebugService.WriteLine(responseSuccess);
                return true;
            }

            try
            {
                IFile file = await fileFolder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);

                await file.DeleteAsync();
            }
            catch (Exception ex) { }

            doesFileExist = await fileFolder.CheckExistsAsync(fileName);

            if (doesFileExist == ExistenceCheckResult.NotFound)
            {
                DebugService.WriteLine(responseSuccess);
                return true;
            }
            else
            {
                DebugService.WriteLine(responseFailed);
                return true;
            }
        }

        public static async Task<bool> DoesFileExist (string fileName, string folderName = "", string subFolderName = "")
        {
            fileName = CleanFilePath(fileName);
            folderName = CleanFilePath(folderName);
            subFolderName = CleanFilePath(subFolderName);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder cacheFolder = await rootFolder.CreateFolderAsync(RootFolderName, CreationCollisionOption.OpenIfExists);
            IFolder fileFolder = cacheFolder;

            if (!string.IsNullOrEmpty(folderName))
            {
                IFolder siteFolder = await fileFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteFolder;
            }

            if (!string.IsNullOrEmpty(subFolderName))
            {
                IFolder siteSubFoler = await fileFolder.CreateFolderAsync(subFolderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteSubFoler;
            }

            string responseSuccess = ("DoesFileExist(" + (fileFolder.Path + "/" + fileName) + ") : TRUE");
            string responseFailed = ("DoesFileExist(" + (fileFolder.Path + "/" + fileName) + ") : FALSE");

            ExistenceCheckResult doesFileExist = await fileFolder.CheckExistsAsync(fileName);

            if (doesFileExist == ExistenceCheckResult.FileExists)
            {
                DebugService.WriteLine(responseSuccess);
                return true;
            }
            else
            {
                DebugService.WriteLine(responseFailed);
                return false;
            }
        }

        public static async Task<bool> SaveToCache (byte[] buffer, DateTime lastWriteTime, string fileName, string folderName = "", string subFolderName = "")
        {
            fileName = CleanFilePath(fileName);
            folderName = CleanFilePath(folderName);
            subFolderName = CleanFilePath(subFolderName);
            lastWriteTime = CleanDateTime(lastWriteTime);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder cacheFolder = await rootFolder.CreateFolderAsync(RootFolderName, CreationCollisionOption.OpenIfExists);
            IFolder fileFolder = cacheFolder;

            if (!string.IsNullOrEmpty(folderName))
            {
                IFolder siteFolder = await fileFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteFolder;
            }

            if (!string.IsNullOrEmpty(subFolderName))
            {
                IFolder siteSubFoler = await fileFolder.CreateFolderAsync(subFolderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteSubFoler;
            }

            IFile file = await fileFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);

            string responseSuccess = ("SaveToCache(" + (file.Path) + ") : TRUE");
            string responseFailed = ("SaveToCache(" + (file.Path) + ") : FALSE");

            using (Stream stream = await file.OpenAsync(FileAccess.ReadAndWrite))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(buffer);
                }
            }

            ExistenceCheckResult doesFileExist = await fileFolder.CheckExistsAsync(fileName);

            if (doesFileExist == ExistenceCheckResult.FileExists)
            {
                IFileService fileService = DependencyService.Get<IFileService>();

                if (fileService != null)
                {
                    fileService.SetLastWriteTime(file.Path, lastWriteTime);
                }

                DebugService.WriteLine(responseSuccess);
                return true;
            }
            else
            {
                DebugService.WriteLine(responseFailed);
                return false;
            }
        }

        public static async Task<string> ReadTextFile (string fileName, string folderName = "", string subFolderName = "")
        {
            fileName = CleanFilePath(fileName);
            folderName = CleanFilePath(folderName);
            subFolderName = CleanFilePath(subFolderName);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder cacheFolder = await rootFolder.CreateFolderAsync(RootFolderName, CreationCollisionOption.OpenIfExists);
            IFolder fileFolder = cacheFolder;

            if (!string.IsNullOrEmpty(folderName))
            {
                IFolder siteFolder = await fileFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteFolder;
            }

            if (!string.IsNullOrEmpty(subFolderName))
            {
                IFolder siteSubFoler = await fileFolder.CreateFolderAsync(subFolderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteSubFoler;
            }

            string responseSuccess = ("ReadTextFile(" + (fileFolder.Path + "/" + fileName) + ") : TRUE");
            string responseFailed = ("ReadTextFile(" + (fileFolder.Path + "/" + fileName) + ") : FALSE");

            ExistenceCheckResult doesFileExist = await fileFolder.CheckExistsAsync(fileName);

            if (doesFileExist == ExistenceCheckResult.NotFound)
            {
                DebugService.WriteLine(responseFailed);
                return null;
            }

            try
            {
                IFile file = await fileFolder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);

                return await file.ReadAllTextAsync();
            }
            catch (Exception ex) { }

            DebugService.WriteLine(responseFailed);
            return null;
        }

        public static async Task<byte[]> LoadFromCache (string fileName, string folderName = "", string subFolderName = "")
        {
            fileName = CleanFilePath(fileName);
            folderName = CleanFilePath(folderName);
            subFolderName = CleanFilePath(subFolderName);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder cacheFolder = await rootFolder.CreateFolderAsync(RootFolderName, CreationCollisionOption.OpenIfExists);
            IFolder fileFolder = cacheFolder;

            if (!string.IsNullOrEmpty(folderName))
            {
                IFolder siteFolder = await fileFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteFolder;
            }

            if (!string.IsNullOrEmpty(subFolderName))
            {
                IFolder siteSubFoler = await fileFolder.CreateFolderAsync(subFolderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteSubFoler;
            }

            string responseSuccess = ("LoadFromCache(" + (fileFolder.Path + "/" + fileName) + ") : TRUE");
            string responseFailed = ("LoadFromCache(" + (fileFolder.Path + "/" + fileName) + ") : FALSE");

            ExistenceCheckResult doesFileExist = await fileFolder.CheckExistsAsync(fileName);

            if (doesFileExist == ExistenceCheckResult.NotFound)
            {
                DebugService.WriteLine(responseFailed);
                return null;
            }

            try
            {
                IFile file = await fileFolder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);

                using (Stream stream = await file.OpenAsync(FileAccess.Read))
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        stream.CopyTo(memoryStream);

                        DebugService.WriteLine(responseSuccess);
                        return memoryStream.ToArray();
                    }
                }
            }
            catch (Exception ex) { }

            DebugService.WriteLine(responseFailed);
            return null;
        }

        public static async Task<DateTime> GetLastWriteTime (string fileName, string folderName = "", string subFolderName = "")
        {
            fileName = CleanFilePath(fileName);
            folderName = CleanFilePath(folderName);
            subFolderName = CleanFilePath(subFolderName);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder cacheFolder = await rootFolder.CreateFolderAsync(RootFolderName, CreationCollisionOption.OpenIfExists);
            IFolder fileFolder = cacheFolder;

            if (!string.IsNullOrEmpty(folderName))
            {
                IFolder siteFolder = await fileFolder.CreateFolderAsync(folderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteFolder;
            }

            if (!string.IsNullOrEmpty(subFolderName))
            {
                IFolder siteSubFoler = await fileFolder.CreateFolderAsync(subFolderName, CreationCollisionOption.OpenIfExists);
                fileFolder = siteSubFoler;
            }

            string responseSuccess = ("GetLastWriteTime(" + (fileFolder.Path + "/" + fileName) + ") : TRUE");
            string responseFailed = ("GetLastWriteTime(" + (fileFolder.Path + "/" + fileName) + ") : FALSE");

            ExistenceCheckResult doesFileExist = await fileFolder.CheckExistsAsync(fileName);

            if (doesFileExist == ExistenceCheckResult.FileExists)
            {
                try
                {
                    IFile file = await fileFolder.GetFileAsync(fileName);

                    IFileService fileService = DependencyService.Get<IFileService>();

                    if (fileService != null)
                    {
                        DateTime lastWriteTime = fileService.GetLastWriteTime(file.Path);

                        DebugService.WriteLine(responseSuccess + " : " + lastWriteTime);
                        return lastWriteTime;
                    }
                }
                catch (Exception ex) { }
            }

            DebugService.WriteLine(responseFailed + " : " + DateTimeMinValue);
            return DateTimeMinValue;
        }
    }
}
