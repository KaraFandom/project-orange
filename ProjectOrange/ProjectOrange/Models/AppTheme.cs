﻿using ProjectOrange.Services;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectOrange.Models
{
    public enum AppThemeViewType
    {
        ListView,
        GridView,
        WindowsView
    }

    public enum AppThemeImageType
    {
        ArrowBack, ArrowCollapse, ArrowExpand, ArrowNext, Background, Banner, GridBox,
        Hamburger, Home, Logo, Mail, ScrollBottom, ScrollTop, Search, Share, Sync
    };

    public class AppTheme
    {
        public const string AboutTitleDefault = "About";
        public const string AboutContentDefault = "";
        public const string ContactTitleDefault = "Contact";
        public const string ContactContentDefault = "";
        public const string FactsTitleDefault = "Help";
        public const string FactsContentDefault = "";
        public const string ButtonColorDefault = "#002D73";
        public const string SeperatorColorDefault = "#FFFFFF";
        public const string TextColorDefault = "#FFFFFF";
        public const string WarningsColorSuccessDefault = "#32B459";
        public const string WarningsColorFailedDefault = "#B44132";
        public const string ViewTypeDefault = "ListView";
        public const string SubViewTypeDefault = "ListView";

        public const string ArrowBackDefault = "PO_ArrowBack.png";
        public const string ArrowCollapseDefault = "PO_ArrowCollapse.png";
        public const string ArrowExpandDefault = "PO_ArrowExpand.png";
        public const string ArrowNextDefault = "PO_ArrowNext.png";
        public const string ArrowReturnDefault = "PO_ArrowReturn.png";
        public const string BackgroundDefault = "PO_Background.png";
        public const string BannerDefault = "PO_Banner.png";
        public const string GridBoxDefault = "PO_GridBox.png";
        public const string HamburgerDefault = "PO_Hamburger.png";
        public const string HomeDefault = "PO_Home.png";
        public const string LogoDefault = "PO_Logo";
        public const string MailDefault = "PO_Mail.png";
        public const string ScrollBottomDefault = "PO_ScrollBottom.png";
        public const string ScrollTopDefault = "PO_ScrollTop.png";
        public const string SearchDefault = "PO_Search.png";
        public const string ShareDefault = "PO_Share.png";
        public const string SyncDefault = "PO_Sync.png";

        public const string LoginBackground = "PO_BackgroundLogin.png";
        public const string CategoryIconDefault = "PO_CategoryIcon.png";
        public const string FileTypeIconDocument = "PO_FileTypeIconDocument.png";
        public const string FileTypeIconImage = "PO_FileTypeIconImage.png";
        public const string FileTypeIconLink = "PO_FileTypeIconLink.png";
        public const string FileTypeIconLinkExternal = "PO_FileTypeIconLinkExternal.png";
        public const string FileTypeIconLinkAutoFill = "PO_FileTypeIconLinkAutoFill.png";
        public const string FileTypeIconLinkCredential = "PO_FileTypeIconLinkCredential.png";
        public const string FileTypeIconLinkLogin = "PO_FileTypeIconLinkLogin.png";
        public const string FileTypeIconMore = "PO_FileTypeIconMore.png";
        public const string FileTypeIconObject = "PO_FileTypeIconObject.png";
        public const string FileTypeIconUpload = "PO_FileTypeIconUpload.png";
        public const string FileTypeIconVideo = "PO_FileTypeIconVideo.png";

        public string ErrorMessage { get; set; }
        public string Id { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string AboutContent { get; set; }
        public string AboutTitle { get; set; }
        public string ButtonColor { get; set; }
        public string ContactContent { get; set; }
        public string ContactTitle { get; set; }
        public string FactsContent { get; set; }
        public string FactsTitle { get; set; }
        public string SeperatorColor { get; set; }
        public string TextColor { get; set; }
        public string ViewType { get; set; }
        public string SubViewType { get; set; }
        public string Title { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public string SidePageTitle { get; set; }
        public string SidePageContent { get; set; }

        public AppThemeImage IconArrowBackImage { get; set; }
        public AppThemeImage IconArrowCollapseImage { get; set; }
        public AppThemeImage IconArrowExpandImage { get; set; }
        public AppThemeImage IconArrowNextImage { get; set; }
        public AppThemeImage BackgroundImage { get; set; }
        public AppThemeImage BannerImage { get; set; }
        public AppThemeImage MainScreenBoxImage { get; set; }
        public AppThemeImage IconHamburgerImage { get; set; }
        public AppThemeImage IconHomeImage { get; set; }
        public AppThemeImage IconLogoImage { get; set; }
        public AppThemeImage IconMailImage { get; set; }
        public AppThemeImage IconScrollBottomImage { get; set; }
        public AppThemeImage IconScrollTopImage { get; set; }
        public AppThemeImage IconSearchImage { get; set; }
        public AppThemeImage IconShareImage { get; set; }
        public AppThemeImage IconSyncImage { get; set; }

        public Color ButtonColorValue { get { return UtilService.SafeFromHex(ButtonColor, Color.FromHex(AppTheme.ButtonColorDefault)); } }
        public Color SeperatorColorValue { get { return UtilService.SafeFromHex(SeperatorColor, Color.FromHex(AppTheme.SeperatorColorDefault)); } }
        public Color TextColorValue { get { return UtilService.SafeFromHex(TextColor, Color.FromHex(AppTheme.TextColorDefault)); } }

        public AppTheme ()
        {
            ErrorMessage = string.Empty;
            Id = string.Empty;
            ModifiedBy = string.Empty;
            ModifiedDate = string.Empty;
            AboutTitle = AboutTitleDefault;
            AboutContent = AboutContentDefault;
            ContactTitle = ContactTitleDefault;
            ContactContent = ContactContentDefault;
            FactsTitle = FactsTitleDefault;
            FactsContent = FactsContentDefault;
            ButtonColor = ButtonColorDefault;
            SeperatorColor = SeperatorColorDefault;
            TextColor = TextColorDefault;
            ViewType = ViewTypeDefault;
            SubViewType = SubViewTypeDefault;

            Title = string.Empty;
            CreatedBy = string.Empty;
            CreatedDate = string.Empty;

            SidePageTitle = AboutTitle;
            SidePageContent = AboutContent;

            IconArrowBackImage = new AppThemeImage() { FileContentDefaultPath = ArrowBackDefault };
            IconArrowCollapseImage = new AppThemeImage() { FileContentDefaultPath = ArrowCollapseDefault };
            IconArrowExpandImage = new AppThemeImage() { FileContentDefaultPath = ArrowExpandDefault };
            IconArrowNextImage = new AppThemeImage() { FileContentDefaultPath = ArrowNextDefault };
            BackgroundImage = new AppThemeImage() { FileContentDefaultPath = BackgroundDefault };
            BannerImage = new AppThemeImage() { FileContentDefaultPath = BannerDefault };
            MainScreenBoxImage = new AppThemeImage() { FileContentDefaultPath = GridBoxDefault };
            IconHamburgerImage = new AppThemeImage() { FileContentDefaultPath = HamburgerDefault };
            IconHomeImage = new AppThemeImage() { FileContentDefaultPath = HomeDefault };
            IconLogoImage = new AppThemeImage() { FileContentDefaultPath = LogoDefault };
            IconMailImage = new AppThemeImage() { FileContentDefaultPath = MailDefault };
            IconScrollBottomImage = new AppThemeImage() { FileContentDefaultPath = ScrollBottomDefault };
            IconScrollTopImage = new AppThemeImage() { FileContentDefaultPath = ScrollTopDefault };
            IconSearchImage = new AppThemeImage() { FileContentDefaultPath = SearchDefault };
            IconShareImage = new AppThemeImage() { FileContentDefaultPath = ShareDefault };
            IconSyncImage = new AppThemeImage() { FileContentDefaultPath = SyncDefault };
        }

        public AppThemeViewType GetViewType ()
        {
            switch (ViewType)
            {
                default:
                case "ListView":
                    return AppThemeViewType.ListView;
                case "GridView":
                    return AppThemeViewType.GridView;
                case "WindowsView":
                    return AppThemeViewType.WindowsView;
            }
        }

        public AppThemeViewType GetSubViewType ()
        {
            switch (SubViewType)
            {
                default:
                case "ListView":
                    return AppThemeViewType.ListView;
                case "GridView":
                    return AppThemeViewType.GridView;
                case "WindowsView":
                    return AppThemeViewType.WindowsView;
            }
        }

        private async Task<bool> DownloadThemeImage (string siteCode, AppThemeImage themeImage)
        {
            if (themeImage != null)
                return await themeImage.DownloadImage(siteCode);
            else
                return false;
        }

        public async Task<bool> DownloadThemeImage (string siteCode, AppThemeImageType themeImageType)
        {
            switch (themeImageType)
            {
                case AppThemeImageType.ArrowBack:
                    return await DownloadThemeImage(siteCode, IconArrowBackImage);

                case AppThemeImageType.ArrowCollapse:
                    return await DownloadThemeImage(siteCode, IconArrowCollapseImage);

                case AppThemeImageType.ArrowExpand:
                    return await DownloadThemeImage(siteCode, IconArrowExpandImage);

                case AppThemeImageType.ArrowNext:
                    return await DownloadThemeImage(siteCode, IconArrowNextImage);

                case AppThemeImageType.Background:
                    return await DownloadThemeImage(siteCode, BackgroundImage);

                case AppThemeImageType.Banner:
                    return await DownloadThemeImage(siteCode, BannerImage);

                case AppThemeImageType.GridBox:
                    return await DownloadThemeImage(siteCode, MainScreenBoxImage);

                case AppThemeImageType.Hamburger:
                    return await DownloadThemeImage(siteCode, IconHamburgerImage);

                case AppThemeImageType.Home:
                    return await DownloadThemeImage(siteCode, IconHomeImage);

                case AppThemeImageType.Logo:
                    return await DownloadThemeImage(siteCode, IconLogoImage);

                case AppThemeImageType.Mail:
                    return await DownloadThemeImage(siteCode, IconMailImage);

                case AppThemeImageType.ScrollBottom:
                    return await DownloadThemeImage(siteCode, IconScrollBottomImage);

                case AppThemeImageType.ScrollTop:
                    return await DownloadThemeImage(siteCode, IconScrollTopImage);

                case AppThemeImageType.Search:
                    return await DownloadThemeImage(siteCode, IconSearchImage);

                case AppThemeImageType.Share:
                    return await DownloadThemeImage(siteCode, IconShareImage);

                case AppThemeImageType.Sync:
                    return await DownloadThemeImage(siteCode, IconSyncImage);

                default:
                    return false;
            }
        }
    }
}
