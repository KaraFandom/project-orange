﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Models;
using ProjectOrange.Services;
using ProjectOrange.Services.Interfaces;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectOrange.ViewModels
{
    public class SubPageViewModel : BindableBase, INavigationAware
    {
        private IEventAggregator _eventAggregator;

        private INavigationService _navigationService;

        public DelegateCommand ShowMasterDetailCommand { get; set; }

        public DelegateCommand RefreshCommand { get; set; }

        public DelegateCommand MailCommand { get; set; }

        public DelegateCommand NavigateToHomePageCommand { get; set; }

        public DelegateCommand NavigateBackCommand { get; set; }

        public DelegateCommand NavigateToUploadPageCommand { get; set; }

        public DelegateCommand<AppCategory> NavigateToSubPageCommand { get; set; }

        public DelegateCommand<AppArticle> NavigateToFileAttachmentCommand { get; set; }

        public DelegateCommand<AppLink> NavigateToTextPageCommand { get; set; }

        public DelegateCommand<AppLink> NavigateToWebCommand { get; set; }

        public DelegateCommand<AppLink> NavigateToAuthenticatingWebCommand { get; set; }

        public DelegateCommand<AppLink> NavigateToWebExternalCommand { get; set; }

        public DelegateCommand<AppLinkFill> NavigateToWebFillCommand { get; set; }

        private bool _isLoading = false;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }

        private string _activityText;
        public string ActivityText
        {
            get { return _activityText; }
            set { SetProperty(ref _activityText, value); }
        }

        private string _categoryIconPath;
        public string CategoryIconPath
        {
            get { return _categoryIconPath; }
            set { SetProperty(ref _categoryIconPath, value); }
        }

        private AppCategory _category = new AppCategory();
        public AppCategory Category
        {
            get { return _category; }
            set { SetProperty(ref _category, value, "Category"); }
        }

        public SubPageViewModel (IEventAggregator eventAggregator, INavigationService navigationService)
        {
            _eventAggregator = eventAggregator;
            _navigationService = navigationService;

            ShowMasterDetailCommand = new DelegateCommand(ShowMasterDetail);
            RefreshCommand = new DelegateCommand(Refresh);
            MailCommand = new DelegateCommand(Mail);
            NavigateToHomePageCommand = new DelegateCommand(NavigateToHomePage);
            NavigateBackCommand = new DelegateCommand(NavigateBack);
            NavigateToUploadPageCommand = new DelegateCommand(NavigateToUploadPage);
            NavigateToSubPageCommand = new DelegateCommand<AppCategory>(NavigateToSubPage);
            NavigateToFileAttachmentCommand = new DelegateCommand<AppArticle>(NavigateToFileAttachment);
            NavigateToTextPageCommand = new DelegateCommand<AppLink>(NavigateToTextPage);
            NavigateToWebCommand = new DelegateCommand<AppLink>(NavigateToWeb);
            NavigateToAuthenticatingWebCommand = new DelegateCommand<AppLink>(NavigateToAuthenticatingWeb);
            NavigateToWebExternalCommand = new DelegateCommand<AppLink>(NavigateToWebExternal);
            NavigateToWebFillCommand = new DelegateCommand<AppLinkFill>(NavigateToWebFill);
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo (NavigationParameters parameters)
        {
            if (parameters.ContainsKey("category"))
            {
                AppCategory category = parameters["category"] as AppCategory;

                if (category != null)
                {
                    Category = category;
                }
            }

            else if (parameters.ContainsKey("reload"))
            {
                string reload = parameters["reload"] as string;

                if (reload.Equals("true"))
                {
                    Refresh();
                }
            }
        }

        private void ShowMasterDetail ()
        {
            string pageName = "SubPage";
            _eventAggregator.GetEvent<Events.TogglePOMasterDetailPageEvent>().Publish(new Events.TogglePOMasterDetailPagePayload { DetailPageName = pageName, IsPresented = true });
        }

        private async void Refresh ()
        {
            AppCategory category = await GetCategory();

            if (category != null)
            {
                Category = category.Clone();
            }
        }

        private void Mail ()
        {
            IMailService mailService = DependencyService.Get<IMailService>();

            if (mailService != null)
                mailService.Compose(null, null, null);
        }

        private void NavigateToHomePage ()
        {
            string pageName = "MainPage";
            _navigationService.NavigateAsync(new Uri(DataService.AppURL + DataService.GetMasterDetailPageUriString(pageName) + pageName, UriKind.Absolute));
        }

        private async void NavigateBack ()
        {
            await _navigationService.GoBackAsync();
        }

        private async void NavigateToUploadPage ()
        {
            string pageName = "UploadPage";
            await _navigationService.NavigateAsync(pageName, new NavigationParameters { { "category", Category } });
        }

        private async void NavigateToSubPage (AppCategory category)
        {
            if (IsLoading)
                return;

            if (category == null)
                return;

            IsLoading = true;

            ActivityText = "Syncing Data";

            category = await DataService.LoadCategory(category);

            IsLoading = false;

            string pageName = "SubPage";
            await _navigationService.NavigateAsync(DataService.GetMasterDetailPageUriString(pageName) + "/" + pageName, new NavigationParameters { { "category", category } });
        }

        private async void NavigateToFileAttachment (AppArticle article)
        {
            if (IsLoading)
                return;

            if (article == null)
                return;

            IsLoading = true;

            ActivityText = "Loading, Please Wait";

            if (await article.PreloadFileContent(DataService.SiteCode))
            {
                string fileName = article.GetFriendlyFileName();
                string fileExtension = article.GetFriendlyFileExtension().ToLower();
                string filePath = article.GetFriendlyFilePath();

                if (fileExtension.Equals(".pdf"))
                    await _navigationService.NavigateAsync("LocalWebPage", new NavigationParameters { { "source", filePath } });

                else if (fileExtension.Equals(".bmp") || fileExtension.Equals(".gif") || fileExtension.Equals(".jpg") || fileExtension.Equals(".jpeg") ||
                        fileExtension.Equals(".png") || fileExtension.Equals(".tif") || fileExtension.Equals(".tiff"))
                    await _navigationService.NavigateAsync("ImagePage", new NavigationParameters { { "source", filePath } });

                else if (fileExtension.Equals(".avi") || fileExtension.Equals(".flv") || fileExtension.Equals(".m2v") || fileExtension.Equals(".m4v") ||
                        fileExtension.Equals(".mkv") || fileExtension.Equals(".mov") || fileExtension.Equals(".mp2") || fileExtension.Equals(".mp4") ||
                        fileExtension.Equals(".mpg") || fileExtension.Equals(".mpeg") || fileExtension.Equals(".mpv") || fileExtension.Equals(".wmv"))
                    await _navigationService.NavigateAsync("VideoPage", new NavigationParameters { { "source", filePath } });

                else
                {
                    if (Device.OS.Equals(TargetPlatform.iOS) &&
                        (fileExtension.Equals(".xls") || fileExtension.Equals(".xlsx") ||       // Excel
                            fileExtension.Equals(".key.zip") ||                                 // Keynote
                            fileExtension.Equals(".numbers.zip") ||                             // Numbers
                            fileExtension.Equals(".pages.zip") ||                               // Pages
                            fileExtension.Equals(".pdf") ||                                     // PDF
                            fileExtension.Equals(".ppt") || fileExtension.Equals(".pptx") ||    // PowerPoint
                            fileExtension.Equals(".rtf") ||                                     // Rich Text Format
                            fileExtension.Equals(".doc") || fileExtension.Equals(".docx")))     // Word
                        await _navigationService.NavigateAsync("LocalWebPage", new NavigationParameters { { "source", filePath } });

                    else
                    {
                        IIntentService intentService = DependencyService.Get<IIntentService>();

                        if (intentService != null)
                            intentService.Open(fileName);
                    }
                }
            }

            IsLoading = false;
        }

        private async void NavigateToTextPage (AppLink link)
        {
            if (link != null && !string.IsNullOrEmpty(link.Value))
                await _navigationService.NavigateAsync("TextPage", new NavigationParameters { { "title", link.Title }, { "body", link.Value } });
        }

        private async void NavigateToWeb (AppLink link)
        {
            if (link != null && !string.IsNullOrEmpty(link.Value))
                await _navigationService.NavigateAsync("WebPage", new NavigationParameters { { "source", link } });
        }

        private async void NavigateToAuthenticatingWeb (AppLink link)
        {
            if (link != null && !string.IsNullOrEmpty(link.Value))
                await _navigationService.NavigateAsync("AuthenticatingWebPage", new NavigationParameters { { "source", link } });
        }

        private void NavigateToWebExternal (AppLink link)
        {
            if (link != null && !string.IsNullOrEmpty(link.Value))
                Device.OpenUri(new Uri(link.Value));
        }

        private async void NavigateToWebFill (AppLinkFill link)
        {
            if (link != null && !string.IsNullOrEmpty(link.Value))
            {
                await Application.Current.MainPage.DisplayAlert("Opening Web Page", link.GetFriendlyValue(), "OK");

                await _navigationService.NavigateAsync("WebPage", new NavigationParameters { { "source", (new AppLink { Title = link.Title, Value = link.GetValue() }) } });
            }
        }

        private async Task<AppCategory> GetCategory ()
        {
            if (IsLoading)
                return null;

            if (Category == null)
                return null;

            IsLoading = true;

            ActivityText = "Syncing Data";

            AppCategory category = await DataService.LoadCategory(Category);

            IsLoading = false;

            if (category == null)
                return null;

            return category;
        }
    }
}
