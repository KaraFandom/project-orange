﻿using ProjectOrange.CustomViews;
using ProjectOrange.Services;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class POMasterDetailPage : MasterDetailPage
    {
        public POMasterDetailPage ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Initialization
            Color separatorColor = Color.White;
            Color textColor = Color.White;
            Color buttonColor = Color.FromHex("#002D73");

            if (DataService.Theme != null)
            {
                separatorColor = DataService.Theme.SeperatorColorValue;

                textColor = DataService.Theme.TextColorValue;

                buttonColor = DataService.Theme.ButtonColorValue;
            }

            if (Master != null)
                Master.BackgroundColor = buttonColor;
            #endregion

            #region Content
            Label labelMenu = new Label ()
            {
                Text = "Menu",
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = textColor
            };
            Stack.Children.Add(labelMenu);

            Stack.Children.Add(new HorizontalSeparator() { Color = separatorColor });

            Button buttonHome = new Button ()
            {
                Text = "Home",
                VerticalOptions = LayoutOptions.StartAndExpand,

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonHome.SetBinding(Button.CommandProperty, "NavigateToHomePageCommand");
            Stack.Children.Add(buttonHome);

            Stack.Children.Add(new HorizontalSeparator() { Color = separatorColor });

            Button buttonAbout = new Button ()
            {
                CommandParameter = "ABOUT",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonAbout.SetBinding(Button.TextProperty, "Theme.AboutTitle");
            buttonAbout.SetBinding(Button.CommandProperty, "NavigateToSidePageCommand");
            Stack.Children.Add(buttonAbout);

            Stack.Children.Add(new HorizontalSeparator() { Color = separatorColor });

            Button buttonContact = new Button ()
            {
                CommandParameter = "CONTACT",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonContact.SetBinding(Button.TextProperty, "Theme.ContactTitle");
            buttonContact.SetBinding(Button.CommandProperty, "NavigateToSidePageCommand");
            Stack.Children.Add(buttonContact);

            Stack.Children.Add(new HorizontalSeparator() { Color = separatorColor });

            Button buttonFacts = new Button ()
            {
                CommandParameter = "FACTS",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonFacts.SetBinding(Button.TextProperty, "Theme.FactsTitle");
            buttonFacts.SetBinding(Button.CommandProperty, "NavigateToSidePageCommand");
            Stack.Children.Add(buttonFacts);

            Stack.Children.Add(new HorizontalSeparator() { Color = separatorColor });

            Button buttonSettings = new Button()
            {
                Text = "Settings",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonSettings.SetBinding(Button.CommandProperty, "NavigateToSettingsPageCommand");
            Stack.Children.Add(buttonSettings);

            Stack.Children.Add(new HorizontalSeparator() { Color = separatorColor });

            Button buttonLogout = new Button()
            {
                Text = "Log Out",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonLogout.SetBinding(Button.CommandProperty, "LogoutCommand");
            Stack.Children.Add(buttonLogout);
            #endregion
        }
    }
}