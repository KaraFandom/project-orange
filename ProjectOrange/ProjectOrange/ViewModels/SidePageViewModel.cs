﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Services;

namespace ProjectOrange.ViewModels
{
    public class SidePageViewModel : BindableBase, INavigationAware
    {
        private IEventAggregator _eventAggregator;

        private INavigationService _navigationService;

        public DelegateCommand ShowMasterDetailCommand { get; set; }

        public DelegateCommand BackCommand { get; set; }

        private string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _body = string.Empty;
        public string Body
        {
            get { return _body; }
            set { SetProperty(ref _body, value); }
        }

        public SidePageViewModel (IEventAggregator eventAggregator, INavigationService navigationService)
        {
            _eventAggregator = eventAggregator;
            _navigationService = navigationService;

            ShowMasterDetailCommand = new DelegateCommand(ShowMasterDetail);
            BackCommand = new DelegateCommand(Back);

            Initialize();
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo (NavigationParameters parameters)
        {
            if (parameters.ContainsKey("title"))
                Title = (string) parameters["title"];

            if (parameters.ContainsKey("body"))
                Body = (string) parameters["body"];
        }

        private void Initialize ()
        {
            if (DataService.Theme != null)
            {
                if (!string.IsNullOrEmpty(DataService.Theme.SidePageTitle))
                    Title = DataService.Theme.SidePageTitle;

                if (!string.IsNullOrEmpty(DataService.Theme.SidePageContent))
                    Body = DataService.Theme.ContactContent;
            }
        }

        private void ShowMasterDetail ()
        {
            string pageName = "SidePage";
            _eventAggregator.GetEvent<Events.TogglePOMasterDetailPageEvent>().Publish(new Events.TogglePOMasterDetailPagePayload { DetailPageName = pageName, IsPresented = true });
        }

        private void Back ()
        {
            string pageName = "MainPage";
            _navigationService.NavigateAsync(DataService.GetMasterDetailPageUriString(pageName) + "/" + pageName);
        }
    }
}
