﻿using ProjectOrange.Models;
using ProjectOrange.Services;
using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class POToolbox : ContentView
    {
        private string _imagePath1 = AppTheme.HomeDefault;
        public string ImagePath1
        {
            get { return _imagePath1; }
        }

        private string _imagePath2 = AppTheme.SyncDefault;
        public string ImagePath2
        {
            get { return _imagePath2; }
        }

        private string _imagePath3 = AppTheme.ScrollBottomDefault;
        public string ImagePath3
        {
            get { return _imagePath3; }
        }

        private string _imagePath4 = AppTheme.MailDefault;
        public string ImagePath4
        {
            get { return _imagePath4; }
        }

        private Image _image1 = new Image();
        public Image Image1
        {
            get { return _image1; }
        }

        private Image _image2 = new Image();
        public Image Image2
        {
            get { return _image2; }
        }

        private Image _image3 = new Image();
        public Image Image3
        {
            get { return _image3; }
        }

        private Image _image4 = new Image();
        public Image Image4
        {
            get { return _image4; }
        }

        private Button _button1 = new Button();
        public Button Button1
        {
            get { return _button1; }
        }

        private Button _button2 = new Button();
        public Button Button2
        {
            get { return _button2; }
        }

        private Button _button3 = new Button();
        public Button Button3
        {
            get { return _button3; }
        }

        private Button _button4 = new Button();
        public Button Button4
        {
            get { return _button4; }
        }

        public POToolbox ()
        {
            InitializeComponent();

            InitializeImagePaths();
            Redraw();
        }

        public void InitializeImagePaths ()
        {
            if (DataService.Theme != null)
            {
                if (DataService.Theme.IconHomeImage != null)
                    _imagePath1 = DataService.Theme.IconHomeImage.GetFileContentPath();

                if (DataService.Theme.IconSyncImage != null)
                    _imagePath2 = DataService.Theme.IconSyncImage.GetFileContentPath();

                if (DataService.Theme.IconScrollBottomImage != null)
                    _imagePath3 = DataService.Theme.IconScrollBottomImage.GetFileContentPath();

                if (DataService.Theme.IconMailImage != null)
                    _imagePath4 = DataService.Theme.IconMailImage.GetFileContentPath();
            }
        }

        private void Redraw ()
        {
            Color colorBackground = Color.White;

            Grid main = new Grid ()
            {
                BackgroundColor = colorBackground,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(25) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(50) }
                }
            };

            _image1.Source = _imagePath1;
            main.Children.Add(_image1, 2, 0);

            _button1.BackgroundColor = Color.Transparent;
            _button1.Margin = Device.OnPlatform(0, -4, 0);
            main.Children.Add(_button1, 1, 0);
            Grid.SetColumnSpan(_button1, 3);

            _image2.Source = _imagePath2;
            main.Children.Add(_image2, 5, 0);

            _button2.BackgroundColor = Color.Transparent;
            _button2.Margin = Device.OnPlatform(0, -4, 0);
            main.Children.Add(_button2, 4, 0);
            Grid.SetColumnSpan(_button2, 3);

            _image3.Source = _imagePath3;
            main.Children.Add(_image3, 8, 0);

            _button3.BackgroundColor = Color.Transparent;
            _button3.Margin = Device.OnPlatform(0, -4, 0);
            main.Children.Add(_button3, 7, 0);
            Grid.SetColumnSpan(_button3, 3);

            _image4.Source = _imagePath4;
            main.Children.Add(_image4, 11, 0);

            _button4.BackgroundColor = Color.Transparent;
            _button4.Margin = Device.OnPlatform(0, -4, 0);
            main.Children.Add(_button4, 10, 0);
            Grid.SetColumnSpan(_button4, 3);

            Content = main;
        }
    }
}
