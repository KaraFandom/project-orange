using System;
using ProjectOrange.iOS.DependencyServices;
using ProjectOrange.Services.Interfaces;
using Xamarin.Forms;
using MessageUI;
using System.IO;
using ProjectOrange.Services;
using Foundation;

[assembly: Dependency(typeof(MailService))]
namespace ProjectOrange.iOS.DependencyServices
{
    public class MailService : IMailService
    {
        public async void Compose (string[] to, string[] cc, string[] bcc, string subject = null, string body = null, bool isHtml = false, string[] attachments = null, Action<bool> completed = null)
        {
            MFMailComposeViewController controller = new MFMailComposeViewController();

            // Set To Recipients
            if (to != null && to.Length > 0) controller.SetToRecipients(to);

            // Set CC Recipients
            if (cc != null && cc.Length > 0) controller.SetCcRecipients(cc);

            // Set BCC Recipients
            if (bcc != null && bcc.Length > 0) controller.SetBccRecipients(bcc);

            // Set Subject
            if (!string.IsNullOrEmpty(subject)) controller.SetSubject(subject);

            // Set Body
            if (!string.IsNullOrEmpty(body)) controller.SetMessageBody(body, isHtml);

            // Set Attachments
            if (attachments != null && attachments.Length > 0)
            {
                string filePath = string.Empty;
                string mimeType = string.Empty;
                byte[] file = null;
                NSData nsfile = null;

                foreach (string attachment in attachments)
                {
                    filePath = StorageService.GetAttachmentPath(DataService.SiteCode, attachment);
                    mimeType = MimeType.GetMimeTypeOrDefault(Path.GetExtension(filePath), "text/rtf");

                    file = await StorageService.LoadFromCache(attachment, DataService.SiteCode, StorageService.AttachmentsFolderName);

                    if (file != null)
                    {
                        nsfile = NSData.FromArray(file);

                        if (nsfile != null)
                            controller.AddAttachmentData(nsfile, mimeType, Path.GetFileName(filePath));
                    }
                }
            }

            // Set Completed callback
            controller.Finished += (s, e) =>
            {
                if (completed != null) completed(e.Result == MFMailComposeResult.Sent);
                e.Controller.DismissViewController(true, null);
            };

            Application.GetActiveViewController().PresentViewController(controller, true, null);
        }
    }
}