﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Services;

namespace ProjectOrange.ViewModels
{
    public class TextPageViewModel : BindableBase, INavigationAware
    {
        private IEventAggregator _eventAggregator;

        private INavigationService _navigationService;

        public DelegateCommand BackCommand { get; set; }

        private string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _body = string.Empty;
        public string Body
        {
            get { return _body; }
            set { SetProperty(ref _body, value); }
        }

        public TextPageViewModel (IEventAggregator eventAggregator, INavigationService navigationService)
        {
            _eventAggregator = eventAggregator;
            _navigationService = navigationService;

            BackCommand = new DelegateCommand(Back);

            Initialize();
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo (NavigationParameters parameters)
        {
            if (parameters.ContainsKey("title"))
                Title = (string) parameters["title"];

            if (parameters.ContainsKey("body"))
                Body = (string) parameters["body"];
        }

        private void Initialize ()
        {
        }

        private void Back ()
        {
            _navigationService.GoBackAsync();
        }
    }
}
