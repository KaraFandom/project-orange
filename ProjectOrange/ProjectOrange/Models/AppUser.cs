﻿namespace ProjectOrange.Models
{
    public class AppUser
    {
        public string ErrorMessage { get; set; }
        public string Id { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string FullName { get; set; }
        public string Group { get; set; }
        public string SiteCode { get; set; }
        public string UserName { get; set; }
        public string apiAuthSessionsUsername { get; set; }
        public string apiAuthSessionsToken { get; set; }
        public string apiAuthSessionsIP { get; set; }
        public string createdDate { get; set; }
        public string ExpirationDate { get; set; }
        public string apiAuthSessionsID { get; set; }

        public AppUser ()
        {
            ErrorMessage = string.Empty;
            Id = string.Empty;
            ModifiedBy = string.Empty;
            ModifiedDate = string.Empty;
            FullName = string.Empty;
            Group = string.Empty;
            SiteCode = string.Empty;
            UserName = string.Empty;
            apiAuthSessionsUsername = string.Empty;
            apiAuthSessionsToken = string.Empty;
            apiAuthSessionsIP = string.Empty;
            createdDate = string.Empty;
            ExpirationDate = string.Empty;
            apiAuthSessionsID = string.Empty;
        }
    }
}
