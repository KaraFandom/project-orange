﻿using ProjectOrange.Models;
using ProjectOrange.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class POCategoriesViewGrid : ContentView
    {
        public static readonly BindableProperty CategoriesProperty = BindableProperty.Create(nameof(Categories),
                                                                                             typeof(List<AppCategory>),
                                                                                             typeof(POCategoriesViewGrid),
                                                                                             new List<AppCategory>(),
                                                                                             BindingMode.TwoWay,
                                                                                             null,
                                                                                             (bindable, oldValue, newValue) =>
                                                                                             {
                                                                                                 POCategoriesViewGrid categoriesViewGrid = (POCategoriesViewGrid) bindable;
                                                                                                 categoriesViewGrid.Redraw();
                                                                                             });

        private string _filterQuery = string.Empty;
        public string FilterQuery
        {
            get { return _filterQuery; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _filterQuery = string.Empty;
                else
                    _filterQuery = value;

                Redraw();
            }
        }

        private ScrollView _scrollView = new ScrollView();
        public ScrollView ScrollView
        {
            get { return _scrollView; }
        }

        public List<AppCategory> Categories
        {
            get { return (List<AppCategory>) GetValue(CategoriesProperty); }
            set { SetValue(CategoriesProperty, value); }
        }

        public List<AppCategory> FilteredCategories
        {
            get { return Categories.Where(x => x.Name.ToLower().Contains(FilterQuery.ToLower())).OrderBy(x => x.SortOrder).ToList(); }
        }

        public POCategoriesViewGrid ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            if (Categories == null || Categories.Count == 0)
                return;

            #region Initialization
            double fontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Micro, NamedSize.Small, NamedSize.Small), typeof(Label));
            string imagePathGridBox = AppTheme.GridBoxDefault;
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);
            TextAlignment alignmentTitleHorizontal = TextAlignment.Center;
            TextAlignment alignmentTitleVertical = TextAlignment.Center;

            if (DataService.Theme != null)
            {
                if (DataService.Theme.MainScreenBoxImage != null)
                    imagePathGridBox = DataService.Theme.MainScreenBoxImage.GetFileContentPath();

                textColor = DataService.Theme.TextColorValue;
            }

            ScrollView main = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Padding = new Thickness(10, 10),
                Spacing = 0
            };

            if (FilteredCategories.Count > 0)
            {
                double screenWidth = Application.Current.MainPage.Width;

                int gridColumns = 3;
                int gridRows = (int) Math.Ceiling(((double) FilteredCategories.Count) / gridColumns);
                double gridColumnWidth = screenWidth / gridColumns;

                double cellHeight = gridColumnWidth;
                double cellIconHeight = cellHeight * 0.4;
                double cellPadHeight = cellHeight - cellIconHeight;
                double cellLabelHeight = 50;

                Grid grid = new Grid ()
                {
                    ColumnSpacing = 0,
                    RowSpacing = 0
                };
                content.Children.Add(grid);

                for (int i = 0; i < gridColumns; i++)
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                for (int i = 0; i < gridRows; i++)
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(cellHeight + cellLabelHeight) });

                string imagePathCategoryIcon = string.Empty;
                AppCategory category;
                for (int i = 0; i < FilteredCategories.Count; i++)
                {
                    category = FilteredCategories[i];

                    if (category == null)
                        continue;

                    imagePathCategoryIcon = category.GetIconFileContentPath();

                    Grid gridCategory = new Grid ()
                    {
                        ColumnSpacing = 0,
                        RowSpacing = 0,

                        ColumnDefinitions =
                        {
                            new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                        },
                        RowDefinitions =
                        {
                            new RowDefinition { Height = new GridLength(cellPadHeight / 2) },
                            new RowDefinition { Height = new GridLength(cellIconHeight) },
                            new RowDefinition { Height = new GridLength(cellPadHeight / 2) },
                            new RowDefinition { Height = new GridLength(cellLabelHeight) }
                        }
                    };
                    grid.Children.Add(gridCategory, (i % gridColumns), ((int)Math.Floor((double)i / gridColumns)));

                    TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
                    {
                        CommandParameter = category
                    };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToSubPageCommand");

                    Image imageBackground = new Image ()
                    {
                        Source = imagePathGridBox
                    };
                    imageBackground.GestureRecognizers.Add(tapGestureRecognizer);
                    gridCategory.Children.Add(imageBackground, 0, 0);
                    Grid.SetRowSpan(imageBackground, 3);

                    Image imageCategory = new Image ()
                    {
                        Source = imagePathCategoryIcon
                    };
                    imageCategory.GestureRecognizers.Add(tapGestureRecognizer);
                    gridCategory.Children.Add(imageCategory, 0, 1);

                    Label labelCategory = new Label ()
                    {
                        Text = category.Name,

                        TextColor = textColor,
                        FontSize = fontSize,
                        HorizontalTextAlignment = alignmentTitleHorizontal,
                        VerticalTextAlignment = alignmentTitleVertical
                    };
                    gridCategory.Children.Add(labelCategory, 0, 3);
                }
            }
            #endregion

            main.Content = content;
            _scrollView = main;
            Content = main;
        }
    }
}
