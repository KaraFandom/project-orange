﻿using System;

namespace ProjectOrange.Models
{
    public class FilePickerEventArgs : EventArgs
    {
        public byte[] DataArray { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public FilePickerEventArgs () { }

        public FilePickerEventArgs (byte[] dataArray) { DataArray = dataArray; }

        public FilePickerEventArgs (byte[] dataArray, string fileName) : this (dataArray) { FileName = fileName; }

        public FilePickerEventArgs (byte[] dataArray, string fileName, string filePath) : this (dataArray, fileName) { FilePath = filePath; }
    }
}
