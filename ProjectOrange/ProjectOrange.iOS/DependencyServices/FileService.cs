using ProjectOrange.iOS.DependencyServices;
using ProjectOrange.Services;
using ProjectOrange.Services.Interfaces;
using System;
using System.IO;
using System.IO.Compression;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileService))]
namespace ProjectOrange.iOS.DependencyServices
{
    public class FileService : IFileService
    {
        public byte[] Compress (byte[] data)
        {
            MemoryStream output = new MemoryStream();

            using (DeflateStream dstream = new DeflateStream(output, CompressionLevel.Optimal))
            {
                dstream.Write(data, 0, data.Length);
            }

            return output.ToArray();
        }

        public byte[] Decompress (byte[] data)
        {
            MemoryStream input = new MemoryStream(data);
            MemoryStream output = new MemoryStream();

            using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress))
            {
                dstream.CopyTo(output);
            }

            return output.ToArray();
        }

        public DateTime GetLastWriteTime (string path)
        {
            if (File.Exists(path))
                return File.GetLastWriteTime(path);
            else
                return StorageService.DateTimeMinValue;
        }

        public void SetLastWriteTime (string path, DateTime lastWriteTime)
        {
            if (File.Exists(path))
                File.SetLastWriteTime(path, lastWriteTime);
        }
    }
}