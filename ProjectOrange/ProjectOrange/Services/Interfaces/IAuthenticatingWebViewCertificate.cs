﻿namespace ProjectOrange.Services.Interfaces
{
    public interface IAuthenticatingWebViewCertificate
    {
        string Host { get; }
        byte[] Hash { get; }
        string HashString { get; }
        byte[] PublicKey { get; }
        string PublicKeyString { get; }
    }
}
