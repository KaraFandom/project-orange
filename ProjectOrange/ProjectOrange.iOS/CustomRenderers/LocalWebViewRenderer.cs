using Foundation;
using ProjectOrange.CustomViews;
using ProjectOrange.iOS.CustomRenderers;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(LocalWebView), typeof(LocalWebViewRenderer))]
namespace ProjectOrange.iOS.CustomRenderers
{
    public class LocalWebViewRenderer : ViewRenderer<LocalWebView, UIWebView>
    {
        protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null)
                SetNativeControl(new UIWebView());

            if (e.PropertyName == LocalWebView.UriProperty.PropertyName)
            {
                LocalWebView localWebView = Element as LocalWebView;
                Control.ScalesPageToFit = true;

                Control.LoadRequest(new NSUrlRequest(new NSUrl(localWebView.Uri, false)));
            }
        }
    }
}