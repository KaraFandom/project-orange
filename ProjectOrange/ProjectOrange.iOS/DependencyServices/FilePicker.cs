using CoreGraphics;
using Foundation;
using MobileCoreServices;
using ProjectOrange.iOS.DependencyServices;
using ProjectOrange.Models;
using ProjectOrange.Services.Interfaces;
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(FilePicker))]
namespace ProjectOrange.iOS.DependencyServices
{
    public class FilePicker : IFilePicker
    {
        private int _requestId;

        private TaskCompletionSource<FilePickerData> _completionSource;

        public EventHandler<FilePickerEventArgs> Handler { get; set; }

        public async Task<FilePickerData> PickFile ()
        {
            string[] allowedUtis = new string[]
            {
                UTType.Text,
                UTType.PlainText,
                UTType.UTF8PlainText,
                UTType.UTF16PlainText,
                UTType.RTF,

                UTType.PDF,

                UTType.Spreadsheet,

                UTType.Image,
                UTType.PNG,

                UTType.Video,
                UTType.Movie,
                UTType.AVIMovie,

                UTType.FileURL
            };

            FilePickerData filePickerData = await PickFileAsync(allowedUtis);

            return filePickerData;
        }

        private Task<FilePickerData> PickFileAsync (string[] allowedUtis)
        {
            int requestId = GetRequestId();

            TaskCompletionSource<FilePickerData> _ncompletionSource = new TaskCompletionSource<FilePickerData>(requestId);

            if (Interlocked.CompareExchange(ref _completionSource, _ncompletionSource, null) != null)
                throw new InvalidOperationException("Only one operation can be active at a time.");

            UIViewController activeViewController = Application.GetActiveViewController();

            UIDocumentMenuViewController documentMenu = new UIDocumentMenuViewController(allowedUtis, UIDocumentPickerMode.Import);
            documentMenu.ModalPresentationStyle = UIModalPresentationStyle.Popover;
            documentMenu.DidPickDocumentPicker += DocumentMenu_DidPickDocumentPicker;
            documentMenu.WasCancelled += DocumentMenu_WasCancelled;
            documentMenu.AddOption("Photos & Videos", null, UIDocumentMenuOrder.First, DocumentMenu_DidPickImagePicker);

            activeViewController.PresentViewController(documentMenu, true, null);

            UIPopoverPresentationController popoverPresentationController = documentMenu.PopoverPresentationController;
            if (popoverPresentationController != null)
            {
                CGRect frame = documentMenu.View.Bounds;
                frame.Y = frame.Height;
                popoverPresentationController.SourceView = documentMenu.View;
                popoverPresentationController.SourceRect = frame;
                popoverPresentationController.PermittedArrowDirections = UIPopoverArrowDirection.Down;
            }

            Handler = null;
            Handler += (s, e) =>
            {
                TaskCompletionSource<FilePickerData> _xcompletionSource = Interlocked.Exchange(ref _completionSource, null);

                if (e != null)
                    _xcompletionSource.SetResult(new FilePickerData(e.DataArray, e.FileName, e.FilePath));
                else
                    _xcompletionSource.SetResult(null);
            };

            return _completionSource.Task;
        }

        private int GetRequestId ()
        {
            int id = _requestId;

            if (_requestId == int.MaxValue)
                _requestId = 0;
            else
                _requestId++;

            return id;
        }

        private void DocumentMenu_DidPickDocumentPicker (object sender, UIDocumentMenuDocumentPickedEventArgs e)
        {
            e.DocumentPicker.DidPickDocument += DocumentPicker_DidPickDocument;
            e.DocumentPicker.WasCancelled += DocumentPicker_WasCancelled;

            Application.GetActiveViewController().PresentViewController(e.DocumentPicker, true, null);
        }

        private void DocumentMenu_DidPickImagePicker ()
        {
            UIImagePickerController imagePicker = new UIImagePickerController();
            imagePicker.MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary);
            imagePicker.AllowsEditing = false;
            imagePicker.VideoQuality = UIImagePickerControllerQualityType.Medium;
            imagePicker.FinishedPickingMedia += ImagePicker_FinishedPickingMedia;
            imagePicker.Canceled += ImagePicker_Canceled;

            Application.GetActiveViewController().PresentViewController(imagePicker, true, null);
        }

        private void DocumentMenu_WasCancelled (object sender, EventArgs e)
        {
            OnCancelled();
        }

        private void DocumentPicker_DidPickDocument (object sender, UIDocumentPickedEventArgs e)
        {
            bool securityEnabled = e.Url.StartAccessingSecurityScopedResource();
            UIDocument document = new UIDocument(e.Url);
            string fileName = document.LocalizedName;
            string filePath = document.FileUrl.ToString();
            if (fileName == null) fileName = filePath?.Substring((filePath?.LastIndexOf('/') ?? 0) + 1);

            NSData data = NSData.FromUrl(e.Url);
            byte[] dataArray = new byte[data.Length];
            Marshal.Copy(data.Bytes, dataArray, 0, Convert.ToInt32(data.Length));
            OnFilePicked(new FilePickerEventArgs(dataArray, fileName, filePath));
        }

        private void DocumentPicker_WasCancelled (object sender, EventArgs e)
        {
            OnCancelled();
        }

        private void ImagePicker_FinishedPickingMedia (object sender, UIImagePickerMediaPickedEventArgs e)
        {
            if (e.Info[UIImagePickerController.MediaType].ToString().Equals("public.image"))
            {
                UIImage image = e.OriginalImage;
                NSData data = image.AsJPEG(0.75f);
                byte[] dataArray = new byte[data.Length];

                Marshal.Copy(data.Bytes, dataArray, 0, Convert.ToInt32(data.Length));

                OnFilePicked(new FilePickerEventArgs(dataArray, "photo.jpeg"));
            }
            else
            {
                string filePath = e.MediaUrl.ToString();
                string fileName = filePath?.Substring((filePath?.LastIndexOf('/') ?? 0) + 1);
                NSData data = NSData.FromUrl(e.MediaUrl);
                byte[] dataArray = new byte[data.Length];
                Marshal.Copy(data.Bytes, dataArray, 0, Convert.ToInt32(data.Length));
                OnFilePicked(new FilePickerEventArgs(dataArray, fileName, filePath));
            }

            UIImagePickerController imagePicker = sender as UIImagePickerController;
            if (sender != null) imagePicker.DismissViewController(true, null);
        }

        private void ImagePicker_Canceled (object sender, EventArgs e)
        {
            OnCancelled();

            UIImagePickerController imagePicker = sender as UIImagePickerController;
            if (sender != null) imagePicker.DismissViewController(true, null);
        }

        private void OnFilePicked (FilePickerEventArgs e)
        {
            Handler?.Invoke(null, e);
        }

        private void OnCancelled ()
        {
            Handler?.Invoke(null, null);
        }
    }
}