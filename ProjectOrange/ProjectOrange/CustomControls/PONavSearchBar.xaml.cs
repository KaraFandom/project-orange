﻿using ProjectOrange.Models;
using ProjectOrange.Services;

using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class PONavSearchBar : ContentView
    {
        private bool HasIosTopPadding = true;

        private bool _isSearching;
        public bool IsSearching
        {
            get { return _isSearching; }
        }

        private Image _imageHamburger = new Image();
        public Image ImageHamburger
        {
            get { return _imageHamburger; }
        }

        private Image _imageBanner = new Image();
        public Image ImageBanner
        {
            get { return _imageBanner; }
        }

        private Image _imageSearch = new Image();
        public Image ImageSearch
        {
            get { return _imageSearch; }
        }

        private SearchBar _searchBar = new SearchBar();
        public SearchBar SearchBar
        {
            get { return _searchBar; }
        }

        private Button _buttonHamburger = new Button();
        public Button ButtonHamburger
        {
            get { return _buttonHamburger; }
        }

        private Button _buttonBanner = new Button();
        public Button ButtonBanner
        {
            get { return _buttonBanner; }
        }

        private Button _buttonSearch = new Button();
        public Button ButtonSearch
        {
            get { return _buttonSearch; }
        }

        public PONavSearchBar ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            string imagePathHamburger = AppTheme.HamburgerDefault;
            string imagePathBanner = AppTheme.BannerDefault;
            string imagePathSearch = AppTheme.SearchDefault;
            Color colorBackground = Color.White;
            Color colorBackgroundSearchiOS = Color.FromHex("#6C6C6C");

            if (DataService.Theme != null)
            {
                if (DataService.Theme.IconHamburgerImage != null)
                    imagePathHamburger = DataService.Theme.IconHamburgerImage.GetFileContentPath();

                if (DataService.Theme.BannerImage != null)
                    imagePathBanner = DataService.Theme.BannerImage.GetFileContentPath();

                if (DataService.Theme.IconSearchImage != null)
                    imagePathSearch = DataService.Theme.IconSearchImage.GetFileContentPath();
            }

            Grid main = new Grid ()
            {
                BackgroundColor = colorBackground,
                Padding = new Thickness(0, Device.OnPlatform(((HasIosTopPadding) ? 20 : 0), 0, 0), 0, 0),

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(15) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(15) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(15) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(15) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(50) }
                }
            };

            _imageHamburger.Source = imagePathHamburger;
            main.Children.Add(_imageHamburger, 1, 0);

            _buttonHamburger.BackgroundColor = Color.Transparent;
            _buttonHamburger.Margin = Device.OnPlatform(0, -4, 0);
            main.Children.Add(_buttonHamburger, 0, 0);
            Grid.SetColumnSpan(_buttonHamburger, 3);

            _imageBanner.Source = imagePathBanner;
            main.Children.Add(_imageBanner, 3, 0);

            _buttonBanner.BackgroundColor = Color.Transparent;
            _buttonBanner.Margin = Device.OnPlatform(0, -4, 0);
            _buttonBanner.IsEnabled = true;
            _buttonBanner.IsVisible = true;
            main.Children.Add(_buttonBanner, 3, 0);

            _searchBar.BackgroundColor = Color.Transparent;
            _searchBar.IsEnabled = false;
            _searchBar.IsVisible = false;
            _searchBar.Placeholder = "Search";
            main.Children.Add(_searchBar, 3, 0);

            _imageSearch.Source = imagePathSearch;
            main.Children.Add(_imageSearch, 5, 0);

             _buttonSearch.BackgroundColor = Color.Transparent;
            _buttonSearch.Margin = Device.OnPlatform(0, -4, 0);
            main.Children.Add(_buttonSearch, 4, 0);
            Grid.SetColumnSpan(_buttonSearch, 3);

            _buttonSearch.Clicked += (s, e) =>
            {
                _isSearching = !_isSearching;

                _searchBar.Text = string.Empty;
                main.BackgroundColor = Device.OnPlatform(((_isSearching) ? colorBackgroundSearchiOS : colorBackground), colorBackground, colorBackground);

                _searchBar.IsEnabled = _searchBar.IsVisible = _isSearching;
                _imageBanner.IsEnabled = _imageBanner.IsVisible = !_isSearching;
                _buttonBanner.IsEnabled = _buttonBanner.IsVisible = !_isSearching;
            };

            Content = main;
        }
    }
}
