﻿using ProjectOrange.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectOrange.Models
{
    public class AppCategory
    {
        public const string IconDefaultPathDefault = AppTheme.CategoryIconDefault;

        public string ErrorMessage { get; set; }
        public string Id { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Icon { get; set; }
        public int AllowUpload { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string ViewColor { get; set; }
        public string CategoryId { get; set; }
        public string ParentId { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string IconName { get; set; }

        public bool IconFileContentCached { get; set; }
        public AppFileContent IconFileContent { get; set; }

        public string IconFileContentDefaultPath { get; set; }

        public List<AppCategory> SubCategories { get; }
        public List<AppArticle> Articles { get; }

        public Color CategoryViewColor
        {
            get
            {
                return UtilService.SafeFromHex(ViewColor, Color.FromHex(AppTheme.ButtonColorDefault));
            }
        }

        public AppCategory ()
        {
            ErrorMessage = string.Empty;
            Id = string.Empty;
            ModifiedBy = string.Empty;
            ModifiedDate = string.Empty;
            Icon = string.Empty;
            AllowUpload = 0;
            Name = string.Empty;
            SortOrder = 0;
            ViewColor = AppTheme.ButtonColorDefault;
            CategoryId = string.Empty;
            ParentId = string.Empty;
            CreatedDate = string.Empty;
            CreatedBy = string.Empty;
            IconName = string.Empty;

            IconFileContentCached = false;
            IconFileContent = new AppFileContent();

            IconFileContentDefaultPath = IconDefaultPathDefault;

            SubCategories = new List<AppCategory>();
            Articles = new List<AppArticle>();
        }

        public AppCategory (AppCategory category)
        {
            ErrorMessage = category.ErrorMessage;
            Id = category.Id;
            ModifiedBy = category.ModifiedBy;
            ModifiedDate = category.ModifiedDate;
            Icon = category.Icon;
            AllowUpload = category.AllowUpload;
            Name = category.Name;
            SortOrder = category.SortOrder;
            ViewColor = category.ViewColor;
            CategoryId = category.CategoryId;
            ParentId = category.ParentId;
            CreatedDate = category.CreatedDate;
            CreatedBy = category.CreatedBy;

            IconFileContentCached = category.IconFileContentCached;
            IconFileContent = category.IconFileContent;

            IconFileContentDefaultPath = category.IconFileContentDefaultPath;

            SubCategories = category.SubCategories;
            Articles = category.Articles;
        }

        public bool AllowUploading ()
        {
            return (AllowUpload == 1 && DataService.AllowUploading) ? true : false;
        }

        public string GetIconFileContentPath ()
        {
            string path = IconFileContentDefaultPath;

            if (string.IsNullOrEmpty(Icon) || string.IsNullOrEmpty(IconName))
                return path;

            if (IconFileContentCached)
                path = StorageService.GetIconImagePath(DataService.SiteCode, IconName);

            return path;
        }

        public async Task<bool> DownloadIcon (string siteCode)
        {
            if (string.IsNullOrEmpty(Icon))
                return false;

            if (IconName.IsNullOrEmpty())
                IconName = Path.GetFileName(Icon);
            string iconsFolderName = StorageService.IconFolderName;
            DateTime lastWriteTime = StorageService.DateTimeMinValue;
            IconFileContentCached = await DataService.LoadFileContent(Icon, IconName, iconsFolderName, ModifiedBy, ModifiedDate,
                                                                      (fc, dt) => { IconFileContent = fc; lastWriteTime = dt; });

            if (IconFileContentCached)
                await IconFileContent.WriteToFile(lastWriteTime, DataService.SiteCode, iconsFolderName);

            return IconFileContentCached;
        }

        public AppCategory Clone ()
        {
            return new AppCategory(this);
        }

    }
}
