using Android.App;
using Android.Text.Method;
using Android.Views;
using Android.Widget;
using ProjectOrange.CustomViews;
using ProjectOrange.Droid.CustomRenderers;
using ProjectOrange.Droid.DependencyServices;
using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(AuthenticatingWebView), typeof(AuthenticatingWebViewRenderer))]
namespace ProjectOrange.Droid.CustomRenderers
{
    public class AuthenticatingWebViewRenderer : WebViewRenderer
    {
        private string _lastUrl;
        private WebViewSource _lastSource;
        private WebNavigationEvent _lastNavigationEvent;

        public new AuthenticatingWebView Element { get { return (AuthenticatingWebView) base.Element; } }

        protected override void OnElementChanged (ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);

            var proxiedClient = CreateXamarinWebViewClient();
            var proxyClient = new AuthenticatingWebViewClient(this, proxiedClient);

            Control.SetWebViewClient(proxyClient);

            if (e.OldElement != null)
                ((WebView)e.OldElement).Navigating -= HandleElementNavigating;

            if (e.NewElement != null)
                ((WebView)e.NewElement).Navigating += HandleElementNavigating;
        }

        private void HandleElementNavigating (object sender, WebNavigatingEventArgs e)
        {
            _lastUrl = e.Url;
            _lastSource = e.Source;
            _lastNavigationEvent = e.NavigationEvent;
        }

        #region Reflections
        private Type _xamarinWebViewClientType;
        private Type XamarinWebViewClientType
        {
            get
            {
                if (_xamarinWebViewClientType == null)
                    _xamarinWebViewClientType = typeof(WebViewRenderer).GetNestedType("WebClient", BindingFlags.NonPublic);

                return _xamarinWebViewClientType;
            }
        }

        private ConstructorInfo _xamarinWebViewClientConstructorInfo;
        private ConstructorInfo XamarinWebViewClientConstructorInfo
        {
            get
            {
                if (_xamarinWebViewClientConstructorInfo == null)
                {
                    if (XamarinWebViewClientType != null)
                        _xamarinWebViewClientConstructorInfo = XamarinWebViewClientType.GetConstructor(new[] { typeof(WebViewRenderer) });
                }

                return _xamarinWebViewClientConstructorInfo;
            }
        }

        private Android.Webkit.WebViewClient CreateXamarinWebViewClient ()
        {
            if (XamarinWebViewClientConstructorInfo != null)
                return (Android.Webkit.WebViewClient) XamarinWebViewClientConstructorInfo.Invoke(new[] { this });
            else
                return null;
        }
        #endregion

        private class AuthenticatingWebViewClient : Android.Webkit.WebViewClient
        {
            private readonly AuthenticatingWebViewRenderer _renderer;
            private readonly Android.Webkit.WebViewClient _originalClient;

            public AuthenticatingWebViewClient (AuthenticatingWebViewRenderer renderer, Android.Webkit.WebViewClient originalClient)
            {
                _renderer = renderer;
                _originalClient = originalClient;
            }

            public override void DoUpdateVisitedHistory (Android.Webkit.WebView view, string url, bool isReload)
            {
                if (_originalClient != null)
                    _originalClient.DoUpdateVisitedHistory(view, url, isReload);
            }

            public override void OnFormResubmission (Android.Webkit.WebView view, Android.OS.Message dontResend, Android.OS.Message resend)
            {
                if (_originalClient != null)
                    _originalClient.OnFormResubmission(view, dontResend, resend);
            }

            public override void OnLoadResource (Android.Webkit.WebView view, string url)
            {
                if (_originalClient != null)
                    _originalClient.OnLoadResource(view, url);
            }

            public override void OnPageFinished (Android.Webkit.WebView view, string url)
            {
                if (_originalClient != null)
                    _originalClient.OnPageFinished(view, url);
            }

            public override void OnPageStarted (Android.Webkit.WebView view, string url, Android.Graphics.Bitmap favicon)
            {
                if (_originalClient != null)
                    _originalClient.OnPageStarted(view, url, favicon);
            }

            [Obsolete("deprecated")]
            public override void OnReceivedError (Android.Webkit.WebView view, Android.Webkit.ClientError errorCode, string description, string failingUrl)
            {
                if (_originalClient != null)
                    _originalClient.OnReceivedError(view, errorCode, description, failingUrl);
            }

            public override void OnReceivedHttpAuthRequest (Android.Webkit.WebView view, Android.Webkit.HttpAuthHandler handler, string host, string realm)
            {
                if (_originalClient != null)
                {
                    string username = "username";
                    string password = "password";
                    Activity currentActivity = Forms.Context as Activity;

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.FillParent, LayoutParams.FillParent);
                    layoutParams.SetMargins(50, 0, 50, 0);

                    EditText ediTextUsername = new EditText (currentActivity)
                    {
                        Hint = "username",
                        InputType = Android.Text.InputTypes.TextVariationVisiblePassword,
                        LayoutParameters = layoutParams,
                        Text = string.Empty
                    };
                    ediTextUsername.SetMaxLines(1);
                    ediTextUsername.SetSingleLine(true);

                    EditText editTextPassword = new EditText (currentActivity)
                    {
                        Hint = "password",
                        InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPassword,
                        LayoutParameters = layoutParams,
                        Text = string.Empty,
                        TransformationMethod = PasswordTransformationMethod.Instance
                    };
                    editTextPassword.SetMaxLines(1);
                    editTextPassword.SetSingleLine(true);

                    LinearLayout linearLayout = new LinearLayout (currentActivity)
                    {
                        Orientation = Orientation.Vertical
                    };
                    linearLayout.AddView(ediTextUsername, LayoutParams.MatchParent);
                    linearLayout.AddView(editTextPassword, LayoutParams.MatchParent);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(currentActivity).
                                                             SetCancelable(false).
                                                             SetTitle("Login Required").
                                                             SetView(linearLayout).
                                                             SetPositiveButton("OK", (s, a) =>
                                                             {
                                                                 username = ediTextUsername.Text;
                                                                 password = editTextPassword.Text;

                                                                 handler.Proceed(username, password);
                                                                 _originalClient.OnReceivedHttpAuthRequest(view, handler, host, realm);
                                                             }).
                                                             SetNegativeButton("Cancel", (s, a) =>
                                                             {
                                                                 _originalClient.OnReceivedHttpAuthRequest(view, handler, host, realm);
                                                             });

                    AlertDialog alertDialog = alertDialogBuilder.Create();

                    alertDialog.Show();
                }
            }

            public override void OnReceivedSslError (Android.Webkit.WebView view, Android.Webkit.SslErrorHandler handler, Android.Net.Http.SslError error)
            {
                bool success = false;

                if (_renderer.Element.ShouldTrustUnknownCertificate != null)
                {
                    var certificate = new AuthenticatingWebViewCertificate(error.Url, error.Certificate);
                    var result = _renderer.Element.ShouldTrustUnknownCertificate(certificate);

                    if (result)
                    {
                        success = true;
                        handler.Proceed();
                    }
                    else
                        handler.Cancel();
                }

                if (!success)
                    SendNavigated(new WebNavigatedEventArgs(_renderer._lastNavigationEvent, _renderer._lastSource, _renderer._lastUrl, WebNavigationResult.Failure));
            }

            public override void OnScaleChanged (Android.Webkit.WebView view, float oldScale, float newScale)
            {
                if (_originalClient != null)
                    _originalClient.OnScaleChanged(view, oldScale, newScale);
            }

            [Obsolete("deprecated")]
            public override void OnTooManyRedirects (Android.Webkit.WebView view, Android.OS.Message cancelMsg, Android.OS.Message continueMsg)
            {
                if (_originalClient != null)
                    _originalClient.OnTooManyRedirects(view, cancelMsg, continueMsg);
            }

            [Obsolete("deprecated")]
            public override void OnUnhandledKeyEvent (Android.Webkit.WebView view, Android.Views.KeyEvent e)
            {
                if (_originalClient != null)
                    _originalClient.OnUnhandledKeyEvent(view, e);
            }

            public override bool ShouldOverrideKeyEvent (Android.Webkit.WebView view, Android.Views.KeyEvent e)
            {
                if (_originalClient != null)
                    return _originalClient.ShouldOverrideKeyEvent(view, e);
                else
                    return false;
            }

            public override bool ShouldOverrideUrlLoading (Android.Webkit.WebView view, string url)
            {
                if (_originalClient != null)
                    return _originalClient.ShouldOverrideUrlLoading(view, url);
                else
                    return false;
            }

            #region Reflections
            private delegate void SendNavigatedDelegate (WebNavigatedEventArgs args);

            private MethodInfo _sendNavigatedMethodInfo;
            private MethodInfo SendNavigatedMethodInfo
            {
                get
                {
                    if (_sendNavigatedMethodInfo == null)
                    {
                        _sendNavigatedMethodInfo = typeof(WebView).GetMethod(name: "SendNavigated",
                                                                             bindingAttr: BindingFlags.Instance | BindingFlags.NonPublic,
                                                                             binder: null,
                                                                             types: new[] { typeof(WebNavigatedEventArgs) },
                                                                             modifiers: null);
                    }

                    return _sendNavigatedMethodInfo;
                }
            }

            private void SendNavigated (WebNavigatedEventArgs args)
            {
                var method = SendNavigatedMethodInfo;

                if (method != null)
                {
                    var methodDelegate = (SendNavigatedDelegate) method.CreateDelegate(typeof(SendNavigatedDelegate), _renderer.Element);

                    if (methodDelegate != null)
                        methodDelegate(args);
                }
            }

            #endregion
        }
    }
}