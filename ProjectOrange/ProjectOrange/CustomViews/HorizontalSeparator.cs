﻿using Xamarin.Forms;

namespace ProjectOrange.CustomViews
{
    public class HorizontalSeparator : BoxView
    {
        public HorizontalSeparator ()
        {
            Color = Color.White;
            HeightRequest = 1;
        }
    }
}
