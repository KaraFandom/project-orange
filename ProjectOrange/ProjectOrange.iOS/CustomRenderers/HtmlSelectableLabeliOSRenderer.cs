using Foundation;
using ProjectOrange.CustomViews;
using ProjectOrange.iOS.CustomRenderers;
using System;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(HtmlSelectableLabeliOS), typeof(HtmlSelectableLabeliOSRenderer))]
namespace ProjectOrange.iOS.CustomRenderers
{
    public class HtmlSelectableLabeliOSRenderer : ViewRenderer<HtmlSelectableLabeliOS, UITextView>
    {
        HtmlSelectableLabeliOS htmlSelectableLabel;
        UITextView uiTextView;

        protected override void OnElementChanged (ElementChangedEventArgs<HtmlSelectableLabeliOS> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                uiTextView = new UITextView();
                SetNativeControl(uiTextView);
            }

            if (e.NewElement != null)
                htmlSelectableLabel = e.NewElement as HtmlSelectableLabeliOS;

            if (Control != null && htmlSelectableLabel != null)
                Redraw();
        }

        protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == HtmlSelectableLabeliOS.SourceProperty.PropertyName ||
                e.PropertyName == HtmlSelectableLabeliOS.FontSizeProperty.PropertyName ||
                e.PropertyName == HtmlSelectableLabeliOS.TextColorProperty.PropertyName)
                Redraw();
        }

        private void Redraw ()
        {
            Control.Editable = false;
            Control.Selectable = true;
            Control.DataDetectorTypes = UIDataDetectorType.All;

            NSAttributedStringDocumentAttributes attributes = new NSAttributedStringDocumentAttributes() { DocumentType = NSDocumentType.HTML };
            NSError error = new NSError();

            Control.AttributedText = new NSAttributedString(NSData.FromString(htmlSelectableLabel.Source, NSStringEncoding.Unicode), attributes, ref error);

            Control.Font = UIFont.FromName("Helvetica", (nfloat) htmlSelectableLabel.FontSize);
            Control.TextColor = htmlSelectableLabel.TextColor.ToUIColor();
        }
    }
}