﻿using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using ProjectOrange.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class POCategoryView1 : ContentView
    {
        public static readonly BindableProperty CategoryProperty = BindableProperty.Create(nameof(Category),
                                                                                           typeof(AppCategory),
                                                                                           typeof(POCategoryView1),
                                                                                           new AppCategory(),
                                                                                           BindingMode.TwoWay,
                                                                                           null,
                                                                                           (bindable, oldValue, newValue) =>
                                                                                           {
                                                                                               POCategoryView1 categoryView1 = (POCategoryView1) bindable;
                                                                                               categoryView1.Category = newValue as AppCategory;
                                                                                               categoryView1.Redraw();
                                                                                           });

        private string _filterQuery = string.Empty;
        public string FilterQuery
        {
            get { return _filterQuery; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _filterQuery = string.Empty;
                else
                    _filterQuery = value;

                Redraw();
            }
        }

        private ScrollView _scrollView = new ScrollView();
        public ScrollView ScrollView
        {
            get { return _scrollView; }
        }

        public AppCategory Category
        {
            get { return (AppCategory) GetValue(CategoryProperty); }
            set { SetValue(CategoryProperty, value); }
        }

        public POCategoryView1 ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            if (Category == null)
                return;

            #region Initialization
            double contentHMargin = 5;
            string imagePathArrowBack = AppTheme.ArrowBackDefault;
            string imagePathArrowCollapse = AppTheme.ArrowCollapseDefault;
            string imagePathArrowExpand = AppTheme.ArrowExpandDefault;
            string imagePathArrowNext = AppTheme.ArrowNextDefault;
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);

            if (DataService.Theme != null)
            {
                if (DataService.Theme.IconArrowBackImage != null)
                    imagePathArrowBack = DataService.Theme.IconArrowBackImage.GetFileContentPath();

                if (DataService.Theme.IconArrowCollapseImage != null)
                    imagePathArrowCollapse = DataService.Theme.IconArrowCollapseImage.GetFileContentPath();

                if (DataService.Theme.IconArrowExpandImage != null)
                    imagePathArrowExpand = DataService.Theme.IconArrowExpandImage.GetFileContentPath();

                if (DataService.Theme.IconArrowNextImage != null)
                    imagePathArrowNext = DataService.Theme.IconArrowNextImage.GetFileContentPath();

                textColor = DataService.Theme.TextColorValue;
            }

            bool allowUpload = Category.AllowUploading();
            double screenWidth = Application.Current.MainPage.Width;
            double contentWidth = screenWidth - (contentHMargin * 2);
            double contentSubWidth = screenWidth - (contentHMargin * 4);
            double contentTitleFontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Default, NamedSize.Default, NamedSize.Default), typeof(Label));
            double elementTitleFontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Small, NamedSize.Small, NamedSize.Small), typeof(Label));
            Color elementTitleColor = Color.Black;
            Color elementBackgroundColor = Color.White;
            Color elementGrayBackgroundColor = new Color(0.925, 0.925, 0.925);
            TextAlignment titleHAlign = TextAlignment.Start;
            TextAlignment titleVAlign = TextAlignment.Center;

            ScrollView main = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0,

                Margin = new Thickness(contentHMargin, contentHMargin)
            };

            #region Content.Header
            Grid gridHeader = CreateHeaderGrid();
            content.Children.Add(gridHeader);

            Image imageHeaderBack = new Image ()
            {
                Source = imagePathArrowBack
            };
            gridHeader.Children.Add(imageHeaderBack, 1, 0);

            Label labelHeaderTitle = new Label ()
            {
                Text = Category.Name,
                TextColor = textColor,

                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            gridHeader.Children.Add(labelHeaderTitle, 2, 0);

            Image imageHeaderIcon = new Image ()
            {
                Source = Category.GetIconFileContentPath()
            };
            gridHeader.Children.Add(imageHeaderIcon, 3, 0);

            Button buttonHeaderBack = new Button ()
            {
                BackgroundColor = Color.Transparent
            };
            buttonHeaderBack.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            gridHeader.Children.Add(buttonHeaderBack, 0, 0);
            Grid.SetColumnSpan(buttonHeaderBack, 5);
            #endregion

            #region Content.Articles
            List<AppArticle> articles = Category.Articles.Where(x => !string.IsNullOrEmpty(x.Title) && x.Title.Contains(FilterQuery)).ToList();

            int gridColumns = 2;
            int gridRows = (int) Math.Ceiling(((double) (articles.Count + (allowUpload ? 1 : 0))) / gridColumns);
            double gridColumnWidth = contentWidth / gridColumns;

            double cellHeight = gridColumnWidth;
            double cellIconHeight = cellHeight * 0.4;
            double cellLabelHeight = 30;
            double cellPadHeight = cellHeight - cellIconHeight - cellLabelHeight;

            double cellTextHeight = gridColumnWidth;
            double cellTextLabelHeight = 30;
            double cellTextContentHeight = cellTextHeight * 0.6;
            double cellTextPadHeight = cellTextHeight - cellTextLabelHeight - cellTextContentHeight;

            Grid grid = new Grid ()
            {
                ColumnSpacing = 2,
                RowSpacing = 2
            };
            content.Children.Add(grid);

            for (int i = 0; i < gridColumns; i++)
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            for (int i = 0; i < gridRows; i++)
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(cellHeight) });

            if (articles != null && articles.Count > 0)
            {
                string articleTitle = string.Empty;
                string articleFileName = string.Empty;
                AppArticleType articleType = AppArticleType.File;
                AppArticle article;

                for (int i = 0; i < articles.Count; i++)
                {
                    article = articles[i];

                    if (article == null)
                        continue;

                    articleTitle = article.Title;
                    articleFileName = article.GetFriendlyFileName();
                    articleType = article.GetArticleType();

                    if (articleType.Equals(AppArticleType.Text) || articleType.Equals(AppArticleType.DirectText))
                    {
                        string text = (articleType.Equals(AppArticleType.DirectText)) ? article.Value : article.TextValue;

                        View articleTextElement = CreateArticleTextView(article, cellTextContentHeight, cellTextLabelHeight, cellTextPadHeight, elementBackgroundColor, elementTitleFontSize, elementTitleFontSize * 0.8, elementTitleColor);

                        if (articleTextElement != null)
                            grid.Children.Add(articleTextElement, (i % gridColumns), ((int) Math.Floor((double) i / gridColumns)));
                    }
                    else
                    {
                        View articleElement = CreateArticleElementView(article, cellIconHeight, cellLabelHeight, cellPadHeight, elementBackgroundColor, elementTitleFontSize, elementTitleColor);

                        if (articleElement != null)
                            grid.Children.Add(articleElement, (i % gridColumns), ((int) Math.Floor((double) i / gridColumns)));
                    }
                }
            }

            if (allowUpload)
            {
                View addArticleElement = CreateAddArticleElementView(cellIconHeight, cellLabelHeight, cellPadHeight, elementGrayBackgroundColor, elementTitleFontSize, elementTitleColor);

                if (addArticleElement != null)
                    grid.Children.Add(addArticleElement, (articles.Count % gridColumns), ((int) Math.Floor((double) articles.Count / gridColumns)));
            }
            #endregion

            #region Content.SubCategories
            List<AppCategory> subCategories = Category.SubCategories.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.ToLower().Contains(FilterQuery)).ToList();
            if (subCategories != null && subCategories.Count > 0)
            {
                bool hasArticles = false;
                bool hasSubCategories = false;
                bool hasNoChildren = false;
                bool hasOnlySubCategories = false;
                bool hasOnlyOneTextArticle = false;
                string subCategoryName = string.Empty;
                Color subViewColor = Color.Transparent;

                foreach (AppCategory subCategory in subCategories)
                {
                    if (subCategory == null)
                        continue;

                    hasArticles = (subCategory.Articles != null && subCategory.Articles.Count > 0);
                    hasSubCategories = (subCategory.SubCategories != null && subCategory.SubCategories.Count > 0);
                    hasNoChildren = (!hasArticles && !hasSubCategories);
                    hasOnlySubCategories = (!hasArticles && hasSubCategories);
                    hasOnlyOneTextArticle = (hasArticles && !hasSubCategories &&
                                            subCategory.Articles.Count == 1 && subCategory.Articles[0] != null &&
                                            (subCategory.Articles[0].GetArticleType().Equals(AppArticleType.Text) || subCategory.Articles[0].GetArticleType().Equals(AppArticleType.DirectText)));
                    subCategoryName = subCategory.Name;

                    subViewColor = UtilService.SafeFromHex(subCategory.ViewColor, Color.Transparent);

                    content.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 10 });

                    Grid gridContent = CreateContentGrid(subViewColor);
                    content.Children.Add(gridContent);

                    Image imageContentIcon = new Image ()
                    {
                        Source = subCategory.GetIconFileContentPath()
                    };
                    gridContent.Children.Add(imageContentIcon, 1, 0);

                    Label labelContentTitle = new Label ()
                    {
                        Text = subCategoryName,

                        FontSize = contentTitleFontSize,
                        TextColor = textColor,
                        HorizontalTextAlignment = titleHAlign,
                        VerticalTextAlignment = titleVAlign
                    };
                    gridContent.Children.Add(labelContentTitle, 3, 0);

                    Image imageContentExpandCollapse = new Image ()
                    {
                        Source = (hasNoChildren || hasOnlySubCategories) ? imagePathArrowNext : imagePathArrowExpand
                    };
                    gridContent.Children.Add(imageContentExpandCollapse, 4, 0);

                    Button buttonContent = new Button ()
                    {
                        BackgroundColor = Color.Transparent
                    };
                    gridContent.Children.Add(buttonContent, 0, 0);
                    Grid.SetColumnSpan(buttonContent, 4);

                    if (hasNoChildren || hasOnlySubCategories)
                    {
                        buttonContent.CommandParameter = subCategory;
                        buttonContent.SetBinding(Button.CommandProperty, "NavigateToSubPageCommand");
                    }
                    else
                    {
                        StackLayout stackContentBody = new StackLayout ()
                        {
                            IsVisible = false,
                            BackgroundColor = subViewColor,

                            Spacing = 0
                        };
                        content.Children.Add(stackContentBody);

                        buttonContent.Clicked += (s, e) =>
                        {
                            stackContentBody.IsVisible = !stackContentBody.IsVisible;

                            if (stackContentBody.IsVisible)
                                imageContentExpandCollapse.Source = imagePathArrowCollapse;
                            else
                                imageContentExpandCollapse.Source = imagePathArrowExpand;
                        };

                        List<AppArticle> subCategoryArticle = subCategory.Articles.ToList();

                        int gridSubColumns = 2;
                        int gridSubRows = (int) Math.Ceiling(((double) subCategoryArticle.Count + 1) / gridSubColumns);
                        double gridSubColumnWidth = contentSubWidth / gridSubColumns;

                        double cellSubHeight = gridSubColumnWidth;
                        double cellSubIconHeight = cellSubHeight * 0.4;
                        double cellSubLabelHeight = 30;
                        double cellSubPadHeight = cellSubHeight - cellSubIconHeight - cellSubLabelHeight;

                        double cellSubTextHeight = gridSubColumnWidth;
                        double cellSubTextLabelHeight = 30;
                        double cellSubTextContentHeight = cellSubTextHeight * 0.6;
                        double cellSubTextPadHeight = cellSubTextHeight - cellSubTextLabelHeight - cellSubTextContentHeight;

                        Grid gridSub = new Grid ()
                        {
                            ColumnSpacing = 2,
                            RowSpacing = 2,

                            Margin = new Thickness(contentHMargin, contentHMargin)
                        };
                        stackContentBody.Children.Add(gridSub);

                        for (int i = 0; i < gridSubColumns; i++)
                            gridSub.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                        for (int i = 0; i < gridSubRows; i++)
                            gridSub.RowDefinitions.Add(new RowDefinition { Height = new GridLength(cellSubHeight) });

                        if (subCategoryArticle != null && subCategoryArticle.Count > 0)
                        {
                            string articleTitle = string.Empty;
                            string articleFileName = string.Empty;
                            AppArticleType articleType = AppArticleType.File;
                            AppArticle article;
                            for (int i = 0; i < subCategoryArticle.Count; i++)
                            {
                                article = subCategoryArticle[i];

                                if (article == null)
                                    continue;

                                articleTitle = article.Title;
                                articleFileName = article.GetFriendlyFileName();
                                articleType = article.GetArticleType();

                                if (articleType.Equals(AppArticleType.Text) || articleType.Equals(AppArticleType.DirectText))
                                {
                                    string text = (articleType.Equals(AppArticleType.DirectText)) ? article.Value : article.TextValue;

                                    View articleTextElement = CreateArticleTextView(article, cellSubTextContentHeight, cellSubTextLabelHeight, cellSubTextPadHeight, elementBackgroundColor, elementTitleFontSize, elementTitleFontSize * 0.8, elementTitleColor);

                                    if (articleTextElement != null)
                                        gridSub.Children.Add(articleTextElement, (i % gridSubColumns), ((int) Math.Floor((double) i / gridSubColumns)));
                                }
                                else
                                {
                                    View articleElement = CreateArticleElementView(article, cellSubIconHeight, cellSubLabelHeight, cellSubPadHeight, elementBackgroundColor, elementTitleFontSize, elementTitleColor);

                                    if (articleElement != null)
                                        gridSub.Children.Add(articleElement, (i % gridSubColumns), ((int) Math.Floor((double) i / gridSubColumns)));
                                }
                            }
                        }

                        View subCategorySubCategoryElement = CreateSubCategoryElementView(subCategory, cellSubIconHeight, cellSubLabelHeight, cellSubPadHeight, elementBackgroundColor, elementTitleFontSize, elementTitleColor);

                        if (subCategorySubCategoryElement != null)
                            gridSub.Children.Add(subCategorySubCategoryElement, (subCategoryArticle.Count % gridSubColumns), ((int) Math.Floor((double) subCategoryArticle.Count / gridSubColumns)));
                    }
                }
            }
            #endregion

            #region Content.ComingSoon
            if (!allowUpload)
            {
                if ((Category.Articles != null && Category.Articles.Count > 0) ||
                    (Category.SubCategories != null && Category.SubCategories.Count > 0))
                { }
                else
                {
                    StackLayout stackComingSoon = new StackLayout ()
                    {
                        BackgroundColor = Color.Transparent,
                        Padding = new Thickness(15, 15),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    content.Children.Add(stackComingSoon);

                    Label labelComingSoon = new Label ()
                    {
                        Text = "Coming Soon",
                        TextColor = textColor,
                        FontSize = contentTitleFontSize,
                        HorizontalTextAlignment = TextAlignment.Center,
                        VerticalTextAlignment = TextAlignment.Center
                    };
                    stackComingSoon.Children.Add(labelComingSoon);
                }
            }
            #endregion
            #endregion

            main.Content = content;
            _scrollView = main;
            Content = main;
        }

        private View CreateArticleElementView (AppArticle article, double cellIconHeight, double cellLabelHeight, double cellPadHeight, Color viewColor, double titleFontSize, Color titleColor)
        {
            if (article == null)
                return null;

            bool isArticleIconAvailable = article.IsArticleIconAvailable();
            string imagePathArticle = isArticleIconAvailable ? article.GetIconFileContentPath() : article.GetFileExtensionImagePath();
            string articleTitle = article.Title;
            AppArticleType articleType = article.GetArticleType();

            Grid gridArticleElement = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(cellPadHeight / 2) },
                    new RowDefinition { Height = new GridLength(cellIconHeight - 5) },
                    new RowDefinition { Height = new GridLength(5) },
                    new RowDefinition { Height = new GridLength(isArticleIconAvailable ? cellPadHeight / 2 : cellLabelHeight) },
                    new RowDefinition { Height = new GridLength(isArticleIconAvailable ? cellLabelHeight : cellPadHeight / 2) }
                }
            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
            {

            };

            BoxView boxBackground = new BoxView ()
            {
                Color = viewColor
            };
            boxBackground.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(boxBackground, 0, 0);
            Grid.SetRowSpan(boxBackground, 5);

            Image imageArticle = new Image ()
            {
                Source = imagePathArticle
            };
            imageArticle.GestureRecognizers.Add(tapGestureRecognizer);
            if (!isArticleIconAvailable)
                gridArticleElement.Children.Add(imageArticle, 0, 1);
            else
            {
                gridArticleElement.Children.Add(imageArticle, 0, 0);
                Grid.SetRowSpan(imageArticle, 5);
            }

            Label labelArticle = new Label ()
            {
                Text = articleTitle,

                Margin = isArticleIconAvailable ? new Thickness(10, 0, 0, 10) : new Thickness(10, 0),
                TextColor = titleColor,
                FontSize = titleFontSize,
                HorizontalTextAlignment = isArticleIconAvailable ? TextAlignment.Start : TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            labelArticle.GestureRecognizers.Add(tapGestureRecognizer);
            if (!isArticleIconAvailable)
                gridArticleElement.Children.Add(labelArticle, 0, 3);
            else
                gridArticleElement.Children.Add(labelArticle, 0, 4);

            if (articleType.Equals(AppArticleType.Link) || articleType.Equals(AppArticleType.LinkLogin) || articleType.Equals(AppArticleType.LinkExternal) ||
                articleType.Equals(AppArticleType.LinkAutoFill) || articleType.Equals(AppArticleType.LinkCredential))
            {
                string link = article.Value;

                if (string.IsNullOrEmpty(link))
                    link = string.Empty;

                if (articleType.Equals(AppArticleType.LinkAutoFill))
                {
                    string username = DataService.Username;
                    string password = DataService.Password;

                    if (string.IsNullOrEmpty(username)) username = "username";
                    if (string.IsNullOrEmpty(password)) password = "password";

                    tapGestureRecognizer.CommandParameter = new AppLinkFill { Title = articleTitle, Value = link, Username = username, Password = password };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebFillCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkCredential))
                {
                    string domain = DataService.BrowserCredentials.Domain;
                    string username = DataService.BrowserCredentials.Username;
                    string password = DataService.BrowserCredentials.Password;

                    if (string.IsNullOrEmpty(username)) domain = string.Empty;
                    if (string.IsNullOrEmpty(username)) username = "username";
                    if (string.IsNullOrEmpty(password)) password = "password";

                    tapGestureRecognizer.CommandParameter = new AppLinkFill { Title = articleTitle, Value = link, Username = (domain + username), Password = password };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebFillCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkExternal))
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebExternalCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkLogin))
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToAuthenticatingWebCommand");
                }
                else
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebCommand");
                }
            }
            else
            {
                tapGestureRecognizer.CommandParameter = article;
                tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToFileAttachmentCommand");
            }

            return gridArticleElement;
        }

        private View CreateArticleTextView (AppArticle article, double cellContentHeight, double cellLabelHeight, double cellPadHeight, Color viewColor, double titleFontSize, double bodyFontSize, Color textColor)
        {
            string title = article.Title;
            string text = (article.GetArticleType().Equals(AppArticleType.DirectText)) ? article.Value : article.TextValue;
            string textClean = Regex.Replace(text, "<.*?>", string.Empty);

            Grid gridArticleElement = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(cellPadHeight / 2) },
                    new RowDefinition { Height = new GridLength(cellLabelHeight) },
                    new RowDefinition { Height = new GridLength(cellContentHeight) },
                    new RowDefinition { Height = new GridLength(cellPadHeight / 2) }
                }
            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
            {

            };

            BoxView boxBackground = new BoxView ()
            {
                Color = viewColor
            };
            boxBackground.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(boxBackground, 0, 0);
            Grid.SetRowSpan(boxBackground, 4);

            Label labelArticle = new Label ()
            {
                Text = title,

                Margin = new Thickness(10, 10),
                TextColor = textColor,
                FontSize = titleFontSize,
                HorizontalTextAlignment = TextAlignment.Start,
                VerticalTextAlignment = TextAlignment.Start,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start
            };
            labelArticle.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(labelArticle, 0, 1);

            Label labelBody = new Label ()
            {
                Text = textClean,

                Margin = new Thickness(15, 0),
                TextColor = textColor,
                FontSize = bodyFontSize,
                HorizontalTextAlignment = TextAlignment.Start,
                VerticalTextAlignment = TextAlignment.Start,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Start
            };
            labelBody.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(labelBody, 0, 2);

            tapGestureRecognizer.CommandParameter = new AppLink { Title = title, Value = text };
            tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToTextPageCommand");

            return gridArticleElement;
        }

        private View CreateSubCategoryElementView (AppCategory category, double cellIconHeight, double cellLabelHeight, double cellPadHeight, Color viewColor, double titleFontSize, Color titleColor)
        {
            string imagePathArticleFileExtension = AppTheme.FileTypeIconMore;
            string title = "See More.";

            Grid gridArticleElement = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(cellPadHeight / 2) },
                    new RowDefinition { Height = new GridLength(cellIconHeight - 20) },
                    new RowDefinition { Height = new GridLength(20) },
                    new RowDefinition { Height = new GridLength(cellLabelHeight) },
                    new RowDefinition { Height = new GridLength(cellPadHeight / 2) }
                }
            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
            {

            };

            BoxView boxBackground = new BoxView ()
            {
                Color = viewColor
            };
            boxBackground.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(boxBackground, 0, 0);
            Grid.SetRowSpan(boxBackground, 5);

            Image imageArticle = new Image ()
            {
                Source = imagePathArticleFileExtension
            };
            imageArticle.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(imageArticle, 0, 1);

            Label labelArticle = new Label ()
            {
                Text = title,

                Margin = new Thickness(5, 5),
                TextColor = titleColor,
                FontSize = titleFontSize,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            labelArticle.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(labelArticle, 0, 3);

            tapGestureRecognizer.CommandParameter = category;
            tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToSubPageCommand");

            return gridArticleElement;
        }

        private View CreateAddArticleElementView (double cellIconHeight, double cellLabelHeight, double cellPadHeight, Color viewColor, double titleFontSize, Color titleColor)
        {
            string imagePathArticleFileExtension = AppTheme.FileTypeIconUpload;
            string title = "Add Article.";

            Grid gridArticleElement = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(cellPadHeight / 2) },
                    new RowDefinition { Height = new GridLength(cellIconHeight - 20) },
                    new RowDefinition { Height = new GridLength(20) },
                    new RowDefinition { Height = new GridLength(cellLabelHeight) },
                    new RowDefinition { Height = new GridLength(cellPadHeight / 2) }
                }
            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
            {

            };

            BoxView boxBackground = new BoxView ()
            {
                Color = viewColor
            };
            boxBackground.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(boxBackground, 0, 0);
            Grid.SetRowSpan(boxBackground, 5);

            Image imageArticle = new Image ()
            {
                Source = imagePathArticleFileExtension
            };
            imageArticle.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(imageArticle, 0, 1);

            Label labelArticle = new Label ()
            {
                Text = title,

                Margin = new Thickness(5, 5),
                TextColor = titleColor,
                FontSize = titleFontSize,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            labelArticle.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(labelArticle, 0, 3);

            tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToUploadPageCommand");

            return gridArticleElement;
        }

        private Grid CreateHeaderGrid ()
        {
            int headerHeight = Device.OnPlatform(55, 65, 65);

            Grid headerGrid = new Grid ()
            {
                BackgroundColor = Color.Transparent,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(40) },
                    new ColumnDefinition { Width = new GridLength(25) },
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(headerHeight) }
                }
            };

            return headerGrid;
        }

        private Grid CreateContentGrid (Color viewColor)
        {
            Grid contentGrid = new Grid ()
            {
                BackgroundColor = viewColor,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(40) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(25) },
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(50) }
                }
            };

            return contentGrid;
        }
    }
}
