﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Models;
using ProjectOrange.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectOrange.ViewModels
{
    public class SettingsPageViewModel : BindableBase, INavigationAware
    {
        private IEventAggregator _eventAggregator;

        private INavigationService _navigationService;

        public DelegateCommand ShowMasterDetailCommand { get; set; }

        public DelegateCommand SavePropertiesCommand { get; set; }

        public DelegateCommand ResetPropertiesCommand { get; set; }

        public DelegateCommand SetPinCodeCommand { get; set; }

        public DelegateCommand SavePinCodeCommand { get; set; }

        public DelegateCommand DeletePinCodeCommand { get; set; }

        public DelegateCommand CancelPinCodeCommand { get; set; }

        public DelegateCommand CleanCacheCommand { get; set; }

        private bool _isSettingPinCode_A;
        public bool IsSettingPinCode_A
        {
            get { return _isSettingPinCode_A; }
            set { SetProperty(ref _isSettingPinCode_A, value); }
        }

        private bool _isSettingPinCode_B;
        public bool IsSettingPinCode_B
        {
            get { return _isSettingPinCode_B; }
            set { SetProperty(ref _isSettingPinCode_B, value); }
        }

        private bool _isSettingPinCode_C;
        public bool IsSettingPinCode_C
        {
            get { return _isSettingPinCode_C; }
            set { SetProperty(ref _isSettingPinCode_C, value); }
        }

        private bool _isSettingPinCode_D;
        public bool IsSettingPinCode_D
        {
            get { return _isSettingPinCode_D; }
            set { SetProperty(ref _isSettingPinCode_D, value); }
        }

        private bool _isSettingPinCode_E;
        public bool IsSettingPinCode_E
        {
            get { return _isSettingPinCode_E; }
            set { SetProperty(ref _isSettingPinCode_E, value); }
        }

        private bool _isSettingPinCode_F;
        public bool IsSettingPinCode_F
        {
            get { return _isSettingPinCode_F; }
            set { SetProperty(ref _isSettingPinCode_F, value); }
        }

        private bool _isWarningsVisible = false;
        public bool IsWarningsVisible
        {
            get { return _isWarningsVisible; }
            set { SetProperty(ref _isWarningsVisible, value); }
        }

        private string _warningsText = string.Empty;
        public string WarningsText
        {
            get { return _warningsText; }
            set { SetProperty(ref _warningsText, value); }
        }

        private string _serverURL;
        public string ServerURL
        {
            get { return _serverURL; }
            set { SetProperty(ref _serverURL, value); }
        }

        private string _apiKey;
        public string ApiKey
        {
            get { return _apiKey; }
            set { SetProperty(ref _apiKey, value); }
        }

        private string _apiAccess;
        public string ApiAccess
        {
            get { return _apiAccess; }
            set { SetProperty(ref _apiAccess, value); }
        }

        private string _apiUsername;
        public string ApiUsername
        {
            get { return _apiUsername; }
            set { SetProperty(ref _apiUsername, value); }
        }

        private string _apiPassword;
        public string ApiPassword
        {
            get { return _apiPassword; }
            set { SetProperty(ref _apiPassword, value); }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _siteCode;
        public string SiteCode
        {
            get { return _siteCode; }
            set { SetProperty(ref _siteCode, value); }
        }

        private string _webCredentialsDomain;
        public string WebCredentialsDomain
        {
            get { return _webCredentialsDomain; }
            set { SetProperty(ref _webCredentialsDomain, value); }
        }

        private string _webCredentialsUsername;
        public string WebCredentialsUsername
        {
            get { return _webCredentialsUsername; }
            set { SetProperty(ref _webCredentialsUsername, value); }
        }

        private string _webCredentialsPassword;
        public string WebCredentialsPassword
        {
            get { return _webCredentialsPassword; }
            set { SetProperty(ref _webCredentialsPassword, value); }
        }

        private string _pinCodeCurrent;
        public string PinCodeCurrent
        {
            get { return _pinCodeCurrent; }
            set { SetProperty(ref _pinCodeCurrent, value); }
        }

        private string _pinCodeNew;
        public string PinCodeNew
        {
            get { return _pinCodeNew; }
            set { SetProperty(ref _pinCodeNew, value); }
        }

        private string _pinCodeNewNew;
        public string PinCodeNewNew
        {
            get { return _pinCodeNewNew; }
            set { SetProperty(ref _pinCodeNewNew, value); }
        }

        private Color _warningsColor = Color.FromHex(AppTheme.WarningsColorFailedDefault);
        public Color WarningsColor
        {
            get { return _warningsColor; }
            set { SetProperty(ref _warningsColor, value); }
        }

        private bool AllowPinCode;
        private bool IsPinCodeAvailable;
        private string SavedUsername;
        private string SavedPassword;
        private string SavedPinCode;

        public SettingsPageViewModel (IEventAggregator eventAggregator, INavigationService navigationService)
        {
            _eventAggregator = eventAggregator;
            _navigationService = navigationService;

            ShowMasterDetailCommand = new DelegateCommand(ShowMasterDetail);
            SavePropertiesCommand = new DelegateCommand(SaveProperties);
            ResetPropertiesCommand = new DelegateCommand(ResetProperties);
            SetPinCodeCommand = new DelegateCommand(SetPinCode);
            SavePinCodeCommand = new DelegateCommand(SavePinCode);
            DeletePinCodeCommand = new DelegateCommand(DeletePinCode);
            CancelPinCodeCommand = new DelegateCommand(CancelPinCode);
            CleanCacheCommand = new DelegateCommand(CleanCache);

            InitializeCredentials();
            InitializeApiCredentials();
            InitializeWebCredentials();
            InitializePinCode();
            InitializePinCodeUI();
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo (NavigationParameters parameters)
        {

        }

        private void InitializeCredentials ()
        {
            ServerURL = DataService.DBService.ServerURL;
            ApiKey = DataService.DBService.ApiKey;
            ApiAccess = DataService.DBService.ApiAccess;
            Username = DataService.DBService.Username;
            Password = DataService.DBService.Password;
            SiteCode = DataService.DBService.SiteCode;

            if (!string.IsNullOrEmpty(Password))
                AllowPinCode = true;
        }

        private void InitializeApiCredentials ()
        {
            DataService.ServerCredentials.TryInitialize(true);
            ApiUsername = DataService.ServerCredentials.Username;
            ApiPassword = DataService.ServerCredentials.Password;
        }

        private void InitializeWebCredentials ()
        {
            DataService.BrowserCredentials.TryInitialize(true);
            WebCredentialsDomain = DataService.BrowserCredentials.Domain;
            WebCredentialsUsername = DataService.BrowserCredentials.Username;
            WebCredentialsPassword = DataService.BrowserCredentials.Password;
        }

        private void InitializePinCode ()
        {
            DataService.UserCredentials.TryInitialize(true);
            IsPinCodeAvailable = DataService.UserCredentials.IsPinCodeValid;

            if (IsPinCodeAvailable)
            {
                SavedUsername = DataService.UserCredentials.Username;
                SavedPassword = DataService.UserCredentials.Password;
                SavedPinCode = DataService.UserCredentials.PinCode;
            }
        }

        private void InitializePinCodeUI ()
        {
            if (AllowPinCode)
            {
                IsSettingPinCode_A = true;
                IsSettingPinCode_B = true;
            }
        }

        private async void ShowWarnings (string message, bool success)
        {
            WarningsText = message;
            WarningsColor = (success) ? Color.FromHex(AppTheme.WarningsColorSuccessDefault) : Color.FromHex(AppTheme.WarningsColorFailedDefault);
            IsWarningsVisible = true;
            await Task.Delay(2500);
            IsWarningsVisible = false;
            WarningsText = string.Empty;
        }

        private void ShowMasterDetail ()
        {
            string pageName = "SettingsPage";
            _eventAggregator.GetEvent<Events.TogglePOMasterDetailPageEvent>().Publish(new Events.TogglePOMasterDetailPagePayload { DetailPageName = pageName, IsPresented = true });
        }

        private async void SaveProperties ()
        {
            if (string.IsNullOrEmpty(ServerURL))
            {
                ShowWarnings("Server URL field is empty.", false);
                return;
            }
            else
            {
                bool successIP = false;
                bool successScheme = false;
                bool successURI = false;
                Uri uri;
                Regex patternIPv4 = new Regex(@"\b((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\b");
                Regex patternIPv6 = new Regex(@"\b(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))\b");

                string matchIPv4 = patternIPv4.Match(ServerURL).Value;
                string matchIPv6 = patternIPv6.Match(ServerURL).Value;

                successIP = !string.IsNullOrEmpty(matchIPv4) || !string.IsNullOrEmpty(matchIPv6);
                successURI = Uri.TryCreate(ServerURL, UriKind.Absolute, out uri);

                if (successURI)
                    successScheme = uri.Scheme.Equals("http") || uri.Scheme.Equals("https");

                if (successScheme && successURI)
                    ServerURL = uri.AbsoluteUri;
                else
                {
                    ShowWarnings("Invalid Server URL.", false);
                    return;
                }
            }

            if (string.IsNullOrEmpty(ApiKey))
            {
                ShowWarnings("API Key field is empty.", false);
                return;
            }

            if (string.IsNullOrEmpty(ApiAccess))
            {
                ShowWarnings("API Access field is empty.", false);
                return;
            }

            if (string.IsNullOrEmpty(ApiUsername))
            {
                ShowWarnings("API Username field is empty.", false);
                return;
            }

            if (string.IsNullOrEmpty(ApiPassword))
            {
                ShowWarnings("API Password field is empty.", false);
                return;
            }

            if (!string.IsNullOrEmpty(WebCredentialsDomain))
            {
                if (!WebCredentialsDomain.EndsWith("\\"))
                {
                    ShowWarnings("Domain field must end with a '\\' or be empty.", false);
                    return;
                }

                if (string.IsNullOrEmpty(WebCredentialsUsername))
                {
                    ShowWarnings("Username field is empty.", false);
                    return;
                }

                if (string.IsNullOrEmpty(WebCredentialsUsername))
                {
                    ShowWarnings("Password field is empty.", false);
                    return;
                }

            }

            if (!string.IsNullOrEmpty(WebCredentialsUsername) && string.IsNullOrEmpty(WebCredentialsPassword))
            {
                ShowWarnings("Password field is empty.", false);
                return;
            }

            if (!string.IsNullOrEmpty(WebCredentialsPassword) && string.IsNullOrEmpty(WebCredentialsUsername))
            {
                ShowWarnings("Username field is empty.", false);
                return;
            }

            Tuple<bool, AppServerConfigImplementation> result = await DataService.ValidateServerConfig(ServerURL, ApiKey, ApiAccess);
            bool successAccessPoint = result.Item1;

            if (!successAccessPoint)
            {
                ShowWarnings("Failed to verify server.", false);
                return;
            }

            bool successServerConfig = await DataService.SaveServerConfig(new AppServerConfig(ServerURL, ApiKey, ApiAccess, result.Item2));
            bool successApiCredentials = await DataService.ServerCredentials.Save(ApiUsername, ApiPassword);
            bool successWebCredentials = await DataService.BrowserCredentials.Save(WebCredentialsDomain, WebCredentialsUsername, WebCredentialsPassword);

            if (successServerConfig && successAccessPoint && successApiCredentials)
                ShowWarnings("Settings saved.", true);
            else
                ShowWarnings("Failed to save.", false);

            if (DataService.IsServerConfigDirty())
            {
                await DataService.UserCredentials.Clear();
                await App.Current.MainPage.DisplayAlert("Server Configuration", "Server details has changed, logging out.", "Ok");
                DataService.LoadServerConfig();
                string pageName = "LoginPage";
                _navigationService.NavigateAsync(new Uri(DataService.AppURL + pageName, UriKind.Absolute));
            }
        }

        private async void ResetProperties ()
        {
            bool logout = false;

            if (DataService.IsServerConfigUndefault())
            {
                await DataService.ServerConfig.Clear();
                DataService.LoadServerConfig();
                logout = true;
            }

            bool successApiCredentials = await DataService.ServerCredentials.Clear();
            bool successWebCredentials = await DataService.BrowserCredentials.Clear();

            InitializeCredentials();
            InitializeApiCredentials();
            InitializeWebCredentials();

            if (successApiCredentials && successWebCredentials)
                ShowWarnings("Settings saved.", true);
            else
                ShowWarnings("Failed to save.", false);

            if (logout)
            {
                await DataService.UserCredentials.Clear();
                await App.Current.MainPage.DisplayAlert("Server Configuration", "Server details has changed, logging out.", "Ok");
                DataService.LoadServerConfig();
                string pageName = "LoginPage";
                _navigationService.NavigateAsync(new Uri(DataService.AppURL + pageName, UriKind.Absolute));
            }
        }

        private void SetPinCode ()
        {
            IsSettingPinCode_B = false;

            if (IsPinCodeAvailable)
            {
                IsSettingPinCode_C = true;
                IsSettingPinCode_E = true;
            }

            IsSettingPinCode_D = true;
            IsSettingPinCode_F = true;
        }

        private async void SavePinCode ()
        {
            if (IsPinCodeAvailable && SavedPinCode != PinCodeCurrent)
            {
                ShowWarnings("Incorrect Pin Code.", false);
                return;
            }

            if (string.IsNullOrEmpty(PinCodeNew) || string.IsNullOrEmpty(PinCodeNewNew))
            {
                ShowWarnings("Pin Code field is empty.", false);
                return;
            }

            if (PinCodeNew != PinCodeNewNew)
            {
                ShowWarnings("Pin Code does not match.", false);
                return;
            }

            if (PinCodeNew.Length < 4)
            {
                ShowWarnings("The minimum Pin Code length is 4.", false);
                return;
            }

            if (PinCodeNew.Length > 6)
            {
                ShowWarnings("The maximum Pin Code length is 6.", false);
                return;
            }

            int intpinCode = -1;
            if (!int.TryParse(PinCodeNew, out intpinCode) || intpinCode < 0)
            {
                ShowWarnings("Pin Code must only contain numeric characters.", false);
                return;
            }

            bool successCredentials = await DataService.UserCredentials.Save(Username, Password, PinCodeNew);

            InitializePinCode();

            CancelPinCode();

            if (successCredentials)
                ShowWarnings("Pin Code saved.", true);
            else
                ShowWarnings("Failed to save.", false);
        }

        private async void DeletePinCode ()
        {
            if (!IsPinCodeAvailable) return;

            if (string.IsNullOrEmpty(PinCodeCurrent))
            {
                ShowWarnings("You must input the current Pin Code.", false);
                return;
            }

            if (IsPinCodeAvailable && SavedPinCode != PinCodeCurrent)
            {
                ShowWarnings("Incorrect Pin Code.", false);
                return;
            }

            await DataService.UserCredentials.Clear();

            InitializePinCode();

            CancelPinCode();

            ShowWarnings("Pin Code deleted.", true);
        }

        private void CancelPinCode ()
        {
            PinCodeCurrent = string.Empty;
            PinCodeNew = string.Empty;
            PinCodeNewNew = string.Empty;

            IsSettingPinCode_B = true;
            IsSettingPinCode_C = false;
            IsSettingPinCode_D = false;
            IsSettingPinCode_E = false;
            IsSettingPinCode_F = false;
        }

        private async void CleanCache ()
        {
            if (!string.IsNullOrEmpty(DataService.SiteCode))
            {
                bool successIcon = await StorageService.CleanCacheSiteFolder(DataService.SiteCode, StorageService.IconFolderName);
                bool successAttachments = await StorageService.CleanCacheSiteFolder(DataService.SiteCode, StorageService.AttachmentsFolderName);

                if (successIcon && successAttachments)
                    ShowWarnings("Cache cleaned.", true);
                else
                    ShowWarnings("Failed to clean cache.", false);
            }
        }
    }
}
