﻿using Prism.Events;

namespace ProjectOrange.Events
{
    public class TogglePOMasterDetailPagePayload
    {
        public bool IsPresented { get; set; }
        public string DetailPageName { get; set; }
    }

    public class TogglePOMasterDetailPageEvent : PubSubEvent<TogglePOMasterDetailPagePayload> { }
}
