﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace ProjectOrange.iOS
{
    public class Application
    {
        // This is the main entry point of the application.
        static void Main (string[] args)
        {
#if DEBUG
            try { UIApplication.Main(args, null, "AppDelegate"); }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Write("\n" +
                                               "=============================================================================================================================" +
                                               "\n" +
                                               e.ToString() +
                                               "=============================================================================================================================");
            }
#else
            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            UIApplication.Main(args, null, "AppDelegate");
#endif
        }

        public static UIViewController GetActiveViewController ()
        {
            UIViewController viewController = UIApplication.SharedApplication.KeyWindow.RootViewController;

            while (viewController.PresentedViewController != null)
                viewController = viewController.PresentedViewController;

            return viewController;
        } 
    }
}
