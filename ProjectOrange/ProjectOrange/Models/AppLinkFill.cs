﻿namespace ProjectOrange.Models
{
    public class AppLinkFill
    {
        public string Title { get; set; }
        public string Value { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public AppLinkFill ()
        {
            Title = string.Empty;
            Value = string.Empty;
            Username = string.Empty;
            Password = string.Empty;
        }

        public string GetValue ()
        {
            return Value.Replace(@"{USERNAME}", Username).Replace(@"{PASSWORD}", Password);
        }

        public string GetFriendlyValue ()
        {
            return Value.Replace(@"{USERNAME}", Username).Replace(@"{PASSWORD}", "********");
        }
    }
}
