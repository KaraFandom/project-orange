﻿using ProjectOrange.Models;
using System.Threading.Tasks;

namespace ProjectOrange.Services.Interfaces
{
    public interface IFilePicker
    {
        Task<FilePickerData> PickFile ();
    }
}
