﻿using ProjectOrange.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectOrange.Models
{
    public enum AppServerConfigImplementation
    {
        Sharepoint,
        Sql
    }

    public class AppServerConfig
    {
        private const string SERVERURL_KEY = "AppServerConfig.ServerURL";
        private const string APIKEY_KEY = "AppServerConfig.ApiKey";
        private const string APIACCESS_KEY = "AppServerConfig.ApiAccess";
        private const string IMPLEMENTATION_KEY = "AppServerConfig.Implementation";

        //*
        public const string SERVERURL_DEFAULT = "http://roche.fandominc.com/api";
        public const string APIKEY_DEFAULT = "RocheApp";
        public const string APIACCESS_DEFAULT = "RocheApp2017";
        public const string IMPLEMENTATION_DEFAULT = "sql";
        //*/
        /*
        public const string SERVERURL_DEFAULT = "http://5.189.181.56/iShareService/iShareAPI.svc";
        public const string APIKEY_DEFAULT = "iShareWcfKey";
        public const string APIACCESS_DEFAULT = "iShareWcfAccess";
        public const string IMPLEMENTATION_DEFAULT = "sharepoint";
        //*/

        private bool _isInitialized = false;
        private string _serverURL = string.Empty;
        private string _apiKey = string.Empty;
        private string _apiAccess = string.Empty;
        private AppServerConfigImplementation _implementation = AppServerConfigImplementation.Sql;

        public bool IsInitialized { get { return _isInitialized; } }
        public string ServerURL { get { return _serverURL; } }
        public string ApiKey { get { return _apiKey; } }
        public string ApiAccess { get { return _apiAccess; } }
        public AppServerConfigImplementation Implementation { get { return _implementation; } }

        public AppServerConfig ()
        {
            _serverURL = SERVERURL_DEFAULT;
            _apiKey = APIKEY_DEFAULT;
            _apiAccess = APIACCESS_DEFAULT;
            _implementation = ImplementationFromString(IMPLEMENTATION_DEFAULT);

        }
        public AppServerConfig (string serverURL, string apiKey, string apiAccess, AppServerConfigImplementation implementation)
        {
            _serverURL = serverURL;
            _apiKey = apiKey;
            _apiAccess = apiAccess;
            _implementation = implementation;
        }

        public void TryInitialize (bool reloadAlways = false)
        {
            if (reloadAlways || !IsInitialized)
                Load();
        }

        public void Load ()
        {
            string serverURL = (string) DataService.LoadAppProperty(SERVERURL_KEY, string.Empty);
            string apiKey = (string) DataService.LoadAppProperty(APIKEY_KEY, string.Empty);
            string apiAccess = (string) DataService.LoadAppProperty(APIACCESS_KEY, string.Empty);
            AppServerConfigImplementation implementation = ImplementationFromString((string) DataService.LoadAppProperty(IMPLEMENTATION_KEY, IMPLEMENTATION_DEFAULT));

            if (!serverURL.IsNullOrEmpty() && !apiKey.IsNullOrEmpty() && !apiAccess.IsNullOrEmpty())
            {
                _serverURL = serverURL;
                _apiKey = apiKey;
                _apiAccess = apiAccess;
                _implementation = implementation;

                _isInitialized = true;
            }
            else
            {
                _serverURL = SERVERURL_DEFAULT;
                _apiKey = APIKEY_DEFAULT;
                _apiAccess = APIACCESS_DEFAULT;
                _implementation = ImplementationFromString(IMPLEMENTATION_DEFAULT);

                _isInitialized = false;
            }
        }

        public async Task<bool> Clear ()
        {
            try
            {
                List<Tuple<string, object>> properties = new List<Tuple<string, object>>();
                properties.Add(new Tuple<string, object>(SERVERURL_KEY, SERVERURL_DEFAULT));
                properties.Add(new Tuple<string, object>(APIKEY_KEY, APIKEY_DEFAULT));
                properties.Add(new Tuple<string, object>(APIACCESS_KEY, APIACCESS_DEFAULT));
                properties.Add(new Tuple<string, object>(IMPLEMENTATION_KEY, IMPLEMENTATION_DEFAULT));

                await DataService.SaveAppProperty(properties.ToArray());

                _isInitialized = false;
                _serverURL = SERVERURL_DEFAULT;
                _apiKey = APIKEY_DEFAULT;
                _apiAccess = APIACCESS_DEFAULT;
                _implementation = ImplementationFromString(IMPLEMENTATION_DEFAULT);

                return true;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<bool> Save (string serverURL, string apiKey, string apiAccess, AppServerConfigImplementation implementation)
        {
            if (string.IsNullOrEmpty(serverURL) || string.IsNullOrEmpty(apiKey) || string.IsNullOrEmpty(apiAccess))
                return false;

            try
            {
                List<Tuple<string, object>> properties = new List<Tuple<string, object>>();
                properties.Add(new Tuple<string, object>(SERVERURL_KEY, serverURL));
                properties.Add(new Tuple<string, object>(APIKEY_KEY, apiKey));
                properties.Add(new Tuple<string, object>(APIACCESS_KEY, apiAccess));
                properties.Add(new Tuple<string, object>(IMPLEMENTATION_KEY, ImplementationToString(implementation)));

                await DataService.SaveAppProperty(properties.ToArray());
                Load();

                return true;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
                return false;
            }
        }

        public static AppServerConfigImplementation ImplementationFromString (string value)
        {
            switch (value.ToLower())
            {
                case "sharepoint":
                    return AppServerConfigImplementation.Sharepoint;

                default:
                case "sql":
                    return AppServerConfigImplementation.Sql;
            }
        }

        public static string ImplementationToString (AppServerConfigImplementation value)
        {
            switch (value)
            {
                case AppServerConfigImplementation.Sharepoint:
                    return "sharepoint";

                default:
                case AppServerConfigImplementation.Sql:
                    return "sql";
            }
        }
    }
}
