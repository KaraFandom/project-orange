using Android.Content;
using Android.OS;
using ProjectOrange.Droid.DependencyServices;
using ProjectOrange.Services;
using ProjectOrange.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(MailService))]
namespace ProjectOrange.Droid.DependencyServices
{
    public class MailService : IMailService
    {
        public async void Compose(string[] to, string[] cc, string[] bcc, string subject = null, string body = null, bool isHtml = false, string[] attachments = null, Action<bool> completed = null)
        {
            Intent email = new Intent(Intent.ActionSend);
            if (attachments != null && attachments.Length > 0) email = new Intent(Intent.ActionSendMultiple);

            // Set Type
            email.SetType("message/rfc822");

            // Set To Recipients
            if (to != null && to.Length > 0) email.PutExtra(Intent.ExtraEmail, to);

            // Set CC Recipients
            if (cc != null && cc.Length > 0) email.PutExtra(Intent.ExtraCc, cc);

            // Set BCC Recipients
            if (bcc != null && bcc.Length > 0) email.PutExtra(Intent.ExtraBcc, bcc);

            // Set Subject
            if (!string.IsNullOrEmpty(subject)) email.PutExtra(Intent.ExtraSubject, subject);

            // Set Body
            if (!string.IsNullOrEmpty(body))
            {
                if (isHtml) email.PutExtra(Intent.ExtraText, Android.Text.Html.FromHtml(body));
                else email.PutExtra(Intent.ExtraText, body);
            }

            // Set Attachements
            if (attachments != null && attachments.Length > 0)
            {
                string filePath = string.Empty;
                byte[] file = null;
                Java.IO.File jfile = null;

                List<IParcelable> uris = new List<IParcelable>();

                foreach (string attachment in attachments)
                {
                    //filePath = Path.Combine(Forms.Context.ExternalCacheDir.AbsolutePath, attachment);
                    filePath = StorageService.GetAttachmentPath(DataService.SiteCode, attachment);

                    file = await StorageService.LoadFromCache(attachment, DataService.SiteCode, StorageService.AttachmentsFolderName);

                    if (file != null)
                    {
                        File.WriteAllBytes(filePath, file);

                        jfile = new Java.IO.File(filePath);
                        jfile.SetReadable(true);

                        if (jfile.CanWrite())
                            uris.Add(Android.Net.Uri.FromFile(jfile));
                    }
                }

                if (uris != null && uris.Count > 0)
                    email.PutParcelableArrayListExtra(Intent.ExtraStream, uris);
            }

            Forms.Context.StartActivity(email);

            if (completed != null) completed(true);
        }
    }
}