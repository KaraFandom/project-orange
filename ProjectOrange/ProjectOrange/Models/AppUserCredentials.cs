﻿using ProjectOrange.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectOrange.Models
{
    public class AppUserCredentials
    {
        private const string USERNAME_KEY = "AppUserCredentials.Username";
        private const string PASSWORD_KEY = "AppUserCredentials.Password";
        private const string PINCODE_KEY = "AppUserCredentials.PinCode";

        private bool _isInitialized = false;
        private string _username = string.Empty;
        private string _password = string.Empty;
        private string _pinCode = string.Empty;

        public bool IsInitialized { get { return _isInitialized; } }
        public string Username { get { return _username; } }
        public string Password { get { return _password; } }
        public string PinCode { get { return _pinCode; } }

        public bool HasUsername { get { return !string.IsNullOrEmpty(Username); } }
        public bool HasPassword { get { return !string.IsNullOrEmpty(Password); } }
        public bool HasPinCode { get { return !string.IsNullOrEmpty(PinCode); } }
        public bool IsPinCodeValid { get { return (HasUsername && HasPassword && HasPinCode); } }

        public AppUserCredentials () { }
        public AppUserCredentials (string username, string password, string pinCode)
        {
            _username = username;
            _password = password;
            _pinCode = pinCode;
        }

        public void TryInitialize (bool reloadAlways = false)
        {
            if (reloadAlways || !IsInitialized)
                Load();
        }

        public void Load ()
        {
            string username = (string) DataService.LoadAppProperty(USERNAME_KEY, string.Empty);
            string password = (string) DataService.LoadAppProperty(PASSWORD_KEY, string.Empty);
            string pinCode = (string) DataService.LoadAppProperty(PINCODE_KEY, string.Empty);

            if (!username.IsNullOrEmpty() && !password.IsNullOrEmpty() && !pinCode.IsNullOrEmpty())
            {
                _username = username;
                _password = password;
                _pinCode = pinCode;

                _isInitialized = true;
            }
            else
            {
                _username = string.Empty;
                _password = string.Empty;
                _pinCode = string.Empty;

                _isInitialized = false;
            }
        }

        public async Task<bool> Clear ()
        {
            try
            {
                List<Tuple<string, object>> properties = new List<Tuple<string, object>>();
                properties.Add(new Tuple<string, object>(USERNAME_KEY, string.Empty));
                properties.Add(new Tuple<string, object>(PASSWORD_KEY, string.Empty));
                properties.Add(new Tuple<string, object>(PINCODE_KEY, string.Empty));

                await DataService.SaveAppProperty(properties.ToArray());

                _isInitialized = false;
                _username = string.Empty;
                _password = string.Empty;
                _pinCode = string.Empty;

                return true;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<bool> Save (string username, string password, string pinCode)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(pinCode))
                return false;

            try
            {
                List<Tuple<string, object>> properties = new List<Tuple<string, object>>();
                properties.Add(new Tuple<string, object>(USERNAME_KEY, username));
                properties.Add(new Tuple<string, object>(PASSWORD_KEY, password));
                properties.Add(new Tuple<string, object>(PINCODE_KEY, pinCode));

                await DataService.SaveAppProperty(properties.ToArray());
                Load();

                return true;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
