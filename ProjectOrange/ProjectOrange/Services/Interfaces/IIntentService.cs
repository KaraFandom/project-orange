﻿namespace ProjectOrange.Services.Interfaces
{
    public interface IIntentService
    {
        void Open (string fileName);
    }
}
