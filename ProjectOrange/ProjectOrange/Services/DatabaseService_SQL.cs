﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectOrange.Models;
using ProjectOrange.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectOrange.Services
{
    public class DatabaseService_SQL : IDatabaseService
    {
        private const string NOT_AUTHENTICATED = "Session Expired. Please login and try again.";

        #region Fields
        private bool _isVerified = true;
        private bool _isAuthenticated = false;
        private string _serverURL = "http://roche.fandominc.com/api";
        private string _apiKey = "RocheApp";
        private string _apiAccess = "RocheApp2017";
        private string _username = string.Empty;
        private string _password = string.Empty;
        private string _storedPassword = string.Empty;
        private string _siteCode = string.Empty;
        private string _accessToken = string.Empty;
        #endregion

        #region Properties
        public bool IsVerified
        {
            get { return _isVerified; }
        }

        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
            private set { _isAuthenticated = value; }
        }

        public string ServerURL
        {
            get { return _serverURL; }
        }

        public string ApiKey
        {
            get { return _apiKey; }
        }

        public string ApiAccess
        {
            get { return _apiAccess; }
        }

        public string Username
        {
            get { return _username; }
            private set { _username = value; }
        }

        public string Password
        {
            get { return _password; }
            private set { _password = value; }
        }

        public string StoredPassword
        {
            get { return _storedPassword; }
            private set { _storedPassword = value; }
        }

        public string SiteCode
        {
            get { return _siteCode; }
            private set { _siteCode = value; }
        }

        public string AccessToken
        {
            get { return _accessToken; }
            private set { _accessToken = value; }
        }

        public AppServerConfigImplementation Implementation
        {
            get { return AppServerConfigImplementation.Sql; }
        }
        #endregion

        #region Constructor
        public DatabaseService_SQL (string serverURL, string apiKey, string apiAccess)
        {
            _serverURL = serverURL;
            _apiKey = apiKey;
            _apiAccess = apiAccess;
            _isVerified = false;
        }

        public DatabaseService_SQL () { }
        #endregion

        #region Methods
        public async Task<bool> Validate ()
        {
            return await Validate(ServerURL, ApiKey, ApiAccess);
        }

        public async Task<AppUser> Authenticate (string username, string password, string siteCode = "")
        {
            if (!IsVerified)
            {
                _isVerified = await Validate();
            }

            if (!IsVerified)
                return new AppUser { ErrorMessage = "Failed to verify server." };

            AppUser user = new AppUser();

            if (string.IsNullOrEmpty(username))
                return new AppUser { ErrorMessage = "Username is null or empty." };

            if (string.IsNullOrEmpty(password))
                return new AppUser { ErrorMessage = "Password is null or empty." };

            string requestUri = string.Format("{0}/auth/access-token?appname={1}&accesskey={2}&username={3}&password={4}",
                                              ServerURL, ApiKey, ApiAccess, username, password);

            string json = await DataService.GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                user = JsonConvert.DeserializeObject<AppUser>(json);

                if (!string.IsNullOrEmpty(user.ErrorMessage))
                    return new AppUser { ErrorMessage = user.ErrorMessage };

                user.UserName = user.apiAuthSessionsUsername;

                IsAuthenticated = true;
                Username = user.UserName;
                Password = password;
                StoredPassword = password;
                SiteCode = user.SiteCode;
                AccessToken = user.apiAuthSessionsToken;
            }

            return user;
        }

        public async Task<AppTheme> GetTheme ()
        {
            AppTheme theme = new AppTheme();

            if (!IsAuthenticated)
                return new AppTheme { ErrorMessage = NOT_AUTHENTICATED };

            string requestUri = string.Format("{0}/v1/theme?sc={1}&username={2}&token={3}",
                                               ServerURL, SiteCode, Username, AccessToken);

            string json = await DataService.GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                theme = JsonConvert.DeserializeObject<AppTheme>(json);

                if (!string.IsNullOrEmpty(theme.ErrorMessage))
                    return new AppTheme { ErrorMessage = theme.ErrorMessage };
            }

            return theme;
        }

        public async Task<List<AppCategory>> GetCategories ()
        {
            List<AppCategory> categories = new List<AppCategory>();

            if (!IsAuthenticated)
                return new List<AppCategory>();

            string requestUri = string.Format("{0}/v1/categories?sc={1}&username={2}&token={3}",
                                               ServerURL, SiteCode, Username, AccessToken);

            string json = await DataService.GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                if (json == "[]") return new List<AppCategory>();

                categories = JsonConvert.DeserializeObject<List<AppCategory>>(json);
                categories = categories.Where(x => string.IsNullOrEmpty(x.ErrorMessage)).ToList();

                if (categories == null)
                    return new List<AppCategory>();
            }

            return categories;
        }

        public async Task<List<AppArticle>> GetArticles (string categoryId)
        {
            List<AppArticle> articles = new List<AppArticle>();

            if (!IsAuthenticated)
                return new List<AppArticle>();

            if (string.IsNullOrEmpty(categoryId))
                return new List<AppArticle>();

            string requestUri = string.Format("{0}/v1/article?sc={1}&cid={2}&username={3}&token={4}",
                                              ServerURL, SiteCode, categoryId, Username, AccessToken);

            string json = await DataService.GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                if (json == "[]") return new List<AppArticle>();

                articles = JsonConvert.DeserializeObject<List<AppArticle>>(json);
                articles = articles.Where(x => string.IsNullOrEmpty(x.ErrorMessage)).ToList();

                if (articles == null)
                    return new List<AppArticle>();
            }

            return articles;
        }

        public async Task<List<AppCategory>> GetSubCategories (string categoryId)
        {
            List<AppCategory> subcategories = new List<AppCategory>();

            if (!IsAuthenticated)
                return new List<AppCategory>();

            if (string.IsNullOrEmpty(categoryId))
                return new List<AppCategory>();

            string requestUri = string.Format("{0}/v1/subcategories?sc={1}&cid={2}&username={3}&token={4}",
                                              ServerURL, SiteCode, categoryId, Username, AccessToken);

            string json = await DataService.GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                if (json == "[]") return new List<AppCategory>();

                subcategories = JsonConvert.DeserializeObject<List<AppCategory>>(json);
                subcategories = subcategories.Where(x => string.IsNullOrEmpty(x.ErrorMessage)).ToList();

                if (subcategories == null)
                    return new List<AppCategory>();
            }

            return subcategories;
        }

        public async Task<AppFileContent> GetFileContent (string requestUri, string fileName, string modifiedDate)
        {
            AppFileContent fileContent = new AppFileContent();

            if (string.IsNullOrEmpty(requestUri))
                return null;

            if (string.IsNullOrEmpty(fileName))
                return null;

            byte[] contents = await DataService.GetBytes(requestUri);

            if (contents != null || contents.Length > 0)
            {
                fileContent.FileName = fileName;
                fileContent.ModifiedDate = modifiedDate;
                fileContent.Contents = contents.ToList();
            }
            else
                return new AppFileContent { ErrorMessage = "Failed to download content." };

            return fileContent;
        }

        public async Task<AppPostResponse> PostTextArticle (string categoryId, string fileType, string title, string value, int sortOrder = 10)
        {
            AppPostResponse postResponse = new AppPostResponse();

            if (!IsAuthenticated)
                return new AppPostResponse { ErrorMessage = NOT_AUTHENTICATED };

            if (string.IsNullOrEmpty(categoryId))
                return new AppPostResponse { ErrorMessage = "Category ID is null or empty." };

            if (string.IsNullOrEmpty(fileType))
                return new AppPostResponse { ErrorMessage = "File Type is null or empty." };

            if (string.IsNullOrEmpty(title))
                return new AppPostResponse { ErrorMessage = "Title is null or empty." };

            if (string.IsNullOrEmpty(value))
                return new AppPostResponse { ErrorMessage = "Value is null or empty." };

            JObject jsonObject = new JObject(new JProperty("categoryid", categoryId),
                                             new JProperty("title", title),
                                             new JProperty("value", value),
                                             new JProperty("sitecode", SiteCode),
                                             new JProperty("sortorder", sortOrder),
                                             new JProperty("user", "mobileappadmin"));

            string requestUri = string.Format("{0}/v1/text-article?username={1}&token={2}",
                                              ServerURL, Username, AccessToken);

            string json = await DataService.PostJson(requestUri, jsonObject);

            if (string.IsNullOrEmpty(json))
                return new AppPostResponse { ErrorMessage = "No response." };

            postResponse = JsonConvert.DeserializeObject<AppPostResponse>(json);

            return postResponse;
        }

        public async Task<AppPostResponse> PostFileArticle (string categoryId, string fileType, string title, string fileName, byte[] fileData, int sortOrder = 10)
        {
            AppPostResponse postResponse = new AppPostResponse();

            if (!IsAuthenticated)
                return new AppPostResponse { ErrorMessage = NOT_AUTHENTICATED };

            if (string.IsNullOrEmpty(categoryId))
                return new AppPostResponse { ErrorMessage = "Category ID is null or empty." };

            if (string.IsNullOrEmpty(fileType))
                return new AppPostResponse { ErrorMessage = "File Type is null or empty." };

            if (string.IsNullOrEmpty(title))
                return new AppPostResponse { ErrorMessage = "Title is null or empty." };

            if (string.IsNullOrEmpty(fileName))
                return new AppPostResponse { ErrorMessage = "File Name is null or empty." };

            if (fileData == null || fileData.Length == 0)
                return new AppPostResponse { ErrorMessage = "File Data is null or empty." };
            
            JObject jsonObject = new JObject(new JProperty("categoryid", categoryId),
                                             new JProperty("title", title),
                                             new JProperty("filename", fileName),
                                             new JProperty("sitecode", SiteCode),
                                             new JProperty("sortorder", sortOrder),
                                             new JProperty("user", "mobileappadmin"));

            string requestUri = string.Format("{0}/v1/file-article?username={1}&token={2}",
                                              ServerURL, Username, AccessToken);

            string json = await DataService.PostJsonByte(requestUri, jsonObject, fileName, fileData);

            if (string.IsNullOrEmpty(json))
                return new AppPostResponse { ErrorMessage = "No response." };

            postResponse = JsonConvert.DeserializeObject<AppPostResponse>(json);

            return postResponse;
        }
        #endregion

        #region Static Methods
        public static async Task<bool> Validate (string serverURL, string apiKey, string apiAccess)
        {
            if (serverURL.IsNullOrEmpty() || apiKey.IsNullOrEmpty() || apiAccess.IsNullOrEmpty())
                return false;

            string param = string.Format("?appname={0}&accesskey{1}", apiKey, apiAccess);
            string requestUri = string.Format("{0}/v1/connect{1}", serverURL, param);

            if (string.IsNullOrEmpty(requestUri))
                return false;

            string json = await DataService.GetJson(requestUri, null, true);

            if (!json.IsNullOrEmpty())
            {
                JObject jObject = JObject.Parse(json);

                if (jObject != null)
                {
                    JToken jIsActive = null;
                    JToken jApiType = null;

                    if (jObject.TryGetValue("IsActive", out jIsActive) && jObject.TryGetValue("ApiType", out jApiType))
                    {
                        string isActive = jIsActive.Value<string>();
                        string apiType = jApiType.Value<string>();

                        if (isActive.ToLower() == "true" && apiType.ToLower() == "sql")
                            return true;
                    }
                }
            }

            return false;
        }
        #endregion
    }
}
