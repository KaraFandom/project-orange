﻿using ProjectOrange.Services;
using ProjectOrange.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectOrange.Models
{
    public class AppFileContent
    {
        public string ErrorMessage { get; set; }
        public string Id { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public List<byte> Contents { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }

        public AppFileContent ()
        {
            ErrorMessage = string.Empty;
            Id = string.Empty;
            ModifiedBy = string.Empty;
            ModifiedDate = string.Empty;
            Contents = new List<byte>();
            FileName = string.Empty;
            FileType = string.Empty;
        }

        public async Task<bool> WriteToFile (DateTime lastWriteTime, string folderName = "", string subFolderName = "", bool decompress = false)
        {
            if (!string.IsNullOrEmpty(FileName) && Contents != null && Contents.Count > 0)
            {
                byte[] contents = Contents.ToArray();

                if (decompress)
                {
                    try
                    {
                        IFileService fileService = DependencyService.Get<IFileService>();

                        DebugService.WriteLine("Data Array Pre-Decompression Length: " + (contents.Length * 0.001) + " KB");

                        if (fileService != null)
                            contents = fileService.Decompress(contents);

                        DebugService.WriteLine("Data Array Post-Decompression Length: " + (contents.Length * 0.001) + " KB");
                    }
                    catch (Exception ex)
                    {
                        contents = Contents.ToArray();
                    }
                }

                bool success = await StorageService.SaveToCache(contents, lastWriteTime, FileName, folderName, subFolderName);

                if (success)
                    Contents = null;

                return success;
            }
            else
            {
                return false;
            }
        }
    }
}
