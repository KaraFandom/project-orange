﻿using ProjectOrange.Services.Interfaces;
using Xamarin.Forms;

namespace ProjectOrange.CustomViews
{
    public delegate bool ShouldTrustCertificate (IAuthenticatingWebViewCertificate certificate);

    public class AuthenticatingWebView : WebView
    {
        public ShouldTrustCertificate ShouldTrustUnknownCertificate { get; set; }
    }
}
