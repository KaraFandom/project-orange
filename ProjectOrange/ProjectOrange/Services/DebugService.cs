﻿using System.Diagnostics;

namespace ProjectOrange.Services
{
    public static class DebugService
    {
        public static bool Enabled = true;

        public static void WriteLine(string message)
        {
            if (!Enabled)
                return;

            Debug.WriteLine(message);
        }
    }
}
