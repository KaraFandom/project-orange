﻿using System;
using Xamarin.Forms;

namespace ProjectOrange.CustomViews
{
    public class RoundedFrame : Frame
    {
        public Action OnUpdateCornerRadius;

        private float _cornerRadius;
        public float CornerRadius
        {
            get { return _cornerRadius; }
            set { _cornerRadius = value; }
        }

        public RoundedFrame () : base ()
        {
            CornerRadius = 0.0f;

            BackgroundColor = Color.Black;
            Padding = new Thickness(10.0);
        }
    }
}
