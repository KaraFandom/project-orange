﻿using ProjectOrange.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectOrange.Services.Interfaces
{
    public interface IDatabaseService
    {
        #region Properties
        bool IsVerified { get; }
        bool IsAuthenticated { get; }
        string ServerURL { get; }
        string ApiKey { get; }
        string ApiAccess { get; }
        string Username { get; }
        string Password { get; }
        string StoredPassword { get; }
        string SiteCode { get; }
        AppServerConfigImplementation Implementation { get; }
        #endregion

        #region Methods
        Task<bool> Validate ();
        Task<AppUser> Authenticate (string username, string password, string siteCode = "");
        Task<AppTheme> GetTheme ();
        Task<List<AppCategory>> GetCategories ();
        Task<List<AppArticle>> GetArticles (string categoryId);
        Task<List<AppCategory>> GetSubCategories (string categoryId);
        Task<AppFileContent> GetFileContent (string requestUri, string fileName, string modifiedDate);
        Task<AppPostResponse> PostTextArticle (string categoryId, string fileType, string title, string value, int sortOrder = 10);
        Task<AppPostResponse> PostFileArticle (string categoryId, string fileType, string title, string fileName, byte[] fileData, int sortOrder = 10);
        #endregion
    }
}
