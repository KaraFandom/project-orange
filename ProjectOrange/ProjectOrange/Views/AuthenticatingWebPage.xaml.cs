﻿using ProjectOrange.CustomControls;
using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class AuthenticatingWebPage : ContentPage
    {
        private Label labelWarnings;

        public AuthenticatingWebPage ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Initialization
            string warningsTextDefault = "Loading...";
            #endregion

            #region Content
            StackLayout main = new StackLayout ()
            {
                BackgroundColor = Color.Black,
                Spacing = 0
            };

            PONavBar navBar = new PONavBar ()
            {
                UseBanner = true,
                UseButtonRight = true
            };
            navBar.Redraw();
            navBar.ButtonLeft.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            navBar.ButtonRight.SetBinding(Button.CommandProperty, "MailCommand");
            main.Children.Add(navBar);

            labelWarnings = new Label ()
            {
                IsVisible = false,

                TextColor = Color.White,
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HeightRequest = 30,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            main.Children.Add(labelWarnings);

            AuthenticatingWebView authenticatingWebView = new AuthenticatingWebView ()
            {
                ShouldTrustUnknownCertificate = cert => true,

                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            authenticatingWebView.Navigating += (s, e) =>
            {
                ShowWarnings(warningsTextDefault, true, 0);
            };
            authenticatingWebView.Navigated += (s, e) =>
            {
                switch (e.Result)
                {
                    case WebNavigationResult.Success:
                        labelWarnings.IsVisible = false;
                        break;

                    case WebNavigationResult.Cancel:
                        ShowWarnings("Cancelled.", false);
                        break;

                    case WebNavigationResult.Timeout:
                        ShowWarnings("Timed out.", false);
                        break;

                    default:
                    case WebNavigationResult.Failure:
                        ShowWarnings("Failed to load.", false);
                        break;
                }
            };
            authenticatingWebView.SetBinding(WebView.SourceProperty, "UrlWebViewSource");
            main.Children.Add(authenticatingWebView);
            #endregion

            Content = main;
        }

        private async void ShowWarnings (string message, bool success, int delay = 2500)
        {
            labelWarnings.Text = message;
            labelWarnings.BackgroundColor = (success) ? Color.FromHex("#333333") : Color.FromHex(AppTheme.WarningsColorFailedDefault);
            labelWarnings.IsVisible = true;

            if (delay > 0)
            {
                await Task.Delay(delay);
                labelWarnings.IsVisible = false;
                labelWarnings.Text = string.Empty;
            }
        }
    }
}
