﻿namespace ProjectOrange.Models
{
    public class AppLink
    {
        public string Title { get; set; }
        public string Value { get; set; }

        public AppLink ()
        {
            Title = string.Empty;
            Value = string.Empty;
        }
    }
}
