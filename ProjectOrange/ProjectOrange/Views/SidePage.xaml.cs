﻿using ProjectOrange.CustomControls;
using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using ProjectOrange.Services;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class SidePage : ContentPage
    {
        public SidePage ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Initialization
            string imagePathBackground = AppTheme.BackgroundDefault;
            string imagePathHamburger = AppTheme.HamburgerDefault;
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);

            if (DataService.Theme != null)
            {
                if (DataService.Theme.BackgroundImage != null)
                    imagePathBackground = DataService.Theme.BackgroundImage.GetFileContentPath();

                if (DataService.Theme.IconHamburgerImage != null)
                    imagePathHamburger = DataService.Theme.IconHamburgerImage.GetFileContentPath();

                textColor = DataService.Theme.TextColorValue;
            }

            RelativeLayout main = new RelativeLayout ()
            {

            };

            Image background = new Image ()
            {
                Source = imagePathBackground,
                Aspect = Aspect.AspectFill
            };
            main.Children.Add(background,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0
            };
            main.Children.Add(content,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));

            PONavBar navBar = new PONavBar ()
            {
                UseBanner = true,
                UseButtonRight = false,
                ImagePathButtonLeft = imagePathHamburger
            };
            navBar.Redraw();
            navBar.ButtonLeft.SetBinding(Button.CommandProperty, "ShowMasterDetailCommand");
            content.Children.Add(navBar);

            ScrollView scrollContent = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            content.Children.Add(scrollContent);

            StackLayout scrollBody = new StackLayout ()
            {
                Padding = new Thickness(15, 15)
            };
            scrollContent.Content = scrollBody;

            Label labelTitle = new Label ()
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),

                TextColor = textColor
            };
            labelTitle.SetBinding(Label.TextProperty, "Title");
            scrollBody.Children.Add(labelTitle);

            switch (Device.OS)
            {
                case TargetPlatform.Android:

                    HtmlSelectableLabelDroid hslDroid = new HtmlSelectableLabelDroid ()
                    {
                        FontSize = Device.GetNamedSize(NamedSize.Small, (typeof(Label))),

                        TextColor = textColor
                    };
                    hslDroid.SetBinding(HtmlSelectableLabelDroid.TextProperty, "Body");
                    scrollBody.Children.Add(hslDroid);

                    break;

                case TargetPlatform.iOS:

                    HtmlSelectableLabeliOS hsliOS = new HtmlSelectableLabeliOS ()
                    {
                        BackgroundColor = Color.Transparent,
                        FontSize = Device.GetNamedSize(NamedSize.Small, (typeof(Label))),

                        TextColor = textColor
                    };
                    hsliOS.SetBinding(HtmlSelectableLabeliOS.SourceProperty, "Body");
                    scrollBody.Children.Add(hsliOS);

                    break;
            }
            #endregion

            Content = main;
        }
    }
}
