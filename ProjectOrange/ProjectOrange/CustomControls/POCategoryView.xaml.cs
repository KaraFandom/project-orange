﻿using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using ProjectOrange.Services;
using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class POCategoryView : ContentView
    {
        public static readonly BindableProperty CategoryProperty = BindableProperty.Create(nameof(Category),
                                                                                           typeof(AppCategory),
                                                                                           typeof(POCategoryView),
                                                                                           new AppCategory(),
                                                                                           BindingMode.TwoWay,
                                                                                           null,
                                                                                           (bindable, oldValue, newValue) =>
                                                                                           {
                                                                                               POCategoryView categoryView = (POCategoryView) bindable;
                                                                                               categoryView.Category = newValue as AppCategory;
                                                                                               categoryView.Redraw();
                                                                                           });

        private string _filterQuery = string.Empty;
        public string FilterQuery
        {
            get { return _filterQuery; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _filterQuery = string.Empty;
                else
                    _filterQuery = value;

                Redraw();
            }
        }

        private ScrollView _scrollView = new ScrollView();
        public ScrollView ScrollView
        {
            get { return _scrollView; }
        }

        public AppCategory Category
        {
            get { return (AppCategory) GetValue(CategoryProperty); }
            set { SetValue(CategoryProperty, value); }
        }

        public POCategoryView ()
        {
            InitializeComponent();

            Redraw();
        }

        public void Redraw ()
        {
            if (Category == null)
                return;

            #region Initialization
            string imagePathArrowBack = AppTheme.ArrowBackDefault;
            string imagePathArrowCollapse = AppTheme.ArrowCollapseDefault;
            string imagePathArrowExpand = AppTheme.ArrowExpandDefault;
            string imagePathArrowNext = AppTheme.ArrowNextDefault;
            Color separatorColor = Color.Transparent;
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);

            if (DataService.Theme != null)
            {
                if (DataService.Theme.IconArrowBackImage != null)
                    imagePathArrowBack = DataService.Theme.IconArrowBackImage.GetFileContentPath();

                if (DataService.Theme.IconArrowCollapseImage != null)
                    imagePathArrowCollapse = DataService.Theme.IconArrowCollapseImage.GetFileContentPath();

                if (DataService.Theme.IconArrowExpandImage != null)
                    imagePathArrowExpand = DataService.Theme.IconArrowExpandImage.GetFileContentPath();

                if (DataService.Theme.IconArrowNextImage != null)
                    imagePathArrowNext = DataService.Theme.IconArrowNextImage.GetFileContentPath();

                textColor = DataService.Theme.TextColorValue;
            }

            bool openTextByDefault = true;
            bool allowUpload = Category.AllowUploading();
            double contentTitleFontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Small, NamedSize.Default, NamedSize.Default), typeof(Label));
            double elementTitleFontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Micro, NamedSize.Small, NamedSize.Small), typeof(Label));
            string categoryIconPath = Category.GetIconFileContentPath();
            Color elementTitleColor = Color.Black;
            Color elementBackgroundColor = Color.White;
            Color elementGrayBackgroundColor = new Color(0.925, 0.925, 0.925);
            TextAlignment titleHAlign = TextAlignment.Start;
            TextAlignment titleVAlign = TextAlignment.Center;

            ScrollView main = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0
            };

            #region Content.Header
            Grid gridHeader = CreateHeaderGrid();
            content.Children.Add(gridHeader);

            Image imageHeaderBack = new Image ()
            {
                Source = imagePathArrowBack
            };
            gridHeader.Children.Add(imageHeaderBack, 1, 0);

            Label labelHeaderTitle = new Label ()
            {
                Text = Category.Name,
                TextColor = textColor,

                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            gridHeader.Children.Add(labelHeaderTitle, 2, 0);

            Image imageHeaderIcon = new Image ()
            {
                Source = categoryIconPath
            };
            gridHeader.Children.Add(imageHeaderIcon, 3, 0);

            Button buttonHeaderBack = new Button ()
            {
                BackgroundColor = Color.Transparent
            };
            buttonHeaderBack.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            gridHeader.Children.Add(buttonHeaderBack, 0, 0);
            Grid.SetColumnSpan(buttonHeaderBack, 5);
            #endregion

            #region Content.Articles
            if (Category.Articles != null && Category.Articles.Count > 0)
            {
                string articleTitle = string.Empty;
                string articleFileName = string.Empty;
                AppArticleType articleType = AppArticleType.File;

                foreach (AppArticle article in Category.Articles)
                {
                    if (article == null)
                        continue;

                    if (!article.Title.ToLower().Contains(FilterQuery))
                        continue;

                    articleTitle = article.Title;
                    articleFileName = article.GetFriendlyFileName();
                    articleType = article.GetArticleType();

                    content.Children.Add(CreateSeparator(separatorColor));

                    if (articleType.Equals(AppArticleType.Text) || articleType.Equals(AppArticleType.DirectText))
                    {
                        string text = (articleType.Equals(AppArticleType.DirectText)) ? article.Value : article.TextValue;

                        Grid gridContent = CreateContentGrid(Color.Transparent);
                        content.Children.Add(gridContent);

                        Label labelContentTitle = new Label ()
                        {
                            Text = articleTitle,

                            FontSize = contentTitleFontSize,
                            TextColor = textColor,
                            HorizontalTextAlignment = titleHAlign,
                            VerticalTextAlignment = titleVAlign
                        };
                        gridContent.Children.Add(labelContentTitle, 1, 0);
                        Grid.SetColumnSpan(labelContentTitle, 3);

                        Image imageContentExpandCollapse = new Image ()
                        {
                            Source = imagePathArrowExpand
                        };
                        gridContent.Children.Add(imageContentExpandCollapse, 4, 0);

                        Button buttonContent = new Button ()
                        {
                            BackgroundColor = Color.Transparent
                        };
                        gridContent.Children.Add(buttonContent, 0, 0);
                        Grid.SetColumnSpan(buttonContent, 4);

                        StackLayout stackContentBody = new StackLayout ()
                        {
                            BackgroundColor = elementBackgroundColor,
                            IsVisible = openTextByDefault
                        };
                        content.Children.Add(stackContentBody);

                        stackContentBody.Children.Add(CreateCollapsibleTextView(text, elementTitleColor));

                        buttonContent.Clicked += (s, e) =>
                        {
                            stackContentBody.IsVisible = !stackContentBody.IsVisible;

                            if (stackContentBody.IsVisible)
                                imageContentExpandCollapse.Source = imagePathArrowCollapse;
                            else
                                imageContentExpandCollapse.Source = imagePathArrowExpand;
                        };
                    }
                    else
                    {
                        View subCategoryArticleElement = CreateArticleElementView(article, elementBackgroundColor, elementTitleFontSize, elementTitleColor, titleHAlign, titleVAlign);

                        if (subCategoryArticleElement != null)
                            content.Children.Add(subCategoryArticleElement);
                    }
                }
            }
            #endregion

            #region Content.SubCategories
            if (Category.SubCategories != null && Category.SubCategories.Count > 0)
            {
                bool hasArticles = false;
                bool hasSubCategories = false;
                bool hasNoChildren = false;
                bool hasOnlySubCategories = false;
                bool hasOnlyOneTextArticle = false;
                string subCategoryName = string.Empty;
                string subCategoryIconPath = string.Empty;
                Color subViewColor = Color.Transparent;

                foreach (AppCategory subCategory in Category.SubCategories)
                {
                    if (subCategory == null)
                        continue;

                    if (!subCategory.Name.ToLower().Contains(FilterQuery))
                        continue;

                    hasArticles = (subCategory.Articles != null && subCategory.Articles.Count > 0);
                    hasSubCategories = (subCategory.SubCategories != null && subCategory.SubCategories.Count > 0);
                    hasNoChildren = (!hasArticles && !hasSubCategories);
                    hasOnlySubCategories = (!hasArticles && hasSubCategories);
                    hasOnlyOneTextArticle = (hasArticles && !hasSubCategories &&
                                            subCategory.Articles.Count == 1 && subCategory.Articles[0] != null &&
                                            (subCategory.Articles[0].GetArticleType().Equals(AppArticleType.Text) || subCategory.Articles[0].GetArticleType().Equals(AppArticleType.DirectText)));
                    subCategoryName = subCategory.Name;
                    subCategoryIconPath = subCategory.GetIconFileContentPath();

                    subViewColor = UtilService.SafeFromHex(subCategory.ViewColor, Color.Transparent);

                    content.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 10 });

                    Grid gridContent = CreateContentGrid(subViewColor);
                    content.Children.Add(gridContent);

                    Image imageContentIcon = new Image ()
                    {
                        Source = subCategoryIconPath
                    };
                    gridContent.Children.Add(imageContentIcon, 1, 0);

                    Label labelContentTitle = new Label ()
                    {
                        Text = subCategoryName,

                        FontSize = contentTitleFontSize,
                        TextColor = textColor,
                        HorizontalTextAlignment = titleHAlign,
                        VerticalTextAlignment = titleVAlign
                    };
                    gridContent.Children.Add(labelContentTitle, 3, 0);

                    Image imageContentExpandCollapse = new Image ()
                    {
                        Source = (hasNoChildren || hasOnlySubCategories) ? imagePathArrowNext : imagePathArrowExpand
                    };
                    gridContent.Children.Add(imageContentExpandCollapse, 4, 0);

                    Button buttonContent = new Button ()
                    {
                        BackgroundColor = Color.Transparent
                    };
                    gridContent.Children.Add(buttonContent, 0, 0);
                    Grid.SetColumnSpan(buttonContent, 4);

                    if (hasNoChildren || hasOnlySubCategories)
                    {
                        buttonContent.CommandParameter = subCategory;
                        buttonContent.SetBinding(Button.CommandProperty, "NavigateToSubPageCommand");
                    }
                    else
                    {
                        StackLayout stackContentBody = new StackLayout ()
                        {
                            IsVisible = (openTextByDefault && hasOnlyOneTextArticle) ? true : false,
                            BackgroundColor = subViewColor,

                            Spacing = 0
                        };
                        content.Children.Add(stackContentBody);

                        buttonContent.Clicked += (s, e) =>
                        {
                            stackContentBody.IsVisible = !stackContentBody.IsVisible;

                            if (stackContentBody.IsVisible)
                                imageContentExpandCollapse.Source = imagePathArrowCollapse;
                            else
                                imageContentExpandCollapse.Source = imagePathArrowExpand;
                        };

                        if (hasOnlyOneTextArticle)
                        {
                            string text = (subCategory.Articles[0].GetArticleType().Equals(AppArticleType.DirectText)) ? subCategory.Articles[0].Value : subCategory.Articles[0].TextValue;

                            stackContentBody.Children.Add(CreateCollapsibleTextView(text, elementTitleColor));
                        }
                        else
                        {
                            foreach (AppArticle subCategoryArticle in subCategory.Articles)
                            {
                                if (subCategoryArticle == null)
                                    continue;

                                if (subCategoryArticle.GetArticleType().Equals(AppArticleType.Text) || subCategoryArticle.GetArticleType().Equals(AppArticleType.DirectText))
                                    continue;

                                stackContentBody.Children.Add(CreateSeparator(separatorColor));

                                View subCategoryArticleElement = CreateArticleElementView(subCategoryArticle, elementBackgroundColor, elementTitleFontSize, elementTitleColor, titleHAlign, titleVAlign);

                                if (subCategoryArticleElement != null)
                                    stackContentBody.Children.Add(subCategoryArticleElement);
                            }
                        }

                        stackContentBody.Children.Add(CreateSeparator(separatorColor));

                        View subCategorySubCategoryElement = CreateSubCategoryElementView(subCategory, elementGrayBackgroundColor, elementTitleFontSize, elementTitleColor, titleHAlign, titleVAlign);

                        if (subCategorySubCategoryElement != null)
                            stackContentBody.Children.Add(subCategorySubCategoryElement);
                    }
                }
            }
            #endregion

            #region Content.LastSeparator
            content.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 10 });
            #endregion

            #region Content.Upload
            if (allowUpload)
            {
                View addArticleElement = CreateAddArticleElementView(elementGrayBackgroundColor, elementTitleFontSize, elementTitleColor, titleHAlign, titleVAlign);

                if (addArticleElement != null)
                    content.Children.Add(addArticleElement);
            }
            #endregion

            #region Content.ComingSoon
            if (!allowUpload)
            {
                if ((Category.Articles != null && Category.Articles.Count > 0) ||
                    (Category.SubCategories != null && Category.SubCategories.Count > 0))
                { }
                else
                {
                    StackLayout stackComingSoon = new StackLayout ()
                    {
                        BackgroundColor = Color.Transparent,
                        Padding = new Thickness(15, 15),
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    content.Children.Add(stackComingSoon);

                    Label labelComingSoon = new Label ()
                    {
                        Text = "Coming Soon",
                        TextColor = textColor,
                        FontSize = contentTitleFontSize,
                        HorizontalTextAlignment = TextAlignment.Center,
                        VerticalTextAlignment = TextAlignment.Center
                    };
                    stackComingSoon.Children.Add(labelComingSoon);
                }
            }
            #endregion
            #endregion

            main.Content = content;
            _scrollView = main;
            Content = main;
        }

        private View CreateSeparator (Color separatorColor)
        {
            return new HorizontalSeparator() { Color = separatorColor };
        }

        private View CreateCollapsibleTextView (string text, Color textColor)
        {
            StackLayout textStack = new StackLayout ()
            {
                BackgroundColor = Color.White
            };

            if (Device.OS.Equals(TargetPlatform.Android))
            {
                HtmlSelectableLabelDroid textView = new HtmlSelectableLabelDroid ()
                {
                    Text = text,
                    TextColor = textColor,
                    Margin = new Thickness(10, 10)
                };
                textStack.Children.Add(textView);
            }
            else
            {
                HtmlSelectableLabeliOS textView = new HtmlSelectableLabeliOS ()
                {
                    Source = text,
                    TextColor = textColor,
                    Margin = new Thickness(10, 10),

                    FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label))
                };
                textStack.Children.Add(textView);
            }

            return textStack;
        }

        private View CreateArticleElementView (AppArticle article, Color backgroundColor, double titleFontSize, Color titleColor, TextAlignment titleHAlign, TextAlignment titleVAlign)
        {
            if (article == null)
                return null;

            bool isArticleIconAvailable = article.IsArticleIconAvailable();
            string imagePathArticle = isArticleIconAvailable ? article.GetIconFileContentPath() : article.GetFileExtensionImagePath();
            string articleTitle = article.Title;
            AppArticleType articleType = article.GetArticleType();

            StackLayout stackArticleElement = new StackLayout ()
            {
                BackgroundColor = backgroundColor,
                Padding = new Thickness(10, 10)
            };

            Grid gridElement = CreateElementGrid();
            stackArticleElement.Children.Add(gridElement);

            Image imageElementIcon = new Image ()
            {
                Source = imagePathArticle
            };
            gridElement.Children.Add(imageElementIcon, 0, 0);

            Label labelElementTitle = new Label ()
            {
                Text = articleTitle,
                HorizontalOptions = LayoutOptions.FillAndExpand,

                FontSize = titleFontSize,
                TextColor = titleColor,
                HorizontalTextAlignment = titleHAlign,
                VerticalTextAlignment = titleVAlign
            };
            gridElement.Children.Add(labelElementTitle, 2, 0);

            Button buttonElement = new Button ()
            {
                BackgroundColor = Color.Transparent
            };
            gridElement.Children.Add(buttonElement, 0, 0);
            Grid.SetColumnSpan(buttonElement, 3);

            if (articleType.Equals(AppArticleType.Link) || articleType.Equals(AppArticleType.LinkLogin) || articleType.Equals(AppArticleType.LinkExternal) ||
                articleType.Equals(AppArticleType.LinkAutoFill) || articleType.Equals(AppArticleType.LinkCredential))
            {
                string link = article.Value;

                if (string.IsNullOrEmpty(link))
                    link = string.Empty;

                if (articleType.Equals(AppArticleType.LinkAutoFill))
                {
                    string username = DataService.Username;
                    string password = DataService.Password;

                    if (string.IsNullOrEmpty(username)) username = "username";
                    if (string.IsNullOrEmpty(password)) password = "password";

                    buttonElement.CommandParameter = new AppLinkFill { Title = articleTitle, Value = link, Username = username, Password = password };
                    buttonElement.SetBinding(Button.CommandProperty, "NavigateToWebFillCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkCredential))
                {
                    string domain = DataService.BrowserCredentials.Domain;
                    string username = DataService.BrowserCredentials.Username;
                    string password = DataService.BrowserCredentials.Password;

                    if (string.IsNullOrEmpty(username)) domain = string.Empty;
                    if (string.IsNullOrEmpty(username)) username = "username";
                    if (string.IsNullOrEmpty(password)) password = "password";

                    buttonElement.CommandParameter = new AppLinkFill { Title = articleTitle, Value = link, Username = (domain + username), Password = password };
                    buttonElement.SetBinding(Button.CommandProperty, "NavigateToWebFillCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkExternal))
                {
                    buttonElement.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    buttonElement.SetBinding(Button.CommandProperty, "NavigateToWebExternalCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkLogin))
                {
                    buttonElement.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    buttonElement.SetBinding(Button.CommandProperty, "NavigateToAuthenticatingWebCommand");
                }
                else
                {
                    buttonElement.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    buttonElement.SetBinding(Button.CommandProperty, "NavigateToWebCommand");
                }
            }
            else
            {
                buttonElement.CommandParameter = article;
                buttonElement.SetBinding(Button.CommandProperty, "NavigateToFileAttachmentCommand");
            }

            return stackArticleElement;
        }

        private View CreateSubCategoryElementView (AppCategory category, Color backgroundColor, double titleFontSize, Color titleColor, TextAlignment titleHAlign, TextAlignment titleVAlign)
        {
            if (category == null)
                return null;

            string imagePathArticleFileExtension = AppTheme.FileTypeIconMore;
            string title = "See More.";

            StackLayout stackSubCategoryElement = new StackLayout ()
            {
                BackgroundColor = backgroundColor,
                Padding = new Thickness(10, 10)
            };

            Grid gridElement = CreateElementGrid();
            stackSubCategoryElement.Children.Add(gridElement);

            Image imageElementIcon = new Image ()
            {
                Source = imagePathArticleFileExtension
            };
            gridElement.Children.Add(imageElementIcon, 0, 0);

            Label labelElementTitle = new Label ()
            {
                Text = title,
                HorizontalOptions = LayoutOptions.FillAndExpand,

                FontSize = titleFontSize,
                TextColor = titleColor,
                HorizontalTextAlignment = titleHAlign,
                VerticalTextAlignment = titleVAlign
            };
            gridElement.Children.Add(labelElementTitle, 2, 0);

            Button buttonElement = new Button ()
            {
                BackgroundColor = Color.Transparent
            };
            gridElement.Children.Add(buttonElement, 0, 0);
            Grid.SetColumnSpan(buttonElement, 3);

            buttonElement.CommandParameter = category;
            buttonElement.SetBinding(Button.CommandProperty, "NavigateToSubPageCommand");

            return stackSubCategoryElement;
        }

        private View CreateAddArticleElementView (Color backgroundColor, double titleFontSize, Color titleColor, TextAlignment titleHAlign, TextAlignment titleVAlign)
        {
            string imagePathArticleFileExtension = AppTheme.FileTypeIconUpload;
            string title = "Add Article.";

            StackLayout stackAddArticleElement = new StackLayout ()
            {
                BackgroundColor = backgroundColor,
                Padding = new Thickness(10, 10)
            };

            Grid gridElement = CreateElementGrid();
            stackAddArticleElement.Children.Add(gridElement);

            Image imageElementIcon = new Image ()
            {
                Source = imagePathArticleFileExtension
            };
            gridElement.Children.Add(imageElementIcon, 0, 0);

            Label labelElementTitle = new Label ()
            {
                Text = title,
                HorizontalOptions = LayoutOptions.FillAndExpand,

                FontSize = titleFontSize,
                TextColor = titleColor,
                HorizontalTextAlignment = titleHAlign,
                VerticalTextAlignment = titleVAlign
            };
            gridElement.Children.Add(labelElementTitle, 2, 0);

            Button buttonElement = new Button ()
            {
                BackgroundColor = Color.Transparent
            };
            gridElement.Children.Add(buttonElement, 0, 0);
            Grid.SetColumnSpan(buttonElement, 3);

            buttonElement.SetBinding(Button.CommandProperty, "NavigateToUploadPageCommand");

            return stackAddArticleElement;
        }

        private Grid CreateHeaderGrid ()
        {
            int headerHeight = Device.OnPlatform(55, 65, 65);

            Grid headerGrid = new Grid ()
            {
                BackgroundColor = Color.Transparent,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(40) },
                    new ColumnDefinition { Width = new GridLength(25) },
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(headerHeight) }
                }
            };

            return headerGrid;
        }

        private Grid CreateContentGrid (Color viewColor)
        {
            Grid contentGrid = new Grid ()
            {
                BackgroundColor = viewColor,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(40) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(25) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(50) }
                }
            };

            return contentGrid;
        }

        private Grid CreateElementGrid ()
        {
            Grid elementGrid = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(50) },
                    new ColumnDefinition { Width = new GridLength(15) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(50) }
                }
            };

            return elementGrid;
        }
    }
}
