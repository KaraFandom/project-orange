﻿using ProjectOrange.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectOrange.Models
{
    public class AppBrowserCredentials
    {
        private const string DOMAIN_KEY = "AppBrowserCredentials.Domain";
        private const string USERNAME_KEY = "AppBrowserCredentials.Username";
        private const string PASSWORD_KEY = "AppBrowserCredentials.Password";

        private bool _isInitialized = false;
        private string _domain = string.Empty;
        private string _username = string.Empty;
        private string _password = string.Empty;

        public bool IsInitialized { get { return _isInitialized; } }
        public string Domain { get { return _domain; } }
        public string Username { get { return _username; } }
        public string Password { get { return _password; } }

        public bool HasUsername { get { return !string.IsNullOrEmpty(Username); } }
        public bool HasPassword { get { return !string.IsNullOrEmpty(Password); } }
        public bool IsValid { get { return (HasUsername && HasPassword); } }

        public AppBrowserCredentials () { }
        public AppBrowserCredentials (string domain, string username, string password)
        {
            _domain = domain;
            _username = username;
            _password = password;
        }

        public void TryInitialize (bool reloadAlways = false)
        {
            if (reloadAlways || !IsInitialized)
                Load();
        }

        public void Load ()
        {
            string domain = (string) DataService.LoadAppProperty(DOMAIN_KEY, string.Empty);
            string username = (string) DataService.LoadAppProperty(USERNAME_KEY, string.Empty);
            string password = (string) DataService.LoadAppProperty(PASSWORD_KEY, string.Empty);

            if (!username.IsNullOrEmpty() && !password.IsNullOrEmpty())
            {
                _domain = domain;
                _username = username;
                _password = password;

                _isInitialized = true;
            }
            else
            {
                _domain = string.Empty;
                _username = string.Empty;
                _password = string.Empty;

                _isInitialized = false;
            }
        }

        public async Task<bool> Clear ()
        {
            try
            {
                List<Tuple<string, object>> properties = new List<Tuple<string, object>>();
                properties.Add(new Tuple<string, object>(DOMAIN_KEY, string.Empty));
                properties.Add(new Tuple<string, object>(USERNAME_KEY, string.Empty));
                properties.Add(new Tuple<string, object>(PASSWORD_KEY, string.Empty));

                await DataService.SaveAppProperty(properties.ToArray());

                _isInitialized = false;
                _domain = string.Empty;
                _username = string.Empty;
                _password = string.Empty;

                return true;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
                return false;
            }
        }

        public async Task<bool> Save (string domain, string username, string password)
        {
            if ((!string.IsNullOrEmpty(domain) && !domain.EndsWith("\\")) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;

            try
            {
                List<Tuple<string, object>> properties = new List<Tuple<string, object>>();
                properties.Add(new Tuple<string, object>(DOMAIN_KEY, domain));
                properties.Add(new Tuple<string, object>(USERNAME_KEY, username));
                properties.Add(new Tuple<string, object>(PASSWORD_KEY, password));

                await DataService.SaveAppProperty(properties.ToArray());
                Load();

                return true;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
