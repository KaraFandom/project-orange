﻿using ProjectOrange.CustomControls;
using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using ProjectOrange.Services;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class SettingsPage : ContentPage
    {
        public SettingsPage ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Initialization
            double entryFontSizeSmall = Device.GetNamedSize(NamedSize.Small, typeof(Entry));
            double labelFontSizeSmall = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            double labelFontSizeMicro = Device.GetNamedSize(NamedSize.Micro, typeof(Label));
            string imagePathBackground = AppTheme.BackgroundDefault;
            string imagePathHamburger = AppTheme.HamburgerDefault;
            Color entryTextColor = Device.OnPlatform(Color.Black, Color.White, Color.White);
            Color buttonColor = Color.FromHex(AppTheme.ButtonColorDefault);
            Color separatorColor = Color.FromHex(AppTheme.SeperatorColorDefault);
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);

            if (DataService.Theme != null)
            {
                if (DataService.Theme.BackgroundImage != null)
                    imagePathBackground = DataService.Theme.BackgroundImage.GetFileContentPath();

                if (DataService.Theme.IconHamburgerImage != null)
                    imagePathHamburger = DataService.Theme.IconHamburgerImage.GetFileContentPath();

                buttonColor = DataService.Theme.ButtonColorValue;

                separatorColor = DataService.Theme.SeperatorColorValue;

                textColor = DataService.Theme.TextColorValue;
            }

            RelativeLayout main = new RelativeLayout ()
            {

            };

            Image background = new Image ()
            {
                Source = imagePathBackground,
                Aspect = Aspect.AspectFill
            };
            main.Children.Add(background,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0
            };
            main.Children.Add(content,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));

            PONavBar navBar = new PONavBar ()
            {
                Title = "Settings",
                UseBanner = false,
                UseButtonRight = false,
                ImagePathButtonLeft = imagePathHamburger
            };
            navBar.Redraw();
            navBar.ButtonLeft.SetBinding(Button.CommandProperty, "ShowMasterDetailCommand");
            content.Children.Add(navBar);

            Label labelWarnings = new Label ()
            {
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HeightRequest = 30,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,

                TextColor = textColor
            };
            labelWarnings.SetBinding(Label.IsVisibleProperty, "IsWarningsVisible");
            labelWarnings.SetBinding(Label.TextProperty, "WarningsText");
            labelWarnings.SetBinding(Label.BackgroundColorProperty, "WarningsColor");
            content.Children.Add(labelWarnings);

            ScrollView scrollContent = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            content.Children.Add(scrollContent);

            StackLayout scrollBody = new StackLayout ()
            {
                Padding = new Thickness(15, 15)
            };
            scrollContent.Content = scrollBody;

            Label labelServerURL = new Label ()
            {
                Text = "Server URL",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            scrollBody.Children.Add(labelServerURL);

            Entry entryServerURL = new Entry ()
            {
                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryServerURL.SetBinding(Entry.TextProperty, "ServerURL");
            scrollBody.Children.Add(entryServerURL);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Label labelAPIKey = new Label ()
            {
                Text = "API Key",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            scrollBody.Children.Add(labelAPIKey);

            Entry entryAPIKey = new Entry ()
            {
                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryAPIKey.SetBinding(Entry.TextProperty, "ApiKey");
            scrollBody.Children.Add(entryAPIKey);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Label labelAPIAccess = new Label ()
            {
                Text = "API Access",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            scrollBody.Children.Add(labelAPIAccess);

            Entry entryAPIAccess = new Entry ()
            {
                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryAPIAccess.SetBinding(Entry.TextProperty, "ApiAccess");
            scrollBody.Children.Add(entryAPIAccess);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Label labelAPIUsername = new Label ()
            {
                Text = "API Username",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            scrollBody.Children.Add(labelAPIUsername);

            Entry entryAPIUsername = new Entry ()
            {
                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryAPIUsername.SetBinding(Entry.TextProperty, "ApiUsername");
            scrollBody.Children.Add(entryAPIUsername);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Label labelAPIPassword = new Label ()
            {
                Text = "API Password",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            scrollBody.Children.Add(labelAPIPassword);

            Entry entryAPIPassword = new Entry ()
            {
                IsPassword = true,

                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryAPIPassword.SetBinding(Entry.TextProperty, "ApiPassword");
            scrollBody.Children.Add(entryAPIPassword);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Label labelUsername = new Label ()
            {
                Text = "Username",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            scrollBody.Children.Add(labelUsername);

            Label labelUsernameValue = new Label ()
            {
                Margin = new Thickness(10, 0, 0, 0),

                FontSize = labelFontSizeMicro,
                TextColor = textColor
            };
            labelUsernameValue.SetBinding(Label.TextProperty, "Username");
            scrollBody.Children.Add(labelUsernameValue);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Label labelSiteCode = new Label ()
            {
                Text = "Site Code",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            scrollBody.Children.Add(labelSiteCode);

            Label labelSiteCodeValue = new Label ()
            {
                Margin = new Thickness(10, 0, 0, 0),

                FontSize = labelFontSizeMicro,
                TextColor = textColor
            };
            labelSiteCodeValue.SetBinding(Label.TextProperty, "SiteCode");
            scrollBody.Children.Add(labelSiteCodeValue);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Label labelWebCredentials = new Label ()
            {
                Text = "Browser Credentials (Optional)",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            scrollBody.Children.Add(labelWebCredentials);

            StackLayout stackLayoutWebCredentials = new StackLayout ()
            {
                Padding = new Thickness(5, 0)
            };
            scrollBody.Children.Add(stackLayoutWebCredentials);

            Label labelWebCredentialsDomain = new Label ()
            {
                Text = "Domain",

                FontSize = labelFontSizeMicro,
                TextColor = textColor
            };
            stackLayoutWebCredentials.Children.Add(labelWebCredentialsDomain);

            Entry entryWebCredentialsDomain = new Entry ()
            {
                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryWebCredentialsDomain.SetBinding(Entry.TextProperty, "WebCredentialsDomain");
            stackLayoutWebCredentials.Children.Add(entryWebCredentialsDomain);

            Label labelWebCredentialsUsername = new Label ()
            {
                Text = "Username",

                FontSize = labelFontSizeMicro,
                TextColor = textColor
            };
            stackLayoutWebCredentials.Children.Add(labelWebCredentialsUsername);

            Entry entryWebCredentialsUsername = new Entry ()
            {
                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryWebCredentialsUsername.SetBinding(Entry.TextProperty, "WebCredentialsUsername");
            stackLayoutWebCredentials.Children.Add(entryWebCredentialsUsername);

            Label labelWebCredentialsPassword = new Label ()
            {
                Text = "Password",

                FontSize = labelFontSizeMicro,
                TextColor = textColor
            };
            stackLayoutWebCredentials.Children.Add(labelWebCredentialsPassword);

            Entry entryWebCredentialsPassword = new Entry ()
            {
                IsPassword = true,

                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryWebCredentialsPassword.SetBinding(Entry.TextProperty, "WebCredentialsPassword");
            stackLayoutWebCredentials.Children.Add(entryWebCredentialsPassword);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 10 });

            Button buttonSaveProperties = new Button ()
            {
                Text = "Save",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonSaveProperties.SetBinding(Button.CommandProperty, "SavePropertiesCommand");
            scrollBody.Children.Add(buttonSaveProperties);

            Button buttonResetProperties = new Button ()
            {
                Text = "Reset",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonResetProperties.SetBinding(Button.CommandProperty, "ResetPropertiesCommand");
            scrollBody.Children.Add(buttonResetProperties);

            StackLayout stackSetPinCode_A = new StackLayout ()
            {

            };
            stackSetPinCode_A.SetBinding(StackLayout.IsVisibleProperty, "IsSettingPinCode_A");
            scrollBody.Children.Add(stackSetPinCode_A);

            stackSetPinCode_A.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            stackSetPinCode_A.Children.Add(new HorizontalSeparator { Color = separatorColor });

            stackSetPinCode_A.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            StackLayout stackSetPinCode_B = new StackLayout ()
            {

            };
            stackSetPinCode_B.SetBinding(StackLayout.IsVisibleProperty, "IsSettingPinCode_B");
            scrollBody.Children.Add(stackSetPinCode_B);

            Button buttonSetPinCode = new Button ()
            {
                Text = "Set Pin Code",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonSetPinCode.SetBinding(Button.CommandProperty, "SetPinCodeCommand");
            stackSetPinCode_B.Children.Add(buttonSetPinCode);

            StackLayout stackSetPinCode_C = new StackLayout ()
            {

            };
            stackSetPinCode_C.SetBinding(StackLayout.IsVisibleProperty, "IsSettingPinCode_C");
            scrollBody.Children.Add(stackSetPinCode_C);

            Label labelPinCodeCurrent = new Label ()
            {
                Text = "Current Pin Code",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            stackSetPinCode_C.Children.Add(labelPinCodeCurrent);

            Entry entryPinCodeCurrent = new Entry ()
            {
                HorizontalTextAlignment = TextAlignment.Center,
                IsPassword = true,
                Keyboard = Keyboard.Numeric,

                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryPinCodeCurrent.SetBinding(Entry.TextProperty, "PinCodeCurrent");
            stackSetPinCode_C.Children.Add(entryPinCodeCurrent);

            StackLayout stackSetPinCode_D = new StackLayout ()
            {

            };
            stackSetPinCode_D.SetBinding(StackLayout.IsVisibleProperty, "IsSettingPinCode_D");
            scrollBody.Children.Add(stackSetPinCode_D);

            Label labelPinCodeNew = new Label ()
            {
                Text = "New Pin Code",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            stackSetPinCode_D.Children.Add(labelPinCodeNew);

            Entry entryPinCodeNew = new Entry ()
            {
                HorizontalTextAlignment = TextAlignment.Center,
                IsPassword = true,
                Keyboard = Keyboard.Numeric,

                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryPinCodeNew.SetBinding(Entry.TextProperty, "PinCodeNew");
            stackSetPinCode_D.Children.Add(entryPinCodeNew);

            Label labelPinCodeNewNew = new Label ()
            {
                Text = "Re-type New Pin Code",

                FontSize = labelFontSizeSmall,
                TextColor = textColor
            };
            stackSetPinCode_D.Children.Add(labelPinCodeNewNew);

            Entry entryPinCodeNewNew = new Entry ()
            {
                HorizontalTextAlignment = TextAlignment.Center,
                IsPassword = true,
                Keyboard = Keyboard.Numeric,

                FontSize = entryFontSizeSmall,
                TextColor = entryTextColor
            };
            entryPinCodeNewNew.SetBinding(Entry.TextProperty, "PinCodeNewNew");
            stackSetPinCode_D.Children.Add(entryPinCodeNewNew);

            Button buttonSavePinCode = new Button ()
            {
                Text = "Save",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonSavePinCode.SetBinding(Button.CommandProperty, "SavePinCodeCommand");
            stackSetPinCode_D.Children.Add(buttonSavePinCode);

            StackLayout stackSetPinCode_E = new StackLayout()
            {

            };
            stackSetPinCode_E.SetBinding(StackLayout.IsVisibleProperty, "IsSettingPinCode_E");
            scrollBody.Children.Add(stackSetPinCode_E);

            Button buttonDeletePinCode = new Button ()
            {
                Text = "Delete",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonDeletePinCode.SetBinding(Button.CommandProperty, "DeletePinCodeCommand");
            stackSetPinCode_E.Children.Add(buttonDeletePinCode);

            StackLayout stackSetPinCode_F = new StackLayout()
            {

            };
            stackSetPinCode_F.SetBinding(StackLayout.IsVisibleProperty, "IsSettingPinCode_F");
            scrollBody.Children.Add(stackSetPinCode_F);

            Button buttonCancelPinCode = new Button ()
            {
                Text = "Cancel",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonCancelPinCode.SetBinding(Button.CommandProperty, "CancelPinCodeCommand");
            stackSetPinCode_F.Children.Add(buttonCancelPinCode);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            scrollBody.Children.Add(new HorizontalSeparator { Color = separatorColor });

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Button buttonCleanCache = new Button ()
            {
                Text = "Clean Cache",

                BackgroundColor = buttonColor,
                TextColor = textColor
            };
            buttonCleanCache.SetBinding(Button.CommandProperty, "CleanCacheCommand");
            scrollBody.Children.Add(buttonCleanCache);
            #endregion

            Content = main;
        }
    }
}
