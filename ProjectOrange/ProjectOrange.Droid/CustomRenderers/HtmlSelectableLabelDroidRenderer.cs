using System.ComponentModel;
using ProjectOrange.CustomViews;
using ProjectOrange.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Text.Util;
using Android.Text;

[assembly: ExportRenderer(typeof(HtmlSelectableLabelDroid), typeof(HtmlSelectableLabelDroidRenderer))]
namespace ProjectOrange.Droid.CustomRenderers
{
    public class HtmlSelectableLabelDroidRenderer : LabelRenderer
    {
        private bool checkForLists = true;

        protected override void OnElementChanged (ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            Redraw();
        }

        protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            Redraw();
        }

        private void Redraw ()
        {
            if (Control != null)
            {
                Control.AutoLinkMask = MatchOptions.All;
                Control.SetTextIsSelectable(true);
                Control.SetText(Html.FromHtml((checkForLists ? (Element.Text.Replace("<li>", " &#149; ")) : Element.Text)),
                                Android.Widget.TextView.BufferType.Spannable);
            }
        }
    }
}