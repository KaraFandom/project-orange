﻿using System;

namespace ProjectOrange.Services.Interfaces
{
    public interface IFileService
    {
        byte[] Compress (byte[] data);

        byte[] Decompress (byte[] data);

        DateTime GetLastWriteTime (string path);

        void SetLastWriteTime (string path, DateTime lastWriteTime);
    }
}
