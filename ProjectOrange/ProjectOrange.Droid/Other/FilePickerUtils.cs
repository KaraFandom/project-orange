using Android.Content;
using Android.Database;
using Android.OS;
using Android.Provider;
using Android.Webkit;
using Java.IO;
using ProjectOrange.Services;
using System;

namespace ProjectOrange.Droid.Other
{
    public class FilePickerUtils
    {
        public static bool IsDownloadsDocument (Android.Net.Uri uri)
        {
            return "com.android.providers.downloads.documents".Equals(uri.Authority);
        }

        public static bool IsExternalStorageDocument (Android.Net.Uri uri)
        {
            return "com.android.externalstorage.documents".Equals(uri.Authority);
        }

        public static bool IsMediaDocument (Android.Net.Uri uri)
        {
            return "com.android.providers.media.documents".Equals(uri.Authority);
        }

        public static string GetDataColumn (Context context, Android.Net.Uri uri, string selection, string[] selectionArgs)
        {
            ICursor cursor = null;
            string column = "_data";
            string[] projection = { column };

            try
            {
                cursor = context.ContentResolver.Query(uri, projection, selection, selectionArgs, null);

                if (cursor != null && cursor.MoveToFirst())
                {
                    int column_index = cursor.GetColumnIndexOrThrow(column);

                    return cursor.GetString(column_index);
                }
            }
            finally
            {
                if (cursor != null)
                    cursor.Close();
            }

            return null;
        }

        public static string GetMimeType (string url)
        {
            string type = null;
            string extension = MimeTypeMap.GetFileExtensionFromUrl(url);

            if (!string.IsNullOrEmpty(extension))
                type = MimeTypeMap.Singleton.GetMimeTypeFromExtension(extension);

            return type;
        }

        public static string GetFileName (Context context, Android.Net.Uri uri)
        {
            ICursor cursor = null;
            string fileName = string.Empty;
            string[] projection = { MediaStore.MediaColumns.DisplayName };

            try
            {
                cursor = context.ContentResolver.Query(uri, projection, null, null, null);

                if (cursor != null && cursor.MoveToFirst())
                {
                    fileName = cursor.GetString(0);

                    return fileName;
                }
            }
            finally
            {
                if (cursor != null)
                    cursor.Close();
            }

            return fileName;
        }

        public static string GetFilePath (Context context, Android.Net.Uri uri)
        {
            bool isKitKat = Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat;

            if (isKitKat && DocumentsContract.IsDocumentUri(context, uri))
            {
                string documentId = DocumentsContract.GetDocumentId(uri);

                if (IsExternalStorageDocument(uri))
                {
                    string[] split = documentId.Split(':');
                    string type = split[0];

                    if ("primary".Equals(type, StringComparison.OrdinalIgnoreCase))
                        return Android.OS.Environment.ExternalStorageDirectory + "/" + split[1];

                    // TODO: Handle non-primary volumes.
                }
                else if (IsDownloadsDocument(uri))
                {
                    Android.Net.Uri contentUri = ContentUris.WithAppendedId(Android.Net.Uri.Parse("content://downloads/public_downloads"), long.Parse(documentId));

                    return GetDataColumn(context, contentUri, null, null);
                }
                else if (IsMediaDocument(uri))
                {
                    string[] split = documentId.Split(':');
                    string type = split[0];
                    var selection = "_id=?";
                    var selectionArgs = new string[] { split[1] };

                    Android.Net.Uri contentUri = null;

                    if ("image".Equals(type)) contentUri = MediaStore.Images.Media.ExternalContentUri;
                    else if ("video".Equals(type)) contentUri = MediaStore.Video.Media.ExternalContentUri;
                    else if ("audio".Equals(type)) contentUri = MediaStore.Audio.Media.ExternalContentUri;

                    return GetDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            else if ("content".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
                return GetDataColumn(context, uri, null, null);
            else if ("file".Equals(uri.Scheme, StringComparison.OrdinalIgnoreCase))
                return uri.Path;

            return null;
        }

        public static byte[] ReadFile (File file)
        {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");

            try
            {
                long longLength = randomAccessFile.Length();
                int intLength = (int) longLength;

                if (intLength != longLength)
                    throw new IOException("File size exceeds allowed size.");

                byte[] dataArray = new byte[intLength];
                randomAccessFile.ReadFully(dataArray);

                return dataArray;
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);

                return new byte[0];
            }
            finally
            {
                randomAccessFile.Close();
            }
        }

        public static byte[] ReadFile (string filePath)
        {
            try
            {
                return ReadFile(new File(filePath));
            }
            catch (Exception ex)
            {
                DebugService.WriteLine(ex.Message);

                return new byte[0];
            }
        }
    }
}