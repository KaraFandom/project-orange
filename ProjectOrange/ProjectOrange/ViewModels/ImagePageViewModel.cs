﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Services.Interfaces;
using System.IO;
using Xamarin.Forms;

namespace ProjectOrange.ViewModels
{
    public class ImagePageViewModel : BindableBase, INavigationAware
    {
        private INavigationService _navigationService;

        public DelegateCommand MailCommand { get; set; }

        public DelegateCommand NavigateBackCommand { get; set; }

        private string _source;
        public string Source
        {
            get { return _source; }
            set { SetProperty(ref _source, value); }
        }

        public ImagePageViewModel (INavigationService navigationService)
        {
            _navigationService = navigationService;

            NavigateBackCommand = new DelegateCommand(NavigateBack);
            MailCommand = new DelegateCommand(Mail);
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        { }

        public void OnNavigatedTo (NavigationParameters parameters)
        {
            if (parameters.ContainsKey("source"))
                Source = parameters["source"] as string;
        }

        private void Mail ()
        {
            IMailService mailService = DependencyService.Get<IMailService>();

            if (mailService != null)
            {
                if (!string.IsNullOrEmpty(Source))
                    mailService.Compose(null,
                                        null,
                                        null,
                                        Path.GetFileName(Source),
                                        null,
                                        false,
                                        new string[] { Path.GetFileName(Source) },
                                        null);
            }
        }

        private void NavigateBack ()
        {
            _navigationService.GoBackAsync();
        }
    }
}
