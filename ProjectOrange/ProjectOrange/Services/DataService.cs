﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectOrange.Models;
using ProjectOrange.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ProjectOrange.Services
{
    public static class DataService
    {
        public static HttpClient httpClient = new HttpClient()
        {
            MaxResponseContentBufferSize = 2147483648,
            Timeout = TimeSpan.FromMinutes(30)
        };

        public const bool AllowViewTypeToggling = true;
        public const bool AllowUploading = true;
        public const bool AlwaysRedownloadFileContent = false;
        public const bool VerboseErrors = true;
        public const bool SuppressErrors = false;
        public const string FriendlyErrorString = "Connection Error. Please Try Again.";
        public const string AppURL = "https://local.projectorange.app/";

        private static AppUser _user = new AppUser();
        private static AppTheme _theme = new AppTheme();

        public static IDatabaseService DBService = new DatabaseService_SQL();
        public static AppServerConfig ServerConfig = new AppServerConfig();
        public static AppUserCredentials UserCredentials = new AppUserCredentials();
        public static AppServerCredentials ServerCredentials = new AppServerCredentials();
        public static AppBrowserCredentials BrowserCredentials = new AppBrowserCredentials();

        public static string Username { get { return DBService.Username; } }
        public static string Password { get { return DBService.Password; } }
        public static string SiteCode { get { return DBService.SiteCode; } }

        public static AppUser User { get { return _user; } private set { _user = value; } }
        public static AppTheme Theme { get { return _theme; } private set { _theme = value; } }

        #region IDatabaseService Methods
        public static async Task<AppUser> Authenticate (string username, string password, string siteCode = "")
        {
            AppUser user = await DBService.Authenticate(username, password, siteCode);

            if (user != null)
                User = user;

            return user;
        }

        private static async Task<AppTheme> GetTheme ()
        {
            return await DBService.GetTheme();
        }

        private static async Task<List<AppCategory>> GetCategories ()
        {
            return await DBService.GetCategories();
        }

        private static async Task<List<AppArticle>> GetArticles (string categoryId)
        {
            return await DBService.GetArticles(categoryId);
        }

        private static async Task<List<AppCategory>> GetSubCategories (string categoryId)
        {
            return await DBService.GetSubCategories(categoryId);
        }

        private static async Task<AppFileContent> GetFileContent (string requestUri, string fileName, string modifiedDate)
        {
            return await DBService.GetFileContent(requestUri, fileName, modifiedDate);
        }

        public static async Task<AppPostResponse> PostTextArticle (string categoryId, string fileType, string title, string value, int sortOrder = 10)
        {
            return await DBService.PostTextArticle(categoryId, fileType, title, value, sortOrder);
        }

        public static async Task<AppPostResponse> PostFileArticle (string categoryId, string fileType, string title, string fileName, byte[] fileData, int sortOrder = 10)
        {
            return await DBService.PostFileArticle(categoryId, fileType, title, fileName, fileData, sortOrder);
        }
        #endregion

        #region IDatabaseService Helper Methods
        public static async Task<bool> ValidateServerConfig (AppServerConfig config)
        {
            if (config.ServerURL.IsNullOrEmpty() || config.ApiKey.IsNullOrEmpty() || config.ApiAccess.IsNullOrEmpty())
                return false;

            switch (config.Implementation)
            {
                case AppServerConfigImplementation.Sharepoint:
                    return await DatabaseService_Sharepoint.Validate(config.ServerURL, config.ApiKey, config.ApiAccess);
                case AppServerConfigImplementation.Sql:
                    return await DatabaseService_SQL.Validate(config.ServerURL, config.ApiKey, config.ApiAccess);
                default:
                    return false;
            }
        }
        public static async Task<Tuple<bool, AppServerConfigImplementation>> ValidateServerConfig (string serverURL, string apiKey, string apiAccess)
        {
            if (serverURL.IsNullOrEmpty() || apiKey.IsNullOrEmpty() || apiAccess.IsNullOrEmpty())
                return new Tuple<bool, AppServerConfigImplementation>(false, AppServerConfigImplementation.Sql);

            bool isValidSql = await DatabaseService_SQL.Validate(serverURL, apiKey, apiAccess);

            if (isValidSql)
                return new Tuple<bool, AppServerConfigImplementation>(true, AppServerConfigImplementation.Sql);

            bool isValidSharepoint = await DatabaseService_Sharepoint.Validate(serverURL, apiKey, apiAccess);

            if (isValidSharepoint)
                return new Tuple<bool, AppServerConfigImplementation>(true, AppServerConfigImplementation.Sharepoint);

            return new Tuple<bool, AppServerConfigImplementation>(false, AppServerConfigImplementation.Sql);
        }

        public static void LoadServerConfig ()
        {
            switch (ServerConfig.Implementation)
            {
                case AppServerConfigImplementation.Sharepoint:
                    DBService = new DatabaseService_Sharepoint(ServerConfig.ServerURL, ServerConfig.ApiKey, ServerConfig.ApiAccess);
                    break;

                case AppServerConfigImplementation.Sql:
                    DBService = new DatabaseService_SQL(ServerConfig.ServerURL, ServerConfig.ApiKey, ServerConfig.ApiAccess);
                    break;
            }

            DebugService.WriteLine(string.Format("Using .serverconfig : \nServerURL: {0}\nApiKey: {1}\nApiAccess: {2}\nImplementation: {3}",
                                                 DBService.ServerURL, DBService.ApiKey, DBService.ApiAccess, DBService.Implementation));
        }

        public static bool IsServerConfigDirty ()
        {
            if ((DBService.ServerURL != ServerConfig.ServerURL ||
                (DBService.ApiKey != ServerConfig.ApiKey)) ||
                (DBService.ApiAccess != ServerConfig.ApiAccess))
                return true;

            return false;
        }

        public static bool IsServerConfigUndefault ()
        {
            if ((DBService.ServerURL != AppServerConfig.SERVERURL_DEFAULT ||
                (DBService.ApiKey != AppServerConfig.APIKEY_DEFAULT)) ||
                (DBService.ApiAccess != AppServerConfig.APIACCESS_DEFAULT))
                return true;

            return false;
        }

        public static async Task<bool> SaveServerConfig (AppServerConfig config)
        {
            return await ServerConfig.Save(config.ServerURL, config.ApiKey, config.ApiAccess, config.Implementation);
        }

        public static async Task<string> GetJson (string requestUri, Tuple<string, string>[] customHeaders = null, bool silent = false)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, requestUri);

                request.Version = new Version(1, 0);
                request.Headers.Add("Connection", "close");
                request.Headers.Add("Accept", "*/*");

                if (customHeaders != null && customHeaders.Length > 0)
                {
                    foreach (Tuple<string, string> h in customHeaders)
                        request.Headers.Add(h.Item1, h.Item2);
                }

                DebugService.WriteLine(string.Format("GetJson: {0}", request.ToString()));

                HttpResponseMessage response = await httpClient.SendAsync(request);
                response.EnsureSuccessStatusCode();

                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                if (silent)
                    return string.Empty;

                string exMessage = !VerboseErrors ? FriendlyErrorString : ex.Message;

                if (!SuppressErrors)
                    await App.Current.MainPage.DisplayAlert("Error", exMessage, "OK");

                DebugService.WriteLine(ex.Message);

                JObject jObject = new JObject(new JProperty("ErrorMessage", exMessage));
                return jObject.ToString();
            }
        }

        public static async Task<string> PostJson (string requestUri, JObject jsonObject)
        {
            try
            {
                if (DBService is DatabaseService_Sharepoint)
                {
                    StringContent contentString = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await httpClient.PostAsync(requestUri, contentString);
                    response.EnsureSuccessStatusCode();

                    return await response.Content.ReadAsStringAsync();
                }
                else if (DBService is DatabaseService_SQL)
                {
                    MultipartFormDataContent contentMulti = new MultipartFormDataContent();
                    StringContent contentJson = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");

                    contentMulti.Add(contentJson, "\"Json\"");

                    HttpResponseMessage response = await httpClient.PostAsync(requestUri, contentMulti);
                    response.EnsureSuccessStatusCode();

                    return await response.Content.ReadAsStringAsync();
                }
                else
                    throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                string exMessage = !VerboseErrors ? FriendlyErrorString : ex.Message;

                if (!SuppressErrors)
                    await App.Current.MainPage.DisplayAlert("Error", exMessage, "OK");

                DebugService.WriteLine(exMessage);

                AppPostResponse postResponse = new AppPostResponse { ErrorMessage = exMessage, InnerException = exMessage, IsSuccessful = "false" };
                return JsonConvert.SerializeObject(postResponse);
            }
        }

        public static async Task<string> PostJsonByte (string requestUri, JObject jsonObject, string fileName, byte[] fileData)
        {
            try
            {
                MultipartFormDataContent content = new MultipartFormDataContent();
                StringContent contentJson = new StringContent(jsonObject.ToString(), Encoding.UTF8);
                contentJson.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = "\"Json\""
                };
                contentJson.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

                string contentByteMimeType = MimeType.GetMimeTypeOrDefault(Path.GetExtension(fileName), string.Empty);
                ByteArrayContent contentBytes = new ByteArrayContent(fileData);
                contentBytes.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = "\"File\"",
                    FileName = "\"" + fileName + "\""
                };
                if (!string.IsNullOrEmpty(contentByteMimeType)) contentBytes.Headers.ContentType = MediaTypeHeaderValue.Parse(contentByteMimeType);

                content.Add(contentJson);
                content.Add(contentBytes);

                HttpResponseMessage response = await httpClient.PostAsync(requestUri, content);

                response.EnsureSuccessStatusCode();

                DebugService.WriteLine(response.ReasonPhrase);

                return await response.Content.ReadAsStringAsync();
            }
            catch (HttpRequestException ex)
            {
                string exMessage = !VerboseErrors ? FriendlyErrorString : ex.Message;

                if (!SuppressErrors)
                    await App.Current.MainPage.DisplayAlert("Error", exMessage, "OK");

                DebugService.WriteLine(string.Format("Exception: {0}" + "\n" +
                                                     "Display Message: {1}" + "\n" +
                                                     "Source: {2}" + "\n" +
                                                     "InnerException: {3}" + "\n" +
                                                     "StackTrace: {4}" + "\n" +
                                                     "HelpLink: {5}",
                                                     ex.Message,
                                                     exMessage,
                                                     ex.Source,
                                                     ex.InnerException?.ToString(),
                                                     ex.StackTrace,
                                                     ex.HelpLink));

                AppPostResponse postResponse = new AppPostResponse { ErrorMessage = exMessage, InnerException = ex.InnerException?.ToString(), IsSuccessful = "false" };
                return JsonConvert.SerializeObject(postResponse);
            }
            catch (Exception ex)
            {
                string exMessage = !VerboseErrors ? FriendlyErrorString : ex.Message;

                if (!SuppressErrors)
                    await App.Current.MainPage.DisplayAlert("Error", exMessage, "OK");

                DebugService.WriteLine(exMessage);

                AppPostResponse postResponse = new AppPostResponse { ErrorMessage = exMessage, InnerException = exMessage, IsSuccessful = "false" };
                return JsonConvert.SerializeObject(postResponse);
            }
        }

        public static async Task<byte[]> GetBytes (string requestUri)
        {
            byte[] bytes = new byte[0];

            try
            {
                HttpResponseMessage response = await httpClient.GetAsync(requestUri);
                response.EnsureSuccessStatusCode();

                bytes = await response.Content.ReadAsByteArrayAsync();
            }
            catch (Exception ex)
            {
                string exMessage = !VerboseErrors ? FriendlyErrorString : ex.Message;

                if (!SuppressErrors)
                    await App.Current.MainPage.DisplayAlert("Error", exMessage, "OK");

                DebugService.WriteLine(exMessage);
            }

            return bytes;
        }

        public static async Task LoadTheme (Action<string> updateProgressText = null)
        {
            if (DBService == null)
                return;

            if (string.IsNullOrEmpty(DBService.SiteCode))
                return;

            string siteCode = DBService.SiteCode;
            DateTime start = DateTime.UtcNow;
            DateTime childStart = DateTime.UtcNow;

            AppTheme theme = await GetTheme();

            if (theme != null && string.IsNullOrEmpty(theme.ErrorMessage))
            {
                Theme = theme;

                DebugService.WriteLine(string.Format("Downloaded theme metadata in {0} seconds.",
                                                     (DateTime.UtcNow - childStart).TotalSeconds));

                Array themeImageTypes = Enum.GetValues(typeof(AppThemeImageType));

                int progress = 0;
                int progressIncrement = 100 / themeImageTypes.Length;
                string progressText = "Dowloading Data." + "\n" +
                                      "Initial setup may take a few moments." + "\n" +
                                      "{0}%";

                childStart = DateTime.UtcNow;

                foreach (AppThemeImageType tit in themeImageTypes)
                {
                    if (updateProgressText != null)
                        updateProgressText(string.Format(progressText, progress));

                    await Theme.DownloadThemeImage(siteCode, tit);
                    progress += progressIncrement;
                }

                DebugService.WriteLine(string.Format("Downloaded theme images in {0} seconds.",
                                                     (DateTime.UtcNow - childStart).TotalSeconds));
            }

            DebugService.WriteLine(string.Format("Downloaded theme in {0} seconds.",
                                                 (DateTime.UtcNow - start).TotalSeconds));
        }

        public static async Task<List<AppCategory>> LoadCategories (Action<string> updateProgressText = null)
        {
            if (DBService == null)
                return new List<AppCategory>();

            if (string.IsNullOrEmpty(DBService.SiteCode))
                return new List<AppCategory>();

            string siteCode = DBService.SiteCode;
            DateTime start = DateTime.UtcNow;

            List<AppCategory> categories = await GetCategories();

            if (categories != null && categories.Count > 0)
            {
                int progress = 0;
                int progressIncrement = 100 / categories.Count;
                string progressText = "Dowloading Data." + "\n" +
                                      "Initial setup may take a few moments." + "\n" +
                                      "{0}%";

                foreach (AppCategory category in categories)
                {
                    if (updateProgressText != null)
                        updateProgressText(string.Format(progressText, progress));

                    await category.DownloadIcon(siteCode);
                    progress += progressIncrement;
                }
            }

            DebugService.WriteLine(string.Format("Downloaded categories in {0} seconds.",
                                                 (DateTime.UtcNow - start).TotalSeconds));

            return categories;
        }

        public static async Task<AppCategory> LoadCategory (AppCategory category, Action<string> updateProgressText = null)
        {
            if (DBService == null)
                return null;

            if (string.IsNullOrEmpty(DBService.Username))
                return null;

            if (string.IsNullOrEmpty(DBService.SiteCode))
                return null;

            if (category == null)
                return null;

            bool downloadChildArticleIcons = true;
            bool downloadChildCategoryIcons = true;
            bool preloadChildArticleTexts = true;
            bool preloadGrandchildren = true;
            bool preloadGrandchildArticleTexts = true;
            string username = DBService.Username;
            string siteCode = DBService.SiteCode;
            DateTime start = DateTime.UtcNow;
            DateTime childStart = DateTime.UtcNow;

            List<AppArticle> childArticles = await GetArticles(category.Id);

            if (childArticles != null && childArticles.Count > 0)
            {
                List<AppArticle> childArticles_Text = new List<AppArticle>();
                List<AppArticle> childArticles_Other = new List<AppArticle>();

                foreach (AppArticle a in childArticles)
                {
                    if (downloadChildArticleIcons)
                        await a.DownloadIcon(siteCode);

                    if (preloadChildArticleTexts && a.GetArticleType() == AppArticleType.Text)
                        await a.PreloadFileContent(siteCode);

                    if (a.GetArticleType() == AppArticleType.Text || a.GetArticleType() == AppArticleType.DirectText)
                        childArticles_Text.Add(a);
                    else
                        childArticles_Other.Add(a);
                }

                category.Articles.Clear();
                category.Articles.AddRange(childArticles_Other.OrderBy(x => x.SortOrder));
                category.Articles.AddRange(childArticles_Text.OrderBy(x => x.SortOrder));

                DebugService.WriteLine(string.Format("Downloaded category (#{0}) '{1}' {2} in {3} seconds.",
                                                     category.Id, category.Name, "child articles", (DateTime.UtcNow - childStart).TotalSeconds));
            }

            childStart = DateTime.UtcNow;

            List<AppCategory> childCategories = await GetSubCategories(category.Id);

            if (childCategories != null && childCategories.Count > 0)
            {
                foreach (AppCategory c in childCategories)
                {
                    if (downloadChildCategoryIcons)
                        await c.DownloadIcon(siteCode);
                }

                category.SubCategories.Clear();
                category.SubCategories.AddRange(childCategories.OrderBy(x => x.SortOrder));

                DebugService.WriteLine(string.Format("Downloaded category (#{0}) '{1}' {2} in {3} seconds.",
                                                     category.Id, category.Name, "child categories", (DateTime.UtcNow - childStart).TotalSeconds));
            }

            if (preloadGrandchildren)
            {
                childStart = DateTime.UtcNow;

                foreach (AppCategory c in category.SubCategories)
                {
                    List<AppArticle> grandChildArticles = await GetArticles(c.Id);

                    if (grandChildArticles != null && grandChildArticles.Count > 0)
                    {
                        List<AppArticle> grandChildArticles_Text = new List<AppArticle>();
                        List<AppArticle> grandChildArticles_Other = new List<AppArticle>();

                        foreach (AppArticle a in grandChildArticles)
                        {
                            if (preloadGrandchildArticleTexts && a.GetArticleType() == AppArticleType.Text)
                                await a.PreloadFileContent(siteCode);

                            if (a.GetArticleType() == AppArticleType.Text || a.GetArticleType() == AppArticleType.DirectText)
                                grandChildArticles_Text.Add(a);
                            else
                                grandChildArticles_Other.Add(a);
                        }

                        c.Articles.Clear();
                        c.Articles.AddRange(grandChildArticles_Other.OrderBy(x => x.SortOrder));
                        c.Articles.AddRange(grandChildArticles_Text.OrderBy(x => x.SortOrder));
                    }

                    List<AppCategory> grandChildCategories = await GetSubCategories(c.Id);

                    if (grandChildCategories != null && grandChildCategories.Count > 0)
                    {
                        c.SubCategories.Clear();
                        c.SubCategories.AddRange(grandChildCategories.OrderBy(x => x.SortOrder));
                    }
                }

                DebugService.WriteLine(string.Format("Downloaded category (#{0}) '{1}' {2} in {3} seconds.",
                                                         category.Id, category.Name, "grand child articles & categories", (DateTime.UtcNow - childStart).TotalSeconds));
            }

            DebugService.WriteLine(string.Format("Downloaded category (#{0}) '{1}' in {2} seconds.",
                                                         category.Id, category.Name, (DateTime.UtcNow - start).TotalSeconds));

            return category;
        }

        public static async Task<bool> LoadFileContent (string requestUri, string fileName, string folderName, string modifiedBy = "", string modifiedDate = "", Action<AppFileContent, DateTime> onCompleteDownload = null)
        {
            if (string.IsNullOrEmpty(requestUri))
                return false;

            if (string.IsNullOrEmpty(fileName))
                return false;

            if (string.IsNullOrEmpty(folderName))
                return false;

            bool success = false;
            DateTime fileLastWriteTime = StorageService.DateTimeMinValue;
            DateTime serverLastWriteTime = StorageService.DateTimeMinValue;

            if (!string.IsNullOrEmpty(modifiedDate))
                serverLastWriteTime = StorageService.ParseExactDateTime(modifiedDate);

            if (!AlwaysRedownloadFileContent &&
                await StorageService.DoesFileExist(fileName, DataService.SiteCode, folderName))
            {
                fileLastWriteTime = await StorageService.GetLastWriteTime(fileName, DataService.SiteCode, folderName);

                if (fileLastWriteTime >= serverLastWriteTime)
                    success = true;
            }

            if (!success)
            {
                AppFileContent fileContent = await GetFileContent(requestUri, fileName, modifiedDate);

                if (fileContent != null && string.IsNullOrEmpty(fileContent.ErrorMessage) && fileContent.Contents != null && fileContent.Contents.Count > 0)
                {
                    success = true;

                    if (onCompleteDownload != null)
                        onCompleteDownload(fileContent, serverLastWriteTime);
                }
            }

            return success;
        }
        #endregion

        #region Methods
        public static void Initialize ()
        {
            ServerConfig.TryInitialize();
            UserCredentials.TryInitialize(); 
            ServerCredentials.TryInitialize();
            BrowserCredentials.TryInitialize();

            DebugService.WriteLine(string.Format("Loaded .serverconfig: \nServerURL: {0}\nApiKey: {1}\nApiAccess: {2}\nImplementation: {3}",
                                                 ServerConfig.ServerURL, ServerConfig.ApiKey, ServerConfig.ApiAccess, ServerConfig.Implementation));

            DebugService.WriteLine(string.Format("Loaded .usercredconfig: \nUsername: {0}\nPassword: {1}\nPin Code: {2}",
                                                 UserCredentials.Username, UserCredentials.Password, UserCredentials.PinCode));

            DebugService.WriteLine(string.Format("Loaded .servercredconfig: \nUsername: {0}\nPassword: {1}",
                                                 ServerCredentials.Username, ServerCredentials.Password));

            DebugService.WriteLine(string.Format("Loaded .browsercredconfig: \nDomain: {0}\nUsername: {1}\nPassword: {2}",
                                                 BrowserCredentials.Domain, BrowserCredentials.Username, BrowserCredentials.Password));

            LoadServerConfig();
        }

        public static object LoadAppProperty (string key, object defaultValue)
        {
            if (Xamarin.Forms.Application.Current.Properties.ContainsKey(key))
                return Xamarin.Forms.Application.Current.Properties[key];
            else
                return defaultValue;
        }

        public static async Task SaveAppProperty (Tuple<string, object>[] properies)
        {
            bool isDirty = false;

            foreach (Tuple<string, object> p in properies)
            {
                if (!p.Item1.IsNullOrEmpty())
                {
                    Xamarin.Forms.Application.Current.Properties[p.Item1] = p.Item2;
                    isDirty = true;
                }
            }

            if (isDirty)
                await Xamarin.Forms.Application.Current.SavePropertiesAsync();
        }

        public static string GetMasterDetailPageUriString (string pageName)
        {
            return string.Format("POMasterDetailPage?detailpagename={0}/", pageName);
        }
        #endregion
    }
}
