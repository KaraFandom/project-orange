﻿using ProjectOrange.CustomControls;
using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using ProjectOrange.Services;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class UploadPage : ContentPage
    {

        private const string IS_LOADING = "IsLoading";

        public UploadPage ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Initialization
            double editorHeight = 300;
            double entryFontSizeSmall = Device.GetNamedSize(NamedSize.Small, typeof(Entry));
            double gridFileDataHeight = Device.OnPlatform(30, 40, 40);
            double buttonFileDataFontSize = Device.OnPlatform(Device.GetNamedSize(NamedSize.Default, typeof(Button)), Device.GetNamedSize(NamedSize.Micro, typeof(Button)), Device.GetNamedSize(NamedSize.Default, typeof(Button)));
            double labelFontSizeSmall = Device.GetNamedSize(NamedSize.Small, typeof(Label));
            string imagePathBackground = AppTheme.BackgroundDefault;
            Color entryTextColor = Device.OnPlatform(Color.Black, Color.White, Color.White);
            Color buttonColor = Color.FromHex(AppTheme.ButtonColorDefault);
            Color separatorColor = Color.FromHex(AppTheme.SeperatorColorDefault);
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);

            if (DataService.Theme != null)
            {
                if (DataService.Theme.BackgroundImage != null)
                    imagePathBackground = DataService.Theme.BackgroundImage.GetFileContentPath();

                buttonColor = DataService.Theme.ButtonColorValue;

                separatorColor = DataService.Theme.SeperatorColorValue;

                textColor = DataService.Theme.TextColorValue;
            }

            RelativeLayout main = new RelativeLayout ()
            {

            };

            Image background = new Image ()
            {
                Source = imagePathBackground,
                Aspect = Aspect.AspectFill
            };
            main.Children.Add(background,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0
            };
            content.SetBinding(StackLayout.IsVisibleProperty, 
                new Binding(IS_LOADING) {
                    Converter = new NegateBooleanConverter()
                });
            main.Children.Add(content,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));

            PONavBar navBar = new PONavBar ()
            {
                Title = "Create Article",
                UseBanner = false,
                UseButtonRight = false
            };
            navBar.Redraw();
            navBar.ButtonLeft.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            content.Children.Add(navBar);

            Label labelWarnings = new Label ()
            {
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                HeightRequest = 30,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,

                TextColor = textColor
            };
            labelWarnings.SetBinding(Label.IsVisibleProperty, "IsWarningsVisible");
            labelWarnings.SetBinding(Label.TextProperty, "WarningsText");
            labelWarnings.SetBinding(Label.BackgroundColorProperty, "WarningsColor");
            content.Children.Add(labelWarnings);

            ScrollView scrollContent = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            content.Children.Add(scrollContent);

            StackLayout scrollBody = new StackLayout ()
            {
                Padding = new Thickness(15, 15)
            };
            scrollContent.Content = scrollBody;

            Label labelTitle = new Label ()
            {
                Text = "Title",

                TextColor = textColor,
                FontSize = labelFontSizeSmall
            };
            scrollBody.Children.Add(labelTitle);

            Entry entryTitle = new Entry ()
            {
                TextColor = entryTextColor,
                FontSize = entryFontSizeSmall
            };
            entryTitle.SetBinding(Entry.TextProperty, "ArticleTitle");
            scrollBody.Children.Add(entryTitle);

            Label labelType = new Label ()
            {
                Text = "Type",

                TextColor = textColor,
                FontSize = labelFontSizeSmall
            };
            scrollBody.Children.Add(labelType);

            Picker pickerType = new Picker ()
            {
                Title = "Type",
                TextColor = entryTextColor
            };
            pickerType.Items.Add("Text");
            pickerType.Items.Add("Text (Direct)");
            pickerType.Items.Add("Link");
            pickerType.Items.Add("Link (Browser)");
            pickerType.Items.Add("Link (Auto-Fill)");
            pickerType.Items.Add("Link (Credentials)");
            pickerType.Items.Add("Link (Login)");
            pickerType.Items.Add("File");
            pickerType.SelectedIndex = 0;
            pickerType.SetBinding(Picker.SelectedIndexProperty, "ArticleTypeIndex");
            scrollBody.Children.Add(pickerType);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            scrollBody.Children.Add(new HorizontalSeparator { Color = separatorColor });

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Label labelText = new Label ()
            {
                Text = "Text",

                TextColor = textColor,
                FontSize = labelFontSizeSmall
            };
            labelText.SetBinding(Label.IsEnabledProperty, "IsSettingText");
            labelText.SetBinding(Label.IsVisibleProperty, "IsSettingText");
            scrollBody.Children.Add(labelText);

            Editor editorText = new Editor ()
            {
                HeightRequest = editorHeight
            };
            editorText.SetBinding(Editor.IsEnabledProperty, "IsSettingText");
            editorText.SetBinding(Editor.IsVisibleProperty, "IsSettingText");
            editorText.SetBinding(Editor.TextProperty, "ArticleText");
            scrollBody.Children.Add(editorText);

            Label labelUrl = new Label ()
            {
                Text = "URL",

                TextColor = textColor,
                FontSize = labelFontSizeSmall
            };
            labelUrl.SetBinding(Label.IsEnabledProperty, "IsSettingUrl");
            labelUrl.SetBinding(Label.IsVisibleProperty, "IsSettingUrl");
            scrollBody.Children.Add(labelUrl);

            Entry entryUrl = new Entry ()
            {
                TextColor = entryTextColor,
                FontSize = entryFontSizeSmall
            };
            entryUrl.SetBinding(Entry.IsEnabledProperty, "IsSettingUrl");
            entryUrl.SetBinding(Entry.IsVisibleProperty, "IsSettingUrl");
            entryUrl.SetBinding(Entry.TextProperty, "ArticleUrl");
            scrollBody.Children.Add(entryUrl);

            Label labelFileName = new Label ()
            {
                Text = "File Name",

                TextColor = textColor,
                FontSize = labelFontSizeSmall
            };
            labelFileName.SetBinding(Label.IsEnabledProperty, "IsSettingFile");
            labelFileName.SetBinding(Label.IsVisibleProperty, "IsSettingFile");
            scrollBody.Children.Add(labelFileName);

            Entry entryFileName = new Entry ()
            {
                TextColor = entryTextColor,
                FontSize = entryFontSizeSmall
            };
            entryFileName.SetBinding(Entry.IsEnabledProperty, "IsSettingFile");
            entryFileName.SetBinding(Entry.IsVisibleProperty, "IsSettingFile");
            entryFileName.SetBinding(Entry.TextProperty, "ArticleFileName");
            scrollBody.Children.Add(entryFileName);

            Label labelFileData = new Label ()
            {
                Text = "File",

                TextColor = textColor,
                FontSize = labelFontSizeSmall
            };
            labelFileData.SetBinding(Label.IsEnabledProperty, "IsSettingFile");
            labelFileData.SetBinding(Label.IsVisibleProperty, "IsSettingFile");
            scrollBody.Children.Add(labelFileData);

            Grid gridFileData = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(4, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(10) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(gridFileDataHeight) }
                }
            };
            gridFileData.SetBinding(Grid.IsEnabledProperty, "IsSettingFile");
            gridFileData.SetBinding(Grid.IsVisibleProperty, "IsSettingFile");
            scrollBody.Children.Add(gridFileData);

            Entry entryFileData = new Entry ()
            {
                IsEnabled = false,
                BackgroundColor = Color.White,

                TextColor = Color.Black,
                FontSize = entryFontSizeSmall
            };
            entryFileData.SetBinding(Entry.TextProperty, "DataFileName");
            gridFileData.Children.Add(entryFileData, 0, 0);

            Button buttonFileData = new Button ()
            {
                Text = "Browse",
                FontSize = buttonFileDataFontSize,

                TextColor = textColor,
                BackgroundColor = buttonColor
            };
            buttonFileData.SetBinding(Button.CommandProperty, "BrowseFileCommand");
            gridFileData.Children.Add(buttonFileData, 2, 0);

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            scrollBody.Children.Add(new HorizontalSeparator { Color = separatorColor });

            scrollBody.Children.Add(new HorizontalSeparator { Color = Color.Transparent, HeightRequest = 5 });

            Button buttonUpload = new Button ()
            {
                Text = "Upload",

                TextColor = textColor,
                BackgroundColor = buttonColor
            };
            buttonUpload.SetBinding(Button.CommandProperty, "AttemptUploadCommand");
            scrollBody.Children.Add(buttonUpload);
            #endregion

            #region Loading
            StackLayout loading = new StackLayout ()
            {
                Spacing = 25,
                Padding = new Thickness(30, 0),
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            loading.SetBinding(StackLayout.IsVisibleProperty, IS_LOADING);
            main.Children.Add(loading,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));

            ActivityIndicator activityIndicator = new ActivityIndicator ()
            {
                Color = Color.White
            };
            activityIndicator.SetBinding(ActivityIndicator.IsRunningProperty, IS_LOADING);
            loading.Children.Add(activityIndicator);

            Label labelActivityIndicator = new Label ()
            {
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                TextColor = Color.White,

                HorizontalTextAlignment = TextAlignment.Center
            };
            labelActivityIndicator.SetBinding(Label.TextProperty, "ActivityText");
            loading.Children.Add(labelActivityIndicator);
            #endregion

            Content = main;
        }
    }
}
