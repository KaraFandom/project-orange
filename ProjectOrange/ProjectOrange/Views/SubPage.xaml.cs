﻿using ProjectOrange.CustomControls;
using ProjectOrange.Models;
using ProjectOrange.Services;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class SubPage : ContentPage
    {
        private delegate void FilterArticles (string filterQuery);
        private delegate Task ScrollDown();
        private delegate Task ScrollUp();

        private double height = 0;
        private double width = 0;
        private bool isScrolledTop = true;
        private bool isScrolling = false;
        private AppThemeViewType viewType = AppThemeViewType.ListView;
        private FilterArticles OnFilterArticles;
        private ScrollDown OnScrollDown;
        private ScrollUp OnScrollUp;

        public static double SHeight = 0;
        public static double SWidth = 0;

        private const string IS_LOADING = "IsLoading";

        public SubPage ()
        {
            InitializeComponent();

            Initialize();
        }

        protected override void OnSizeAllocated (double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (this.width != width || this.height != height)
            {
                this.width = width;
                this.height = height;

                SWidth = width;
                SHeight = height;

                Redraw();
            }
        }

        private void Initialize ()
        {
            if (DataService.Theme != null)
                viewType = DataService.Theme.GetSubViewType();
        }

        private void Redraw ()
        {
            #region Initialization
            isScrolledTop = true;
            string imagePathBackground = AppTheme.BackgroundDefault;
            string imagePathScrollBottom = AppTheme.ScrollBottomDefault;
            string imagePathScrollTop = AppTheme.ScrollTopDefault;

            if (DataService.Theme != null)
            {
                if (DataService.Theme.BackgroundImage != null)
                    imagePathBackground = DataService.Theme.BackgroundImage.GetFileContentPath();

                if (DataService.Theme.IconScrollBottomImage != null)
                    imagePathScrollBottom = DataService.Theme.IconScrollBottomImage.GetFileContentPath();

                if (DataService.Theme.IconScrollTopImage != null)
                    imagePathScrollTop = DataService.Theme.IconScrollTopImage.GetFileContentPath();
            }

            RelativeLayout main = new RelativeLayout ()
            {

            };

            Image background = new Image ()
            {
                Source = imagePathBackground,
                Aspect = Aspect.AspectFill
            };
            main.Children.Add(background,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0
            };
            content.SetBinding(StackLayout.BackgroundColorProperty, "Category.CategoryViewColor");
            content.SetBinding(StackLayout.IsVisibleProperty,
                new Binding(IS_LOADING) {
                    Converter = new NegateBooleanConverter()
                });
            main.Children.Add(content,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));

            PONavSearchBar navSearchBar = new PONavSearchBar ()
            {

            };
            navSearchBar.ButtonHamburger.SetBinding(Button.CommandProperty, "ShowMasterDetailCommand");
            content.Children.Add(navSearchBar);

            switch (viewType)
            {
                default:
                case AppThemeViewType.ListView:
                    POCategoryView listView = new POCategoryView ()
                    {
                        Category = new AppCategory(),
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    listView.SetBinding(POCategoryView.CategoryProperty, "Category");
                    OnFilterArticles = (filterQuery) => { listView.FilterQuery = filterQuery; };
                    OnScrollDown = async () => { await listView.ScrollView.ScrollToAsync(0, listView.ScrollView.Height, true); };
                    OnScrollUp = async () => { await listView.ScrollView.ScrollToAsync(0, 0, true); };
                    content.Children.Add(listView);
                    break;

                case AppThemeViewType.GridView:
                    POCategoryView2 gridView = new POCategoryView2 ()
                    {
                        Category = new AppCategory(),
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    gridView.SetBinding(POCategoryView2.CategoryProperty, "Category");
                    OnFilterArticles = (filterQuery) => { gridView.FilterQuery = filterQuery; };
                    OnScrollDown = async () => { await gridView.ScrollView.ScrollToAsync(0, gridView.ScrollView.Height, true); };
                    OnScrollUp = async () => { await gridView.ScrollView.ScrollToAsync(0, 0, true); };
                    content.Children.Add(gridView);
                    break;

                case AppThemeViewType.WindowsView:
                    POCategoryView1 windowsView = new POCategoryView1 ()
                    {
                        Category = new AppCategory(),
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };
                    windowsView.SetBinding(POCategoryView1.CategoryProperty, "Category");
                    OnFilterArticles = (filterQuery) => { windowsView.FilterQuery = filterQuery; };
                    OnScrollDown = async () => { await windowsView.ScrollView.ScrollToAsync(0, windowsView.ScrollView.Height, true); };
                    OnScrollUp = async () => { await windowsView.ScrollView.ScrollToAsync(0, 0, true); };
                    content.Children.Add(windowsView);
                    break;
            }

            POToolbox toolbox = new POToolbox ()
            {
                VerticalOptions = LayoutOptions.End
            };
            toolbox.Button1.SetBinding(Button.CommandProperty, "NavigateToHomePageCommand");
            toolbox.Button2.SetBinding(Button.CommandProperty, "RefreshCommand");
            toolbox.Button4.SetBinding(Button.CommandProperty, "MailCommand");
            content.Children.Add(toolbox);
            #endregion

            #region Loading
            StackLayout loading = new StackLayout ()
            {
                Spacing = 25,
                Padding = new Thickness(30, 0),
                VerticalOptions = LayoutOptions.CenterAndExpand
            };
            loading.SetBinding(StackLayout.IsVisibleProperty, IS_LOADING);
            main.Children.Add(loading,
                              widthConstraint: Constraint.RelativeToParent((p) => { return p.Width; }),
                              heightConstraint: Constraint.RelativeToParent((p) => { return p.Height; }));

            ActivityIndicator activityIndicator = new ActivityIndicator ()
            {
                Color = Color.White
            };
            activityIndicator.SetBinding(ActivityIndicator.IsRunningProperty, IS_LOADING);
            loading.Children.Add(activityIndicator);

            Label labelActivityIndicator = new Label ()
            {
                FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                TextColor = Color.White,

                HorizontalTextAlignment = TextAlignment.Center
            };
            labelActivityIndicator.SetBinding(Label.TextProperty, "ActivityText");
            loading.Children.Add(labelActivityIndicator);
            #endregion

            #region Events
            navSearchBar.SearchBar.TextChanged += (s, e) =>
            {
                if (OnFilterArticles != null)
                    OnFilterArticles(navSearchBar.SearchBar.Text);
            };

            if (DataService.AllowViewTypeToggling)
            {
                navSearchBar.ButtonBanner.Clicked += (s, e) =>
                {
                    if (viewType == AppThemeViewType.ListView)
                        viewType = AppThemeViewType.GridView;
                    else if (viewType == AppThemeViewType.GridView)
                        viewType = AppThemeViewType.WindowsView;
                    else if (viewType == AppThemeViewType.WindowsView)
                        viewType = AppThemeViewType.ListView;

                    Redraw();
                };
            }

            toolbox.Button3.Clicked += async (s, e) =>
            {
                if (!isScrolling)
                {
                    isScrolling = true;

                    if (isScrolledTop)
                    {
                        if (OnScrollDown != null)
                            await OnScrollDown.Invoke();

                        toolbox.Image3.Source = imagePathScrollTop;
                        isScrolledTop = false;
                    }
                    else
                    {
                        if (OnScrollUp != null)
                            await OnScrollUp.Invoke();

                        toolbox.Image3.Source = imagePathScrollBottom;
                        isScrolledTop = true;
                    }

                    isScrolling = false;
                }
            };
            #endregion

            Content = main;
        }
    }
}
