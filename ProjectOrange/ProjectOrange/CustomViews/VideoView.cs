﻿using System;
using Xamarin.Forms;

namespace ProjectOrange.CustomViews
{
    public class VideoView : View
    {
        public static readonly BindableProperty FileSourceProperty = BindableProperty.Create(nameof(FileSource),
                                                                                             typeof(string),
                                                                                             typeof(VideoView),
                                                                                             default(string),
                                                                                             BindingMode.TwoWay,
                                                                                             null,
                                                                                             (bindable, oldValue, newValue) =>
                                                                                             {
                                                                                                 VideoView vw = (VideoView) bindable;
                                                                                             });

        public string FileSource
        {
            get { return (string) GetValue(FileSourceProperty); }
            set { SetValue(FileSourceProperty, value); }
        }

        public Action OnStop;

        public void Stop ()
        {
            if (OnStop != null)
                OnStop();
        }
    }
}
