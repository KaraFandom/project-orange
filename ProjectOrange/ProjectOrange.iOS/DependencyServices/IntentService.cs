using Foundation;
using ProjectOrange.iOS.DependencyServices;
using ProjectOrange.Services;
using ProjectOrange.Services.Interfaces;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(IntentService))]
namespace ProjectOrange.iOS.DependencyServices
{
    public class IntentService : IIntentService
    {
        public async void Open (string fileName)
        {
            byte[] file = null;
            NSData nsfile = null;

            file = await StorageService.LoadFromCache(fileName, DataService.SiteCode, StorageService.AttachmentsFolderName);
            nsfile = NSData.FromArray(file);

            if (nsfile != null)
            {
                NSObject nsobject = NSObject.FromObject(nsfile);
                UIActivityViewController controller = new UIActivityViewController(new NSObject[] { nsobject }, null);

                Application.GetActiveViewController().PresentViewController(controller, true, null);
            }
        }
    }
}