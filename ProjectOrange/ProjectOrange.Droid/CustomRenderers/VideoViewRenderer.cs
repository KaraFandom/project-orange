using Android.Graphics;
using Android.Media;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ProjectOrange.Droid.CustomRenderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ProjectOrange.CustomViews.VideoView), typeof(VideoViewRenderer))]
namespace ProjectOrange.Droid.CustomRenderers
{
    public class VideoViewRenderer : ViewRenderer<ProjectOrange.CustomViews.VideoView, Android.Widget.VideoView>, ISurfaceHolderCallback, MediaController.IMediaPlayerControl, MediaPlayer.IOnPreparedListener
    {
        Android.Widget.VideoView videoView;
        MediaPlayer player;
        MediaController mediaController;
        ProjectOrange.CustomViews.VideoView mVideoView;

        #region ISurfaceHolderCallback
        public void SurfaceChanged (ISurfaceHolder holder, [GeneratedEnum] Format format, int width, int height) { }

        public void SurfaceCreated (ISurfaceHolder holder)
        {
            if (player != null)
                player.SetDisplay(holder);
        }

        public void SurfaceDestroyed (ISurfaceHolder holder) { }
        #endregion

        #region IMediaPlayerControl
        public int AudioSessionId { get { return 0; } }

        public int BufferPercentage { get { return 0; } }

        public int CurrentPosition { get { if (player != null) return player.CurrentPosition; else return 0; } }

        public int Duration { get { if (player != null) return player.Duration; else return 0; } }

        public bool IsPlaying { get { if (player != null) return player.IsPlaying; else return false; } }

        public bool CanPause () { return true; }

        public bool CanSeekBackward () { return true; }

        public bool CanSeekForward () { return true; }

        public void Pause () { if (player != null) player.Pause(); }

        public void SeekTo (int pos) { if (player != null) player.SeekTo(pos); }

        public void Start () { if (player != null) player.Start(); }
        #endregion

        #region IOnPreparedListener
        public void OnPrepared (MediaPlayer mp)
        {
            if (mediaController != null)
            {
                mediaController.SetMediaPlayer(this);
                mediaController.SetAnchorView(videoView);
            }
        }
        #endregion

        public VideoViewRenderer ()
        {

        }

        protected override void OnElementChanged (ElementChangedEventArgs<ProjectOrange.CustomViews.VideoView> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
                mVideoView = e.NewElement as ProjectOrange.CustomViews.VideoView;

            if (mVideoView != null && !string.IsNullOrEmpty(mVideoView.FileSource))
            {
                mVideoView.OnStop = () =>
                {
                    mediaController.Hide();
                    player.Stop();
                    player.Release();
                    Control.StopPlayback();
                };

                videoView = new Android.Widget.VideoView(Context);
                base.SetNativeControl(videoView);
                Control.Holder.AddCallback(this);
                player = new MediaPlayer();
                player.SetOnPreparedListener(this);
                mediaController = new MediaController(Context);

                Play(mVideoView.FileSource);
            }
        }

        protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == ProjectOrange.CustomViews.VideoView.FileSourceProperty.PropertyName)
            {
                if (mVideoView != null && !string.IsNullOrEmpty(mVideoView.FileSource))
                {
                    mVideoView.OnStop = () =>
                    {
                        mediaController.Hide();
                        player.Stop();
                        player.Release();
                        Control.StopPlayback();
                    };

                    videoView = new Android.Widget.VideoView(Context);
                    base.SetNativeControl(videoView);
                    Control.Holder.AddCallback(this);
                    player = new MediaPlayer();
                    player.SetOnPreparedListener(this);
                    mediaController = new MediaController(Context);

                    Play(mVideoView.FileSource);
                }
            }
        }

        public override bool OnTouchEvent (MotionEvent e)
        {
            if (mediaController != null)
                mediaController.Show();

            return false;
        }

        private void Play (string fullPath)
        {
            if (player != null)
            {
                player.SetDataSource(fullPath);
                player.Prepare();
                player.Start();
                Control.Layout(0, 200, player.VideoHeight, player.VideoWidth);
                Control.SetZOrderMediaOverlay(true);
            }
        }
    }
}