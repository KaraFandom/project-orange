﻿using ProjectOrange.CustomControls;
using ProjectOrange.CustomViews;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class VideoPage : ContentPage
    {
        VideoView videoView;

        public VideoPage ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Content
            StackLayout main = new StackLayout ()
            {
                BackgroundColor = Color.Black,
                Spacing = 0
            };

            PONavBar navBar = new PONavBar ()
            {
                UseBanner = true,
                UseButtonRight = true
            };
            navBar.Redraw();
            navBar.ButtonLeft.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            navBar.ButtonRight.SetBinding(Button.CommandProperty, "MailCommand");
            main.Children.Add(navBar);

            videoView = new VideoView ()
            {
                HeightRequest = 100,
                WidthRequest = 100,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            videoView.SetBinding(VideoView.FileSourceProperty, "Source");
            main.Children.Add(videoView);
            #endregion

            Content = main;
        }

        protected override void OnDisappearing ()
        {
            base.OnDisappearing();

            if (videoView != null)
            {
                videoView.Stop();
                videoView.FileSource = string.Empty;
            }
        }
    }
}
