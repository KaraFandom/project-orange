﻿using ProjectOrange.CustomControls;
using ProjectOrange.CustomViews;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class TestPage : ContentPage
    {
        public TestPage()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Content
            StackLayout main = new StackLayout ()
            {
                BackgroundColor = Color.Black,
                Spacing = 0
            };

            PONavBar navBar = new PONavBar ()
            {

            };
            main.Children.Add(navBar);

            RoundedFrame roundedFrame1 = new RoundedFrame ()
            {
                CornerRadius = 100.0f,

                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            main.Children.Add(roundedFrame1);

            StackLayout newStack = new StackLayout ()
            {
                BackgroundColor = Color.Red,

                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            roundedFrame1.Content = newStack;

            Label labelNewStack = new Label ()
            {
                Text = "Title",
                TextColor = Color.White
            };
            newStack.Children.Add(labelNewStack);
            #endregion

            Content = main;
        }
    }
}
