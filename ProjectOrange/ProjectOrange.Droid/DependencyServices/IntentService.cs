using Android.Content;
using ProjectOrange.Droid.DependencyServices;
using ProjectOrange.Services;
using ProjectOrange.Services.Interfaces;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(IntentService))]
namespace ProjectOrange.Droid.DependencyServices
{
    public class IntentService : IIntentService
    {
        public async void Open (string fileName)
        {
            Intent intent = new Intent(Intent.ActionView);

            //string filePath = Path.Combine(Forms.Context.ExternalCacheDir.AbsolutePath, fileName);
            string filePath = StorageService.GetAttachmentPath(DataService.SiteCode, fileName);
            byte[] file = null;
            Java.IO.File jfile = null;

            file = await StorageService.LoadFromCache(fileName, DataService.SiteCode, StorageService.AttachmentsFolderName);

            if (file != null)
            {
                File.WriteAllBytes(filePath, file);

                jfile = new Java.IO.File(filePath);
                jfile.SetReadable(true);

                if (jfile.CanWrite())
                {
                    string extension = Android.Webkit.MimeTypeMap.GetFileExtensionFromUrl(fileName);
                    string mimeType = Android.Webkit.MimeTypeMap.Singleton.GetMimeTypeFromExtension(extension);

                    if (string.IsNullOrEmpty(extension) || string.IsNullOrEmpty(mimeType))
                        intent.SetDataAndType(Android.Net.Uri.FromFile(jfile), "text/*");
                    else
                        intent.SetDataAndType(Android.Net.Uri.FromFile(jfile), mimeType);

                    Forms.Context.StartActivity(intent);
                }
            }
        }
    }
}