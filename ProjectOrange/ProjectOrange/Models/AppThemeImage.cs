﻿using ProjectOrange.Services;
using System;
using System.Threading.Tasks;

namespace ProjectOrange.Models
{
    public class AppThemeImage
    {
        public string FileName { get; set; }
        public string Id { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string FileType { get; set; }
        public string Contents { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public bool FileContentCached { get; set; }
        public AppFileContent FileContent { get; set; }

        public string FileContentDefaultPath { get; set; }

        public AppThemeImage ()
        {
            FileName = string.Empty;
            Id = string.Empty;
            ModifiedBy = string.Empty;
            ModifiedDate = string.Empty;
            FileType = string.Empty;
            Contents = string.Empty;
            CreatedDate = string.Empty;
            CreatedBy = string.Empty;

            FileContentCached = false;
            FileContent = new AppFileContent();
        }

        public string GetFileContentPath ()
        {
            string path = FileContentDefaultPath;

            if (string.IsNullOrEmpty(FileName))
                return path;

            if (FileContentCached)
                path = StorageService.GetThemeImagePath(DataService.SiteCode, FileName);

            return path;
        }

        public async Task<bool> DownloadImage (string siteCode)
        {
            if (string.IsNullOrEmpty(FileName))
                return false;

            string requestUri = (DataService.DBService is DatabaseService_Sharepoint) ? FileName :
                                (DataService.DBService is DatabaseService_SQL) ? Contents :
                                string.Empty;

            if (string.IsNullOrEmpty(requestUri))
                return false;

            string themeFolderName = StorageService.ThemeFolderName;
            DateTime lastWriteTime = StorageService.DateTimeMinValue;
            FileContentCached = await DataService.LoadFileContent(requestUri, FileName, themeFolderName, ModifiedBy, ModifiedDate,
                                                                  (fc, dt) => { FileContent = fc; lastWriteTime = dt; });

            if (FileContentCached)
                await FileContent.WriteToFile(lastWriteTime, DataService.SiteCode, themeFolderName);

            return FileContentCached;
        }
    }
}
