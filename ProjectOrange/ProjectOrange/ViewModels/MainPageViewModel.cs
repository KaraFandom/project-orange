﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using ProjectOrange.Models;
using ProjectOrange.Services;
using ProjectOrange.Services.Interfaces;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ProjectOrange.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {
        private IEventAggregator _eventAggregator;

        private INavigationService _navigationService;

        public DelegateCommand ShowMasterDetailCommand { get; set; }

        public DelegateCommand RefreshCommand { get; set; }

        public DelegateCommand MailCommand { get; set; }

        public DelegateCommand<AppCategory> NavigateToSubPageCommand { get; set; }

        private bool _isLoading = false;
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }

        private string _activityText;
        public string ActivityText
        {
            get { return _activityText; }
            set { SetProperty(ref _activityText, value); }
        }

        private List<AppCategory> _categories;
        public List<AppCategory> Categories
        {
            get { return _categories; }
            set { SetProperty(ref _categories, value); }
        }

        public MainPageViewModel (IEventAggregator eventAggregator, INavigationService navigationService)
        {
            _eventAggregator = eventAggregator;
            _navigationService = navigationService;

            ShowMasterDetailCommand = new DelegateCommand(ShowMasterDetail);
            RefreshCommand = new DelegateCommand(Refresh);
            MailCommand = new DelegateCommand(Mail);
            NavigateToSubPageCommand = new DelegateCommand<AppCategory>(NavigateToSubPage);

            Initialize();
        }

        public void OnNavigatedFrom (NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo (NavigationParameters parameters)
        {

        }

        private void Initialize ()
        {
            GetCategories();
        }

        private void ShowMasterDetail ()
        {
            string pageName = "MainPage";
            _eventAggregator.GetEvent<Events.TogglePOMasterDetailPageEvent>().Publish(new Events.TogglePOMasterDetailPagePayload { DetailPageName = pageName, IsPresented = true });
        }

        private void Refresh ()
        {
            GetCategories();
        }

        private void Mail ()
        {
            IMailService mailService = DependencyService.Get<IMailService>();

            if (mailService != null)
                mailService.Compose(null, null, null);
        }

        private async void NavigateToSubPage (AppCategory category)
        {
            if (IsLoading)
                return;

            if (category == null)
                return;

            IsLoading = true;

            ActivityText = "Syncing Data";

            category = await DataService.LoadCategory(category);

            IsLoading = false;

            string pageName = "SubPage";
            await _navigationService.NavigateAsync(DataService.GetMasterDetailPageUriString(pageName) + "/" + pageName, new NavigationParameters { { "category", category } });
        }

        private async void GetCategories ()
        {
            if (IsLoading)
                return;

            IsLoading = true;

            ActivityText = "Syncing Data";

            Categories = await DataService.LoadCategories((s) => { ActivityText = s; });

            IsLoading = false;
        }
    }
}
