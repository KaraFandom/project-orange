﻿using Xamarin.Forms;

namespace ProjectOrange.Services
{
    public static class UtilService
    {
        public static Color SafeFromHex (string hex, Color defaultValue)
        {
            try
            {
                if (string.IsNullOrEmpty(hex))
                    return defaultValue;

                Color color = Color.FromHex(hex);

                if (color != null)
                    return color;
                else
                    return defaultValue;
            }
            catch { return defaultValue; }
        }

        public static bool IsNullOrEmpty (this string value)
        {
            return string.IsNullOrEmpty(value);
        }
    }
}
