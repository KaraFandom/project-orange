﻿using ProjectOrange.Effects;
using ProjectOrange.iOS.CustomRenderers;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("FandomInc")]
[assembly: ExportEffect(typeof(LabelShadowEffectRenderer), "LabelShadowEffect")]
namespace ProjectOrange.iOS.CustomRenderers
{
    public class LabelShadowEffectRenderer : PlatformEffect
    {
        protected override void OnAttached ()
        {
            try
            {
                LabelShadowEffect effect = (LabelShadowEffect) Element.Effects.FirstOrDefault(e => e is LabelShadowEffect);

                if (effect != null)
                {
                    Control.Layer.CornerRadius = effect.Radius;
                    Control.Layer.ShadowColor = effect.Color.ToCGColor();
                    Control.Layer.ShadowOffset = new CoreGraphics.CGSize(effect.DistanceX, effect.DistanceY);
                    Control.Layer.ShadowOpacity = 1.0f;
                }
            }
            catch (Exception ex) { }
        }

        protected override void OnDetached () { }
    }
}