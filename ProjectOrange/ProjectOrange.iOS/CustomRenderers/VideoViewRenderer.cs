using CoreGraphics;
using Foundation;
using MediaPlayer;
using ProjectOrange.CustomViews;
using ProjectOrange.iOS.CustomRenderers;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(VideoView), typeof(VideoViewRenderer))]
namespace ProjectOrange.iOS.CustomRenderers
{
    public class VideoViewRenderer : ViewRenderer<VideoView, UIVideoView>
    {
        VideoView videoView;

        protected override void OnElementChanged (ElementChangedEventArgs<VideoView> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
                videoView = e.NewElement as VideoView;

            Redraw();
        }

        protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == VideoView.FileSourceProperty.PropertyName)
                Redraw();
        }

        private void Redraw ()
        {
            if (videoView != null && !string.IsNullOrEmpty(videoView.FileSource))
            {
                videoView.OnStop = () =>
                {
                    this.Control.Stop();
                };

                base.SetNativeControl(new UIVideoView(videoView.FileSource, UIScreen.MainScreen.Bounds));
            }
        }
    }

    public class UIVideoView : UIView
    {
        bool isPlaying = false;
        MPMoviePlayerController moviePlayer;

        public UIVideoView (string fileSource, CGRect bounds)
        {
            this.AutoresizingMask = UIViewAutoresizing.All;
            this.ContentMode = UIViewContentMode.ScaleToFill;
            this.Frame = bounds;

            moviePlayer = new MPMoviePlayerController(NSUrl.FromFilename(fileSource));
            moviePlayer.View.AutoresizingMask = UIViewAutoresizing.All;
            moviePlayer.View.ContentMode = UIViewContentMode.ScaleToFill;
            moviePlayer.View.Frame = bounds;

            moviePlayer.RepeatMode = MPMovieRepeatMode.None;
            moviePlayer.ControlStyle = MPMovieControlStyle.Embedded;
            moviePlayer.ScalingMode = MPMovieScalingMode.AspectFit;
            moviePlayer.SetFullscreen(true, true);

            this.Add(moviePlayer.View);

            moviePlayer.Play();
            isPlaying = true;
        }

        public void Stop ()
        {
            if (isPlaying && moviePlayer != null)
            {
                moviePlayer.Stop();
                isPlaying = false;
                moviePlayer = null;
            }
        }
    }
}
