﻿using Prism.Navigation;
using Prism.Unity;
using ProjectOrange.Services;
using ProjectOrange.Views;

namespace ProjectOrange
{
    public partial class App : PrismApplication
    {
        private bool autoLogin = false;
        private string username = "user1";
        private string password = "123456789";

        public App (IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized ()
        {
            InitializeComponent();

            InitializeSettings();
            LoadLandingPage();
        }

        protected override void RegisterTypes ()
        {
            Container.RegisterTypeForNavigation<LoginPage>();

            Container.RegisterTypeForNavigation<POMasterDetailPage>();
            Container.RegisterTypeForNavigation<SidePage>();
            Container.RegisterTypeForNavigation<SettingsPage>();

            Container.RegisterTypeForNavigation<MainPage>();
            Container.RegisterTypeForNavigation<SubPage>();
            Container.RegisterTypeForNavigation<UploadPage>();

            Container.RegisterTypeForNavigation<AuthenticatingWebPage>();
            Container.RegisterTypeForNavigation<ImagePage>();
            Container.RegisterTypeForNavigation<LocalWebPage>();
            Container.RegisterTypeForNavigation<TextPage>();
            Container.RegisterTypeForNavigation<VideoPage>();
            Container.RegisterTypeForNavigation<WebPage>();

            Container.RegisterTypeForNavigation<TestPage>();
        }

        private void InitializeSettings ()
        {
            DataService.Initialize();
        }

        private void LoadLandingPage ()
        {
            if (autoLogin)
                NavigationService.NavigateAsync("LoginPage", new NavigationParameters { { "username", username }, { "password", password } });
            else
                NavigationService.NavigateAsync("LoginPage");
        }
    }
}
