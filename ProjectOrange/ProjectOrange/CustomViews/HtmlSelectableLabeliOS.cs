﻿using Xamarin.Forms;

namespace ProjectOrange.CustomViews
{
    public class HtmlSelectableLabeliOS : View
    {
        public static readonly BindableProperty SourceProperty = BindableProperty.Create(nameof(Source),
                                                                                         typeof(string),
                                                                                         typeof(HtmlSelectableLabeliOS),
                                                                                         string.Empty,
                                                                                         BindingMode.TwoWay,
                                                                                         null,
                                                                                         (bindable, oldValue, newValue) =>
                                                                                         {
                                                                                             HtmlSelectableLabeliOS hsl = (HtmlSelectableLabeliOS) bindable;
                                                                                         });

        public static readonly BindableProperty FontSizeProperty = BindableProperty.Create(nameof(FontSize),
                                                                                           typeof(double),
                                                                                           typeof(HtmlSelectableLabeliOS),
                                                                                           Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                                                                                           BindingMode.TwoWay,
                                                                                           null,
                                                                                           (bindable, oldValue, newValue) =>
                                                                                           {
                                                                                               HtmlSelectableLabeliOS hsl = (HtmlSelectableLabeliOS) bindable;
                                                                                           });

        public static readonly BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor),
                                                                                           typeof(Color),
                                                                                           typeof(HtmlSelectableLabeliOS),
                                                                                           Color.Black,
                                                                                           BindingMode.TwoWay,
                                                                                           null,
                                                                                           (bindable, oldValue, newValue) =>
                                                                                           {
                                                                                               HtmlSelectableLabeliOS hsl = (HtmlSelectableLabeliOS) bindable;
                                                                                           });

        public string Source
        {
            get { return (string) GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public double FontSize
        {
            get { return (double) GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public Color TextColor
        {
            get { return (Color) GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }
    }
}
