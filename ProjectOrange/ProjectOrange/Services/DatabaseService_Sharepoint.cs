﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectOrange.Models;
using ProjectOrange.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectOrange.Services
{
    public class DatabaseService_Sharepoint : IDatabaseService
    {
        private const string NOT_AUTHENTICATED = "Session Expired. Please login and try again.";

        #region Fields
        private bool _isVerified = true;
        private bool _isAuthenticated = false;
        private string _serverURL = "http://5.189.181.56/iShareService/iShareAPI.svc";
        private string _apiKey = "iShareWcfKey";
        private string _apiAccess = "iShareWcfAccess";
        private string _username = string.Empty;
        private string _password = string.Empty;
        private string _storedPassword = string.Empty;
        private string _siteCode = string.Empty;
        #endregion

        #region Properties
        public bool IsVerified
        {
            get { return _isVerified; }
        }

        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
            private set { _isAuthenticated = value; }
        }

        public string ServerURL
        {
            get { return _serverURL; }
        }

        public string ApiKey
        {
            get { return _apiKey; }
        }

        public string ApiAccess
        {
            get { return _apiAccess; }
        }

        public string Username
        {
            get { return _username; }
            private set { _username = value; }
        }

        public string Password
        {
            get { return _password; }
            private set { _password = value; }
        }

        public string StoredPassword
        {
            get { return _storedPassword; }
            private set { _storedPassword = value; }
        }

        public string SiteCode
        {
            get { return _siteCode; }
            private set { _siteCode = value; }
        }

        public AppServerConfigImplementation Implementation
        {
            get { return AppServerConfigImplementation.Sharepoint; }
        }
        #endregion

        #region Constructor
        public DatabaseService_Sharepoint (string serverURL, string apiKey, string apiAccess)
        {
            _serverURL = serverURL;
            _apiKey = apiKey;
            _apiAccess = apiAccess;
            _isVerified = false;
        }

        public DatabaseService_Sharepoint () { }
        #endregion

        #region Methods
        private async Task<string> GetJson (string requestUri, Tuple<string, string>[] customHeaders = null, bool silent = false)
        {
            List<Tuple<string, string>> headers = new List<Tuple<string, string>>();
            headers.Add(new Tuple<string, string>("apiKey", ApiKey));
            headers.Add(new Tuple<string, string>("apiAccess", ApiAccess));

            if (customHeaders != null)
                headers.AddRange(customHeaders);

            return await DataService.GetJson(requestUri, headers.ToArray(), silent);
        }

        public async Task<bool> Validate ()
        {
            return await Validate(ServerURL, ApiKey, ApiAccess);
        }

        public async Task<AppUser> Authenticate (string username, string password, string siteCode = "")
        {
            if (!IsVerified)
            {
                _isVerified = await Validate();
            }

            if (!IsVerified)
                return new AppUser { ErrorMessage = "Failed to verify server." };

            AppUser user = new AppUser();

            if (string.IsNullOrEmpty(username))
                return new AppUser { ErrorMessage = "Username is null or empty." };

            string bypassPassword = "BYPASSPASSWORD";
            string storedPassword = !string.IsNullOrEmpty(password) ? password : bypassPassword;            

            string requestUri = string.Format("{0}/Authenticate/{1}/{2}",
                                              ServerURL, username, storedPassword);

            List<Tuple<string, string>> customHeaders = new List<Tuple<string, string>>();

            if (string.IsNullOrEmpty(password)) customHeaders.Add(new Tuple<string, string>("ByPassPassword", "1"));
            if (!string.IsNullOrEmpty(siteCode)) customHeaders.Add(new Tuple<string, string>("SiteCode", siteCode));

            string json = await GetJson(requestUri, customHeaders.ToArray(), true);

            if (!string.IsNullOrEmpty(json))
            {
                user = JsonConvert.DeserializeObject<AppUser>(json);

                if (!string.IsNullOrEmpty(user.ErrorMessage))
                    return new AppUser { ErrorMessage = user.ErrorMessage };

                IsAuthenticated = true;
                Username = user.UserName;
                Password = password;
                StoredPassword = storedPassword;
                SiteCode = user.SiteCode;
            }

            return user;
        }

        public async Task<AppTheme> GetTheme ()
        {
            AppTheme theme = new AppTheme();

            if (!IsAuthenticated)
                return new AppTheme { ErrorMessage = NOT_AUTHENTICATED };

            string requestUri = string.Format("{0}/GetTheme/{1}",
                                               ServerURL, SiteCode);

            string json = await GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                theme = JsonConvert.DeserializeObject<AppTheme>(json);

                if (!string.IsNullOrEmpty(theme.ErrorMessage))
                    return new AppTheme { ErrorMessage = theme.ErrorMessage };
            }

            return theme;
        }

        public async Task<List<AppCategory>> GetCategories ()
        {
            List<AppCategory> categories = new List<AppCategory>();

            if (!IsAuthenticated)
                return new List<AppCategory>();

            string requestUri = string.Format("{0}/GetCategories/{1}/{2}",
                                               ServerURL, SiteCode, Username);

            string json = await GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                if (json == "[]") return new List<AppCategory>();

                categories = JsonConvert.DeserializeObject<List<AppCategory>>(json);
                categories = categories.Where(x => string.IsNullOrEmpty(x.ErrorMessage)).ToList();

                if (categories == null)
                    return new List<AppCategory>();
            }

            return categories;
        }

        public async Task<List<AppArticle>> GetArticles (string categoryId)
        {
            List<AppArticle> articles = new List<AppArticle>();

            if (!IsAuthenticated)
                return new List<AppArticle>();

            if (string.IsNullOrEmpty(categoryId))
                return new List<AppArticle>();

            string requestUri = string.Format("{0}/GetArticles/{1}/{2}",
                                              ServerURL, SiteCode, categoryId);

            string json = await GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                if (json == "[]") return new List<AppArticle>();

                articles = JsonConvert.DeserializeObject<List<AppArticle>>(json);
                articles = articles.Where(x => string.IsNullOrEmpty(x.ErrorMessage)).ToList();

                if (articles == null)
                    return new List<AppArticle>();
            }

            return articles;
        }

        public async Task<List<AppCategory>> GetSubCategories (string categoryId)
        {
            List<AppCategory> subcategories = new List<AppCategory>();

            if (!IsAuthenticated)
                return new List<AppCategory>();

            if (string.IsNullOrEmpty(categoryId))
                return new List<AppCategory>();

            string requestUri = string.Format("{0}/GetSubCategories/{1}/{2}/{3}",
                                              ServerURL, SiteCode, categoryId, Username);

            string json = await GetJson(requestUri);

            if (!string.IsNullOrEmpty(json))
            {
                if (json == "[]") return new List<AppCategory>();

                subcategories = JsonConvert.DeserializeObject<List<AppCategory>>(json);
                subcategories = subcategories.Where(x => string.IsNullOrEmpty(x.ErrorMessage)).ToList();

                if (subcategories == null)
                    return new List<AppCategory>();
            }

            return subcategories;
        }

        public async Task<AppFileContent> GetFileContent (string requestUri, string fileName, string modifiedDate)
        {
            AppFileContent fileContent = new AppFileContent();

            string fileCode = (!string.IsNullOrEmpty(requestUri)) ? requestUri : fileName;

            if (string.IsNullOrEmpty(fileCode))
                return null;

            string rUri = string.Format("{0}/GetFileContent/{1}/{2}",
                                              ServerURL, SiteCode, fileCode);

            string json = await GetJson(rUri);

            if (!string.IsNullOrEmpty(json))
            {
                fileContent = JsonConvert.DeserializeObject<AppFileContent>(json);

                if (!string.IsNullOrEmpty(fileContent.ErrorMessage))
                    return new AppFileContent { ErrorMessage = fileContent.ErrorMessage };
            }

            return fileContent;
        }

        public async Task<AppPostResponse> PostTextArticle (string categoryId, string fileType, string title, string value, int sortOrder = 10)
        {
            AppPostResponse postResponse = new AppPostResponse();

            if (!IsAuthenticated)
                return new AppPostResponse { ErrorMessage = NOT_AUTHENTICATED };

            if (string.IsNullOrEmpty(categoryId))
                return new AppPostResponse { ErrorMessage = "Category ID is null or empty." };

            if (string.IsNullOrEmpty(fileType))
                return new AppPostResponse { ErrorMessage = "File Type is null or empty." };

            if (string.IsNullOrEmpty(title))
                return new AppPostResponse { ErrorMessage = "Title is null or empty." };

            if (string.IsNullOrEmpty(value))
                return new AppPostResponse { ErrorMessage = "Value is null or empty." };

            JObject jsonObject = new JObject(new JProperty("SiteCode", SiteCode),
                                             new JProperty("CategoryId", categoryId),
                                             new JProperty("FileType", fileType),
                                             new JProperty("Title", title),
                                             new JProperty("TextValue", value),
                                             new JProperty("SortOrder", sortOrder));

            string requestUri = string.Format("{0}/AddTextArticle",
                                              ServerURL);

            string json = await DataService.PostJson(requestUri, jsonObject);

            if (string.IsNullOrEmpty(json))
                return new AppPostResponse { ErrorMessage = "No response." };

            postResponse = JsonConvert.DeserializeObject<AppPostResponse>(json);

            return postResponse;
        }

        public async Task<AppPostResponse> PostFileArticle (string categoryId, string fileType, string title, string fileName, byte[] fileData, int sortOrder = 10)
        {
            AppPostResponse postResponse = new AppPostResponse();

            if (!IsAuthenticated)
                return new AppPostResponse { ErrorMessage = NOT_AUTHENTICATED };

            if (string.IsNullOrEmpty(categoryId))
                return new AppPostResponse { ErrorMessage = "Category ID is null or empty." };

            if (string.IsNullOrEmpty(fileType))
                return new AppPostResponse { ErrorMessage = "File Type is null or empty." };

            if (string.IsNullOrEmpty(title))
                return new AppPostResponse { ErrorMessage = "Title is null or empty." };

            if (string.IsNullOrEmpty(fileName))
                return new AppPostResponse { ErrorMessage = "File Name is null or empty." };

            if (fileData == null || fileData.Length == 0)
                return new AppPostResponse { ErrorMessage = "File Data is null or empty." };

            string fileDataBase64 = Convert.ToBase64String(fileData);

            JObject jsonObject = new JObject(new JProperty("SiteCode", SiteCode),
                                             new JProperty("CategoryId", categoryId),
                                             new JProperty("FileType", fileType),
                                             new JProperty("Title", title),
                                             new JProperty("FileName", fileName),
                                             new JProperty("Base64File", fileDataBase64),
                                             new JProperty("SortOrder", sortOrder));

            string requestUri = string.Format("{0}/AddFileArticle",
                                              ServerURL);

            string json = await DataService.PostJson(requestUri, jsonObject);

            if (string.IsNullOrEmpty(json))
                return new AppPostResponse { ErrorMessage = "No response." };

            postResponse = JsonConvert.DeserializeObject<AppPostResponse>(json);

            return postResponse;
        }
        #endregion

        #region Static Methods
        public static async Task<bool> Validate (string serverURL, string apiKey, string apiAccess)
        {
            if (serverURL.IsNullOrEmpty() || apiKey.IsNullOrEmpty() || apiAccess.IsNullOrEmpty())
                return false;

            string requestUri = string.Format("{0}/Authenticate/{1}/{2}", serverURL, "applebanana", "cherrydate");
            List<Tuple<string, string>> headers = new List<Tuple<string, string>>();
            headers.Add(new Tuple<string, string>("apiKey", apiKey));
            headers.Add(new Tuple<string, string>("apiAccess", apiAccess));

            if (string.IsNullOrEmpty(requestUri))
                return false;

            string json = await DataService.GetJson(requestUri, headers.ToArray(), true);

            if (!json.IsNullOrEmpty())
            {
                JObject jObject = JObject.Parse(json);

                if (jObject != null)
                {
                    JToken jErrorMessage = null;

                    if (jObject.TryGetValue("ErrorMessage", out jErrorMessage))
                        return true;
                }
            }

            return false;
        }
        #endregion
    }
}
