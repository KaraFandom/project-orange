﻿using ProjectOrange.CustomControls;
using ProjectOrange.CustomViews;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class TextPage : ContentPage
    {
        public TextPage()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Initialization
            Color textColor = Color.Black;
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0,

                BackgroundColor = Color.White
            };

            PONavBar navBar = new PONavBar ()
            {
                UseBanner = false,
                UseButtonRight = false
            };
            navBar.Redraw();
            navBar.LabelTitle.SetBinding(Label.TextProperty, "Title");
            navBar.ButtonLeft.SetBinding(Button.CommandProperty, "BackCommand");
            content.Children.Add(navBar);

            content.Children.Add(new HorizontalSeparator { Color = Color.Gray, HeightRequest = 1 });

            ScrollView scrollContent = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            content.Children.Add(scrollContent);

            StackLayout scrollBody = new StackLayout ()
            {
                Padding = new Thickness(15, 15)
            };
            scrollContent.Content = scrollBody;

            switch (Device.OS)
            {
                case TargetPlatform.Android:

                    HtmlSelectableLabelDroid hslDroid = new HtmlSelectableLabelDroid ()
                    {
                        FontSize = Device.GetNamedSize(NamedSize.Small, (typeof(Label))),

                        TextColor = textColor,
                        HeightRequest = 500
                    };
                    hslDroid.SetBinding(HtmlSelectableLabelDroid.TextProperty, "Body");
                    scrollBody.Children.Add(hslDroid);

                    break;

                case TargetPlatform.iOS:

                    HtmlSelectableLabeliOS hsliOS = new HtmlSelectableLabeliOS ()
                    {
                        BackgroundColor = Color.Transparent,
                        FontSize = Device.GetNamedSize(NamedSize.Small, (typeof(Label))),

                        TextColor = textColor,
                        HeightRequest = 500
                    };
                    hsliOS.SetBinding(HtmlSelectableLabeliOS.SourceProperty, "Body");
                    scrollBody.Children.Add(hsliOS);

                    break;
            }
            #endregion

            Content = content;
        }
    }
}
