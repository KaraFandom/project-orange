﻿using ProjectOrange.CustomControls;
using ProjectOrange.CustomViews;
using Xamarin.Forms;

namespace ProjectOrange.Views
{
    public partial class LocalWebPage : ContentPage
    {
        public LocalWebPage ()
        {
            InitializeComponent();

            Redraw();
        }

        private void Redraw ()
        {
            #region Content
            StackLayout main = new StackLayout ()
            {
                BackgroundColor = Color.Black,
                Spacing = 0
            };

            PONavBar navBar = new PONavBar ()
            {
                UseBanner = true,
                UseButtonRight = true
            };
            navBar.Redraw();
            navBar.ButtonLeft.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            navBar.ButtonRight.SetBinding(Button.CommandProperty, "MailCommand");
            main.Children.Add(navBar);

            LocalWebView localWebView = new LocalWebView ()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            localWebView.SetBinding(LocalWebView.UriProperty, "Source");
            main.Children.Add(localWebView);
            #endregion

            Content = main;
        }
    }
}
