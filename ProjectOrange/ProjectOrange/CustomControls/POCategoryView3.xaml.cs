﻿using ProjectOrange.CustomViews;
using ProjectOrange.Models;
using ProjectOrange.Services;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace ProjectOrange.CustomControls
{
    public partial class POCategoryView3 : ContentView
    {
        public static readonly BindableProperty CategoryProperty = BindableProperty.Create(nameof(Category),
                                                                                           typeof(AppCategory),
                                                                                           typeof(POCategoryView2),
                                                                                           new AppCategory(),
                                                                                           BindingMode.TwoWay,
                                                                                           null,
                                                                                           (bindable, oldValue, newValue) =>
                                                                                           {
                                                                                               POCategoryView3 categoryView3 = (POCategoryView3)bindable;
                                                                                               categoryView3.Category = newValue as AppCategory;
                                                                                               categoryView3.Redraw();
                                                                                           });

        private string _filterQuery = string.Empty;
        public string FilterQuery
        {
            get { return _filterQuery; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _filterQuery = string.Empty;
                else
                    _filterQuery = value;

                Redraw();
            }
        }

        private ScrollView _scrollView = new ScrollView();
        public ScrollView ScrollView
        {
            get { return _scrollView; }
        }

        public AppCategory Category
        {
            get { return (AppCategory)GetValue(CategoryProperty); }
            set { SetValue(CategoryProperty, value); }
        }

        public POCategoryView3()
        {
            InitializeComponent();
        }

        private void Redraw ()
        {
            if (Category == null)
                return;

            #region Initialization
            double contentHMargin = 5;
            string imagePathArrowBack = AppTheme.ArrowBackDefault;
            string imagePathArrowCollapse = AppTheme.ArrowCollapseDefault;
            string imagePathArrowExpand = AppTheme.ArrowExpandDefault;
            string imagePathArrowNext = AppTheme.ArrowNextDefault;
            Color textColor = Color.FromHex(AppTheme.TextColorDefault);

            if (DataService.Theme != null)
            {
                if (DataService.Theme.IconArrowBackImage != null)
                    imagePathArrowBack = DataService.Theme.IconArrowBackImage.GetFileContentPath();

                if (DataService.Theme.IconArrowCollapseImage != null)
                    imagePathArrowCollapse = DataService.Theme.IconArrowCollapseImage.GetFileContentPath();

                if (DataService.Theme.IconArrowExpandImage != null)
                    imagePathArrowExpand = DataService.Theme.IconArrowExpandImage.GetFileContentPath();

                if (DataService.Theme.IconArrowNextImage != null)
                    imagePathArrowNext = DataService.Theme.IconArrowNextImage.GetFileContentPath();

                textColor = DataService.Theme.TextColorValue;
            }

            bool allowUpload = Category.AllowUploading();
            double screenWidth = Application.Current.MainPage.Width;
            double contentWidth = screenWidth - (contentHMargin * 2);
            double contentSubWidth = screenWidth - (contentHMargin * 4);
            double contentTitleFontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Default, NamedSize.Default, NamedSize.Default), typeof(Label));
            double elementTitleFontSize = Device.GetNamedSize(Device.OnPlatform(NamedSize.Small, NamedSize.Small, NamedSize.Small), typeof(Label));
            Color elementTitleColor = Color.Black;
            Color elementBackgroundColor = Color.White;
            Color elementGrayBackgroundColor = new Color(0.925, 0.925, 0.925);
            TextAlignment titleHAlign = TextAlignment.Start;
            TextAlignment titleVAlign = TextAlignment.Center;

            ScrollView main = new ScrollView ()
            {
                IsClippedToBounds = true
            };
            #endregion

            #region Content
            StackLayout content = new StackLayout ()
            {
                Spacing = 0,

                Margin = new Thickness(contentHMargin, contentHMargin)
            };

            #region Content.Header
            Grid gridHeader = CreateHeaderGrid();
            content.Children.Add(gridHeader);

            Image imageHeaderBack = new Image ()
            {
                Source = imagePathArrowBack
            };
            gridHeader.Children.Add(imageHeaderBack, 1, 0);

            Label labelHeaderTitle = new Label ()
            {
                Text = Category.Name,
                TextColor = textColor,

                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            gridHeader.Children.Add(labelHeaderTitle, 2, 0);

            Image imageHeaderIcon = new Image ()
            {
                Source = Category.GetIconFileContentPath()
            };
            gridHeader.Children.Add(imageHeaderIcon, 3, 0);

            Button buttonHeaderBack = new Button ()
            {
                BackgroundColor = Color.Transparent
            };
            buttonHeaderBack.SetBinding(Button.CommandProperty, "NavigateBackCommand");
            gridHeader.Children.Add(buttonHeaderBack, 0, 0);
            Grid.SetColumnSpan(buttonHeaderBack, 5);
            #endregion

            #region Content.Articles
            int gridColumns = 2;
            double gridColumnWidth = (contentWidth - contentHMargin) / gridColumns;

            double cellHeight = gridColumnWidth;
            double cellIconHeight = cellHeight * 0.4;
            double cellLabelHeight = 30;
            double cellPadHeight = cellHeight - cellIconHeight - cellLabelHeight;

            if (Category.Articles != null && Category.Articles.Count > 0)
            {
                List<AppArticle> articles = Category.Articles.Where(x => !string.IsNullOrEmpty(x.Title) && x.Title.Contains(FilterQuery)).ToList();

                if (articles != null && articles.Count > 0)
                {
                    StackLayout contentArticles = new StackLayout ()
                    {
                        Orientation = StackOrientation.Horizontal,
                        Spacing = contentHMargin
                    };
                    content.Children.Add(contentArticles);

                    StackLayout contentArticlesLeft = new StackLayout ()
                    {
                        Spacing = 5,
                        HorizontalOptions = LayoutOptions.Fill
                    };
                    contentArticles.Children.Add(contentArticlesLeft);

                    StackLayout contentArticlesRight = new StackLayout()
                    {
                        Spacing = 5,
                        HorizontalOptions = LayoutOptions.Fill
                    };
                    contentArticles.Children.Add(contentArticlesRight);

                    bool placeLeft = true;
                    string articleTitle = string.Empty;
                    string articleFileName = string.Empty;
                    AppArticleType articleType = AppArticleType.File;
                    AppArticle article;

                    for (int i = 0; i < articles.Count; i++)
                    {
                        article = articles[i];

                        if (article == null)
                            continue;

                        articleTitle = article.Title;
                        articleFileName = article.GetFriendlyFileName();
                        articleType = article.GetArticleType();

                        if (articleType.Equals(AppArticleType.Text) || articleType.Equals(AppArticleType.DirectText))
                        {
                            View articleTextElement = CreateArticleTextView(article, cellHeight, elementBackgroundColor, elementTitleFontSize, elementTitleFontSize * 0.8, elementTitleColor);

                            if (articleTextElement != null)
                            {
                                if (placeLeft)
                                    contentArticlesLeft.Children.Add(articleTextElement);
                                else
                                    contentArticlesRight.Children.Add(articleTextElement);

                                placeLeft = !placeLeft;
                            }

                        }
                        else
                        {
                            View articleElement = CreateArticleElementView(article, cellIconHeight, cellLabelHeight, cellPadHeight, elementBackgroundColor, elementTitleFontSize, elementTitleColor);

                            if (articleElement != null)
                            {
                                if (placeLeft)
                                    contentArticlesLeft.Children.Add(articleElement);
                                else
                                    contentArticlesRight.Children.Add(articleElement);

                                placeLeft = !placeLeft;
                            }
                        }
                    }
                }
            }
            #endregion
            #endregion

            main.Content = content;
            _scrollView = main;
            Content = main;
        }

        private View CreateSeparator (Color separatorColor)
        {
            return new HorizontalSeparator() { Color = separatorColor };
        }

        private View CreateArticleElementView (AppArticle article, double cellIconHeight, double cellLabelHeight, double cellPadHeight, Color viewColor, double titleFontSize, Color titleColor)
        {
            if (article == null)
                return null;

            bool isArticleIconAvailable = article.IsArticleIconAvailable();
            string imagePathArticle = isArticleIconAvailable ? article.GetIconFileContentPath() : article.GetFileExtensionImagePath();
            string articleTitle = article.Title;
            AppArticleType articleType = article.GetArticleType();

            Grid gridArticleElement = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(cellPadHeight / 2) },
                    new RowDefinition { Height = new GridLength(cellIconHeight - 5) },
                    new RowDefinition { Height = new GridLength(5) },
                    new RowDefinition { Height = new GridLength(isArticleIconAvailable ? cellPadHeight / 2 : cellLabelHeight) },
                    new RowDefinition { Height = new GridLength(isArticleIconAvailable ? cellLabelHeight : cellPadHeight / 2) }
                }
            };

            TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer ()
            {

            };

            BoxView boxBackground = new BoxView ()
            {
                Color = viewColor
            };
            boxBackground.GestureRecognizers.Add(tapGestureRecognizer);
            gridArticleElement.Children.Add(boxBackground, 0, 0);
            Grid.SetRowSpan(boxBackground, 5);

            Image imageArticle = new Image ()
            {
                Source = imagePathArticle
            };
            imageArticle.GestureRecognizers.Add(tapGestureRecognizer);
            if (!isArticleIconAvailable)
                gridArticleElement.Children.Add(imageArticle, 0, 1);
            else
            {
                gridArticleElement.Children.Add(imageArticle, 0, 0);
                Grid.SetRowSpan(imageArticle, 5);
            }

            Label labelArticle = new Label ()
            {
                Text = articleTitle,

                Margin = isArticleIconAvailable ? new Thickness(10, 0, 0, 10) : new Thickness(10, 0),
                TextColor = titleColor,
                FontSize = titleFontSize,
                HorizontalTextAlignment = isArticleIconAvailable ? TextAlignment.Start : TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center
            };
            labelArticle.GestureRecognizers.Add(tapGestureRecognizer);
            if (!isArticleIconAvailable)
                gridArticleElement.Children.Add(labelArticle, 0, 3);
            else
                gridArticleElement.Children.Add(labelArticle, 0, 4);

            if (articleType.Equals(AppArticleType.Link) || articleType.Equals(AppArticleType.LinkLogin) || articleType.Equals(AppArticleType.LinkExternal) ||
                articleType.Equals(AppArticleType.LinkAutoFill) || articleType.Equals(AppArticleType.LinkCredential))
            {
                string link = article.Value;

                if (string.IsNullOrEmpty(link))
                    link = string.Empty;

                if (articleType.Equals(AppArticleType.LinkAutoFill))
                {
                    string username = DataService.Username;
                    string password = DataService.Password;

                    if (string.IsNullOrEmpty(username)) username = "username";
                    if (string.IsNullOrEmpty(password)) password = "password";

                    tapGestureRecognizer.CommandParameter = new AppLinkFill { Title = articleTitle, Value = link, Username = username, Password = password };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebFillCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkCredential))
                {
                    string domain = DataService.BrowserCredentials.Domain;
                    string username = DataService.BrowserCredentials.Username;
                    string password = DataService.BrowserCredentials.Password;

                    if (string.IsNullOrEmpty(username)) domain = string.Empty;
                    if (string.IsNullOrEmpty(username)) username = "username";
                    if (string.IsNullOrEmpty(password)) password = "password";

                    tapGestureRecognizer.CommandParameter = new AppLinkFill { Title = articleTitle, Value = link, Username = (domain + username), Password = password };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebFillCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkExternal))
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebExternalCommand");
                }
                else if (articleType.Equals(AppArticleType.LinkLogin))
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToAuthenticatingWebCommand");
                }
                else
                {
                    tapGestureRecognizer.CommandParameter = new AppLink { Title = articleTitle, Value = link };
                    tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToWebCommand");
                }
            }
            else
            {
                tapGestureRecognizer.CommandParameter = article;
                tapGestureRecognizer.SetBinding(TapGestureRecognizer.CommandProperty, "NavigateToFileAttachmentCommand");
            }

            return gridArticleElement;
        }

        private View CreateArticleTextView (AppArticle article, double cellSize, Color viewColor, double titleFontSize, double bodyFontSize, Color textColor)
        {
            string title = article.Title;
            string text = (article.GetArticleType().Equals(AppArticleType.DirectText)) ? article.Value : article.TextValue;
            string textClean = Regex.Replace(text, "<.*?>", string.Empty);

            Grid textGrid = new Grid ()
            {
                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(cellSize) }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) }
                }
            };

            BoxView textBox = new BoxView ()
            {
                Color = Color.White,
                HeightRequest = cellSize
            };
            textGrid.Children.Add(textBox, 0, 0);

            StackLayout textStack = new StackLayout ()
            {
                Spacing = 5
            };
            textGrid.Children.Add(textStack, 0, 0);

            Label textLabel = new Label ()
            {
                Text = title,
                Margin = new Thickness(10, 10, 0, 0)
            };
            textStack.Children.Add(textLabel);

            if (Device.OS.Equals(TargetPlatform.Android))
            {
                HtmlSelectableLabelDroid textView = new HtmlSelectableLabelDroid ()
                {
                    Text = text,
                    TextColor = textColor,
                    Margin = new Thickness(10, 10)
                };
                textStack.Children.Add(textView);
            }
            else
            {
                HtmlSelectableLabeliOS textView = new HtmlSelectableLabeliOS ()
                {
                    Source = text,
                    TextColor = textColor,
                    Margin = new Thickness(10, 10),

                    FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label))
                };
                textStack.Children.Add(textView);
            }

            return textGrid;
        }

        private Grid CreateHeaderGrid ()
        {
            int headerHeight = Device.OnPlatform(55, 65, 65);

            Grid headerGrid = new Grid ()
            {
                BackgroundColor = Color.Transparent,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(40) },
                    new ColumnDefinition { Width = new GridLength(25) },
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(headerHeight) }
                }
            };

            return headerGrid;
        }

        private Grid CreateContentGrid (Color viewColor)
        {
            Grid contentGrid = new Grid ()
            {
                BackgroundColor = viewColor,

                ColumnSpacing = 0,
                RowSpacing = 0,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(25) },
                    new ColumnDefinition { Width = new GridLength(40) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(20) },
                    new ColumnDefinition { Width = new GridLength(25) },
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = new GridLength(50) }
                }
            };

            return contentGrid;
        }
    }
}
